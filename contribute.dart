import 'package:flutter/material.dart';
import 'package:santhwanam_plus/validation.dart';




class GiveMoney extends StatefulWidget {
  const GiveMoney({Key? key}) : super(key: key);

  @override
  _GiveMoneyState createState() => _GiveMoneyState();
}

class _GiveMoneyState extends State<GiveMoney> {

  TextEditingController amount = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Back To Home"),
        backgroundColor: Color(0xffef5350),
      ),
      body: SingleChildScrollView(
        child: SafeArea(
          child:Column(
            children: [
              SizedBox(height: 20,),
              Text("Contribute Money",
                style: TextStyle(color: Colors.black,fontSize: 20,
                  fontWeight: FontWeight.bold,
                  fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                ),),

              SizedBox(height: 30,),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: Container(width: 400,height: 300,
                  child: Card(shadowColor: Colors.grey,elevation: 2.0,
                    shape:RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0),
                    ),color: Colors.white,
                    child: Container(
                      child: Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Column(
                          children: [

                            SizedBox(height:20),

                            TextFormField(
                              controller: amount,
                              cursorColor: Colors.black,
                              keyboardType: TextInputType.number,
                              decoration: InputDecoration(
                                hintText: 'Enter Amount',
                                hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                              ),
                              validator: (value) {
                                return Validate.pincodevalidator(value!.trim());
                              },
                            ), //count



                            SizedBox(height: 10,),

                            SizedBox(height: 30,),
                            ElevatedButton(
                                onPressed: () {
                                  //Go to user home

                                },
                                style: ElevatedButton.styleFrom(
                                  primary: Color(0xffef5350),
                                  shape: new RoundedRectangleBorder(
                                    borderRadius: new BorderRadius.circular(15),
                                  ),
                                ),
                                child: Text(
                                  "  Submit ",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                                  ),
                                )),
                            //SizedBox(height:15),


                          ],
                        ),
                      ),


                    ),
                  ),
                ),
              ),

            ],
          ),

        ),
      ),
    );
  }


}
