import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

import '../validation.dart';

class GiveThings extends StatefulWidget {
  var uid;
  var uname;
  var uphone;
   GiveThings({Key? key,this.uid,this.uname,this.uphone}) : super(key: key);

  @override
  _GiveThingsState createState() => _GiveThingsState();
}

class _GiveThingsState extends State<GiveThings> {
  var uuid = Uuid();
  var contribute_id;
  var _contributeequi = GlobalKey<FormState>();
  TextEditingController object = TextEditingController();
  TextEditingController count = TextEditingController();
  TextEditingController uphone = TextEditingController();
  TextEditingController uname = TextEditingController();

  @override
  void initState() {
    contribute_id = uuid.v1();
    uname.text=widget.uname;
    uphone.text=widget.uphone;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: SafeArea(
          child: Form(
            key: _contributeequi,
            child: Column(
              children: [
                const SizedBox(
                  height: 20,
                ),
                const Text(
                  "Contribute Equipments",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Container(
                    width: 400,
                    height: 440,
                    child: Card(
                      shadowColor: Colors.grey,
                      elevation: 2.0,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      color: Colors.white,
                      child: Container(
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Column(
                            children: [
                              SizedBox(height: 20),
                              TextFormField(
                                keyboardType: TextInputType.text,
                                //maxLines: null,
                                controller: uname,
                                decoration: const InputDecoration(
                                  hintText: 'name',
                                  hintStyle: TextStyle(
                                      fontFamily:
                                      "RobotoSlab-VariableFont_wght.ttf"),
                                ),
                                validator: (value) {
                                  return Validate.namevalidator(value!.trim());
                                },
                              ), //thing name
                              TextFormField(
                                keyboardType: TextInputType.text,
                                //maxLines: null,
                                controller: object,
                                decoration: const InputDecoration(
                                  hintText: 'Which object you contribute?',
                                  hintStyle: TextStyle(
                                      fontFamily:
                                          "RobotoSlab-VariableFont_wght.ttf"),
                                ),
                                validator: (value) {
                                  return Validate.disease(value!.trim());
                                },
                              ), //thing name
                              TextFormField(
                                controller: count,
                                cursorColor: Colors.black,
                                keyboardType: TextInputType.number,
                                decoration: const InputDecoration(
                                  hintText: 'Count',
                                  hintStyle: TextStyle(
                                      fontFamily:
                                          "RobotoSlab-VariableFont_wght.ttf"),
                                ),
                                validator: (value) {
                                  return Validate.placevalidator(value!.trim());
                                },
                              ),
                              TextFormField(
                                controller: uphone,
                                cursorColor: Colors.black,
                                keyboardType: TextInputType.number,
                                decoration: const InputDecoration(
                                  hintText: 'phone',
                                  hintStyle: TextStyle(
                                      fontFamily:
                                      "RobotoSlab-VariableFont_wght.ttf"),
                                ),
                                validator: (value) {
                                  return Validate.phonevalidator(value!.trim());
                                },
                              ), //count//count

                              const SizedBox(
                                height: 30,
                              ),
                              ElevatedButton(
                                  onPressed: () {
                                    if (_contributeequi.currentState!
                                        .validate()) {
                                      FirebaseFirestore.instance
                                          .collection('equipment_contributions')
                                          .doc(contribute_id)
                                          .set({
                                        'contribute_id': contribute_id,
                                        //'userid':widget.name,
                                        'name': widget.uname,
                                        'object': object.text,
                                        'phone': widget.uphone,
                                        'count': count.text,
                                        'status': 1,
                                        'date': DateTime.now()
                                      }).then((value) {
                                        showsnackbar(context,
                                            "Equipment Contributed successfully");
                                      });
                                    }
                                  },
                                  style: ElevatedButton.styleFrom(
                                    primary: Color(0xffef5350),
                                    shape:  RoundedRectangleBorder(
                                      borderRadius:
                                           BorderRadius.circular(15),
                                    ),
                                  ),
                                  child: const Text(
                                    "  Submit ",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontFamily:
                                          "RobotoSlab-VariableFont_wght.ttf",
                                    ),
                                  )),
                              //SizedBox(height:15),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  showsnackbar(BuildContext context, String value) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        behavior: SnackBarBehavior.floating,
        backgroundColor: Colors.red,
        margin: EdgeInsetsDirectional.all(10),
        content: Text(
          value,
          style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
        )));
  }
}
