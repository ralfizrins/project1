import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';


class Ambulance extends StatefulWidget {

  var Ambname = [
    "Rinshad",
    "Ashiq",
    "Jishnu",
    'Rahim',
    'Sudheep','Manu','Maneesh'
  ];
  var phone = [
    '9048572066',
    '9567563300',
    '9567011225',
    '6543219078',
    '8976097612',
    '7856341290',
    '8965436734',];


  @override
  _AmbulanceState createState() => _AmbulanceState();
}

class _AmbulanceState extends State<Ambulance> {

  var _ambulancekey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xffef5350),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Form(
            key: _ambulancekey,
            child: Column(
              children: [

                SizedBox(height: 20,),
                Column(
                  children: [
                    Text("Ambulance Services",style: TextStyle(fontFamily: 'RobotoSlab-VariableFont_wght.ttf',fontWeight: FontWeight.bold,fontSize: 16),),
                    SizedBox(height:20),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height,
                      color: Colors.white,
                      child: ListView.builder(
                          itemCount: widget.Ambname.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Card(
                              elevation: 10.0,
                              margin: const EdgeInsets.only(
                                  left: 10, top: 0, right: 10, bottom: 10),
                              shadowColor: Colors.white,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15),
                              ),
                              child: SingleChildScrollView(
                                child: ListTile(
                                  onTap: () {
                                    Navigator.pushNamed(context, '/9');
                                  },
                                  title: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [

                                      Padding(
                                        padding: const EdgeInsets.only(left: 23),
                                        child: Text(
                                          widget.Ambname[index],
                                          style: const TextStyle(
                                              fontFamily:
                                              'RobotoSlab-VariableFont_wght.ttf',
                                              fontWeight: FontWeight.bold,
                                              fontSize: 18),
                                        ),
                                      ),
                                      const SizedBox(
                                          width: 5),

                                      TextButton(
                                          onPressed: () {
                                            _makePhoneCall(widget.phone[index]);
                                          },
                                          child: Text(
                                            widget.phone[index],
                                            style: TextStyle(color: Colors.blue),
                                          ),
                                        ),

                                      const Icon(
                                        Icons.smartphone,
                                        size: 20,
                                        color: Colors.blue,
                                      ),







                                    ],
                                  ),

                                ),
                              ),
                            );
                          }),

                    ),
                  ],
                )
              ],
            ),
          ),
        ),

      ),

    );
  }
  Future<void> _makePhoneCall(String number) async {
    var url = 'tel:${number}';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

}
