import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class User_Notification extends StatefulWidget {
  var uid;
  User_Notification({Key? key, this.uid}) : super(key: key);

  @override
  _User_NotificationState createState() => _User_NotificationState();
}

class _User_NotificationState extends State<User_Notification> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Allocations",
        ),
        backgroundColor: const Color(0xffef5350),
      ),
      body: SafeArea(
        child: StreamBuilder<QuerySnapshot>(
          stream: FirebaseFirestore.instance
              .collection('alloc_list')
              .where('user_id', isEqualTo: widget.uid)
              .snapshots(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return const Center(child: CircularProgressIndicator());
            } else if (snapshot.hasData && snapshot.data!.docs.isEmpty) {
              return const Center(
                  child: Text(
                "No allocation found",
                style: TextStyle(fontSize: 25),
              ));
            } else {
              return ListView.builder(
                  itemCount: snapshot.data!.docs.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Column(
                      children: [
                        const SizedBox(
                          height: 20,
                        ),
                        Card(
                          elevation: 10.0,
                          margin: const EdgeInsets.only(
                              left: 10, top: 0, right: 10, bottom: 10),
                          shadowColor: Colors.white,
                          shape: const RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(30),
                                topRight: Radius.circular(30)),
                          ),
                          child: SingleChildScrollView(
                            child: ListTile(
                              onTap: () {},
                              title: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  Row(
                                    children: const [
                                      Text(
                                        "Blood Allocated Successfully",
                                        style: TextStyle(
                                            fontSize: 17,
                                            fontFamily:
                                                'RobotoSlab-VariableFont_wght.ttf'),
                                      ),
                                      Icon(
                                        Icons.check,
                                        color: Colors.green,
                                        size: 30,
                                      )
                                    ],
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        snapshot.data?.docs[index]
                                            ['donor_name'],
                                        style: const TextStyle(
                                            fontFamily:
                                                'RobotoSlab-VariableFont_wght.ttf',
                                            fontWeight: FontWeight.bold,
                                            fontSize: 18),
                                      ),
                                      const SizedBox(
                                        width: 15,
                                      ),
                                      Text(

                                        snapshot.data?.docs[index]
                                            ['donor_phone'],
                                        style: const TextStyle(
                                            fontSize: 16,
                                            fontFamily:
                                                'RobotoSlab-VariableFont_wght.ttf',
                                            color: Colors.blueAccent),
                                      )
                                    ],
                                  ),
                                  Text(
                                    snapshot.data?.docs[index]['place'],
                                    style: const TextStyle(
                                      fontSize: 16,
                                      fontFamily:
                                          'RobotoSlab-VariableFont_wght.ttf',
                                    ),
                                  ),

                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    );
                  });
            }
          },
        ),
      ),
    );
  }
  Future<void> _makePhoneCall(String number) async {
    var url = 'tel:${number}';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
