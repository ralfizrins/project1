import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:santhwanam_plus/users/request_sevice.dart';


class Store extends StatefulWidget {
  var uid;
  var uemail;
  var uphone;
  var uname;
   Store({Key? key,this.uid,this.uname,this.uphone,this.uemail}) : super(key: key);

  @override
  _StoreState createState() => _StoreState();
}

class _StoreState extends State<Store> {
  var _storekey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(

        body: SafeArea(
          child: Form(
            key: _storekey,
            child:Column(
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 15.0),
                    child: StreamBuilder<QuerySnapshot>(
                      stream:
                      FirebaseFirestore.instance.collection('store').snapshots(),
                      builder: (context, snapshot) {
                        if (!snapshot.hasData)
                          return Center(child: CircularProgressIndicator());
                        else if (snapshot.hasData && snapshot.data!.docs.length == 0) {
                          return const Center(
                              child: Text(
                                "No data found",
                                style: TextStyle(fontSize: 25),
                              ));
                        } else {
                          return ListView.builder(
                              itemCount: snapshot.data!.docs.length,
                              itemBuilder: (BuildContext context, int index) {
                                return Padding(
                                  padding: const EdgeInsets.only(bottom: 15.0),
                                  child: Center(
                                    child: Container(
                                      width: 250,
                                      height: 250,
                                      decoration: BoxDecoration(
                                        border: Border.all(
                                          color: Color(0xff89d3fb),
                                        ),
                                        borderRadius: BorderRadius.circular(8),
                                      ),
                                      child: Column(
                                        children: [
                                          Column(
                                            children: [
                                              SizedBox(height: 10.0,),
                                              Text(
                                                snapshot.data?.docs[index]['name'],
                                                style: const TextStyle(
                                                    fontFamily:
                                                    'RobotoSlab-VariableFont_wght.ttf',
                                                    fontSize: 20,
                                                    fontWeight: FontWeight.bold),
                                              ),
                                              Image.network(
                                                snapshot.data?.docs[index]['imgurl'],
                                                width: 150,
                                                height: 150,
                                              ),
                                              SizedBox(height: 10.0,),
                                              Text(
                                                'Stock: '+ snapshot.data?.docs[index]['stock'],
                                                style: const TextStyle(
                                                    fontFamily:
                                                    'RobotoSlab-VariableFont_wght.ttf',
                                                    fontSize: 15,
                                                    fontWeight: FontWeight.bold),
                                              ),
                                            ],

                                          ),

                                        ],
                                      ),
                                    ),

                                  ),
                                );

                              });
                        }
                      },
                    ),
                  ),


                ),
                ElevatedButton(onPressed: (){
                  if (_storekey.currentState!.validate()) {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ServiceRequest(
                              uname:widget.uname ,
                              uid:widget.uid ,
                            )));
                  }
                },
                  child: Text('Request'), style: ElevatedButton.styleFrom(
                      fixedSize: Size(100,30),
                      primary: Color(0xffef5350),
                      shape:  RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      )),

                ),
              ],
            ),
          ),
        ));
  }
}