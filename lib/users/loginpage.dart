import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:santhwanam_plus/users/register.dart';
import 'package:santhwanam_plus/users/userdesign.dart';
import 'package:santhwanam_plus/validation.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';

import '../admin/admin_home.dart';
import '../volunteer/volunteer_home.dart';


class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  var _loginkey = GlobalKey<FormState>();
  TextEditingController emailInputcontroller = TextEditingController();
  TextEditingController passwordInputcontroller = TextEditingController();
  bool visible = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: SafeArea(
        child: SingleChildScrollView(
          child: Form(
            key: _loginkey,
            child: Column(
                children: [
                  ClipPath(
                    clipper: WaveClipperOne(),
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height/2.3,
                      color: Color(0xffef5350),
                      child: Container(height: 50,width: 50,
                          child: Center(child: Text("Santhwanam+",style: (TextStyle(fontSize: 20,fontFamily: 'RobotoSlab-VariableFont_wght.ttf')),)),),
                    ),
                    ),
                  SizedBox(height:30,),
                  Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Card(shadowColor: Colors.grey,elevation: 2.0,
                      shape:RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      ),color: Colors.white,
                      child: Container(
                        child: Padding(
                          padding: const EdgeInsets.all(25.0),
                          child: Column(
                            children: [

                              SizedBox(height:20),
                              TextFormField(
                                controller: emailInputcontroller,
                                cursorColor: Colors.black,
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                    focusColor: Color(0xffeeeeee),
                                    fillColor: Colors.red,
                                    hintText: 'Username',
                                    prefixIcon: Icon(Icons.email),
                                    hintStyle: const TextStyle(
                                        color: Colors.grey,
                                        fontFamily: "RobotoSlab-VariableFont_wght.ttf"),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: const BorderSide(
                                            color: Color(0xffef5350),width: 2.0),
                                        borderRadius: BorderRadius.circular(15)),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: const BorderSide(
                                        color: Color(0xffef5350),

                                        width: 2.0,
                                      ),
                                      borderRadius: BorderRadius.circular(15),
                                    )),
                                validator: (value) {
                                  return Validate.emailValidator(value!.trim());
                                },
                              ),

                              SizedBox(height: 10,),
                              TextFormField(
                                controller: passwordInputcontroller,
                                cursorColor: Colors.redAccent,
                                keyboardType: TextInputType.text,
                                obscureText: visible,
                                obscuringCharacter: ".",
                                decoration: InputDecoration(
                                    hintText: 'Password',
                                    prefixIcon: Icon(Icons.lock,color: Colors.grey,),
                                    hintStyle: const TextStyle(
                                        color: Colors.grey,
                                        fontFamily: "RobotoSlab-VariableFont_wght.ttf"),
                                    suffixIcon: GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            visible = !visible;
                                          });
                                        },
                                        child: Icon(
                                          visible
                                              ? Icons.visibility
                                              : Icons.visibility_off,color: Color(0xffef5350),

                                        )),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: const BorderSide(
                                          color: Color(0xffef5350),

                                        width: 2.0,
                                      ),
                                      borderRadius: BorderRadius.circular(15),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: const BorderSide(
                                        color: Color(0xffef5350),
                                        width: 2.0,
                                      ),
                                      borderRadius: BorderRadius.circular(15),
                                    )),
                                validator: (value) {
                                  return Validate.passwordvalidator(value!.trim());
                                },
                              ),
                              SizedBox(height: 15,),
                              ElevatedButton(
                                  onPressed: () {
                                        if (_loginkey.currentState!.validate()) {
                                          FirebaseAuth.instance.signInWithEmailAndPassword(
                                              email: emailInputcontroller.text.trim(),
                                              password: passwordInputcontroller.text.trim()).
                                          then((user) =>FirebaseFirestore.instance
                                                  .collection('login').doc(
                                                  user.user!.uid).get().then((
                                                  value)
                                    {
                                      if (value.data()!['userstatus'] ==1 && value.data()!['status'] == 1)
                                      {
                                                  FirebaseFirestore.instance.collection('user').doc(user.user!.uid).get()
                                                      .then((value) =>
                                                  Navigator.push(context,MaterialPageRoute(builder: (context) =>UserDesign(
                                                     uid:value.data()!['uid'],uname:value.data()!['name'],
                                                    uemail:value.data()!['email'],uphone:value.data()!['phone'],imgurl:value.data()!['imgurl'],
                                                    dob: value.data()!['dob'],address:value.data()!['address'] ,pincode: value.data()!['pincode'],proof: value.data()!['proof'],bloodgroup: value.data()!['bloodgroup'],job: value.data()!['job'],
                                                   ))));
                                      }
                                      else if (value.data()!['userstatus'] == 2 && value.data()!['status'] == 1)
                                      {
                                                  FirebaseFirestore.instance.collection('volunteer').doc(user.user!.uid)
                                                  .get().then((value) =>
                                              Navigator.push(context,MaterialPageRoute(builder: (context) =>Volunteer_home(uid:value.data()!['uid'],vemail:value.data()!['email'],vname:value.data()!['name'],vphone:value.data()!['phone'],vaddress:value.data()!['address'],vbloodgroup:value.data()!['bloodgroup'],vproof:value.data()!['proof'],vjob:value.data()!['job'], ))));
                                      }
                                      else if (value.data()!['userstatus'] ==3 && value.data()!['status'] == 1)
                                      {
                                                  FirebaseFirestore.instance.collection('Admin').doc(user.user!.uid)
                                                  .get().then((value) =>
                                          Navigator.push(context,MaterialPageRoute(builder: (context) =>Adminhome())));
                                      }
                                      else if (value.data()!['userstatus'] ==4 && value.data()!['status'] == 1)
                                      {
                                        FirebaseFirestore.instance.collection('careperson').doc(user.user!.uid)
                                            .get().then((value) =>
                                            Navigator.push(context,MaterialPageRoute(builder: (context) =>Adminhome())));
                                      }
                                              })).catchError((e) =>
                                          (emailInputcontroller.text == 'admin@gmail.com' &&
                                              passwordInputcontroller.text == '1234567890')?
                                          Navigator.push(context,MaterialPageRoute(builder: (context) => Adminhome())):(emailInputcontroller.text == 'volunteer@gmail.com' &&
                                              passwordInputcontroller.text == '1234567890')?
                                          Navigator.push(context,MaterialPageRoute(builder: (context) => Volunteer_home())):showsnackbar(context,"login failed"));

                                          //showsnackbar(context,"Login Failed"));
                                           }

                                    // (emailInputcontroller.text == 'admin@gmail.com' &&
                                    //     passwordInputcontroller.text == '1234567890')?
                                    // Navigator.push(context,MaterialPageRoute(builder: (context) => Adminhome())):(emailInputcontroller.text == 'volunteer@gmail.com' &&
                                    //     passwordInputcontroller.text == '1234567890')?
                                    // Navigator.push(context,MaterialPageRoute(builder: (context) => Volunteer_home())):showsnackbar(context,"login failed");

                                          },
                                  style: ElevatedButton.styleFrom(
                                    primary: Color(0xffef5350),
                                    shape:  RoundedRectangleBorder(
                                      borderRadius: new BorderRadius.circular(15),
                                    ),
                                  ),
                                  child: const Text(
                                    "    Login    ",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                                    ),
                                  )),
                              SizedBox(height:15),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  const Text("New User ?",
                                    style: TextStyle(
                                    color: Colors.black,fontSize: 10,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                                  ),),
                                  TextButton(onPressed: (){
                                    Navigator.push(context,
                                        MaterialPageRoute(builder: (context)=> Register()));

                                  }, child: const Text("Register",style: TextStyle(
                                    color: Colors.black,fontSize: 18,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                                  ),))
                                ],
                              ),
                              // Column(
                              //   children: [
                              //     TextButton(onPressed: (){
                              //       Navigator.push(context,
                              //           MaterialPageRoute(builder: (context)=> Adminhome()));
                              //
                              //     }, child: const Text("Admin",style: TextStyle(
                              //       color: Colors.black,fontSize: 18,
                              //       fontWeight: FontWeight.bold,
                              //       fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                              //     ),)),
                              //     TextButton(onPressed: (){
                              //       Navigator.push(context,
                              //           MaterialPageRoute(builder: (context)=> Volunteer_home()));
                              //
                              //     }, child: const Text("Volunteer",style: TextStyle(
                              //       color: Colors.black,fontSize: 18,
                              //       fontWeight: FontWeight.bold,
                              //       fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                              //     ),))
                              //   ],
                              // ),

                              SizedBox(height: 15,),




                            ],
                          ),
                        ),


                      ),
                    ),
                  ),





                ],



              ),
          ),

        ),
      ),

    );
  }

}
showsnackbar(BuildContext context, String value) {
  ScaffoldMessenger.of(context).showSnackBar(
       SnackBar(behavior: SnackBarBehavior.floating,
          backgroundColor: Colors.red,
          margin:EdgeInsetsDirectional.all(10),
      content: Text(
        value,
        style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
      )));
}
