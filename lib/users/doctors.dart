import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';

class Doctors_Details extends StatefulWidget {
  @override
  _Doctors_DetailsState createState() => _Doctors_DetailsState();
}

class _Doctors_DetailsState extends State<Doctors_Details> {
  var _doctorkey = GlobalKey<FormState>();
  TextEditingController doctorname = TextEditingController();
  TextEditingController department = TextEditingController();
  TextEditingController hospital = TextEditingController();
  TextEditingController time = TextEditingController();
  TextEditingController place = TextEditingController();
  TextEditingController number = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xffef5350),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Form(
            key: _doctorkey,
            child: Column(
              children: [
                const SizedBox(
                  height: 10,
                ),
                const Text(
                  "Doctors Details",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 480,
                  color: Colors.white,
                  child: StreamBuilder<QuerySnapshot>(
                      stream: FirebaseFirestore.instance
                          .collection('doctors')
                          .where('status', isEqualTo: 1)
                          .snapshots(),
                      builder: (context, snapshot) {
                        if (!snapshot.hasData)
                          return Center(child: CircularProgressIndicator());
                        else if (snapshot.hasData &&
                            snapshot.data!.docs.length == 0)
                          return Center(child: Text("no data found"));
                        else {
                          return ListView.builder(
                              itemCount: snapshot.data!.docs.length,
                              itemBuilder: (BuildContext context, int index) {
                                return Card(
                                  elevation: 10.0,
                                  margin: const EdgeInsets.only(
                                      left: 10, top: 0, right: 10, bottom: 10),
                                  shadowColor: Colors.white,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                  child: SingleChildScrollView(
                                    child: ListTile(
                                      onTap: () {},
                                      title: SingleChildScrollView(
                                        child: Column(
                                          children: [
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(8.0),
                                              child: Row(
                                                children: [
                                                  Text(
                                                    snapshot.data!.docs[index]
                                                        ['name'],
                                                    style: const TextStyle(
                                                        fontFamily:
                                                            'RobotoSlab-VariableFont_wght.ttf',
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 15),
                                                  ),
                                                  const SizedBox(
                                                    width: 20,
                                                  ),
                                                  Text(
                                                    snapshot.data!.docs[index]
                                                        ['department'],
                                                    style: const TextStyle(
                                                        fontFamily:
                                                            'RobotoSlab-VariableFont_wght.ttf',
                                                        color: Colors.blue,
                                                        fontSize: 15),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(8.0),
                                              child: Row(
                                                children: [
                                                  Text(
                                                    snapshot.data!.docs[index]
                                                        ['hospital'],
                                                  ),
                                                  const SizedBox(
                                                    width: 20,
                                                  ),
                                                  Text(
                                                    snapshot.data!.docs[index]
                                                        ['time'],
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(8.0),
                                              child: Row(
                                                children: [
                                                  Text(snapshot.data!
                                                      .docs[index]['place']),
                                                  const SizedBox(
                                                    width: 20,
                                                  ),
                                                  Text(
                                                    snapshot.data!.docs[index]
                                                        ['phone'],
                                                    style: const TextStyle(
                                                        color: Colors.blue),
                                                  ),
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                );
                              });
                        }
                      }),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
