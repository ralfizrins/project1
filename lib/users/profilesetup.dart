import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:santhwanam_plus/users/userhome.dart';
import 'package:santhwanam_plus/users/userdesign.dart';
import 'package:santhwanam_plus/validation.dart';
import 'package:date_time_picker/date_time_picker.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';



class ProfileSetup extends StatefulWidget {
  var uid;
  var name;
  var email;
  var phone;
  var imgurl;
   ProfileSetup({Key? key,this.uid,this.name,this.phone,this.email,this.imgurl}) : super(key: key);

  @override
  _ProfileSetupState createState() => _ProfileSetupState();
}

class _ProfileSetupState extends State<ProfileSetup> {
  var _profilekey = GlobalKey<FormState>();
  TextEditingController emailInputcontroller = TextEditingController();
  TextEditingController phone = TextEditingController();
  TextEditingController name = TextEditingController();
  TextEditingController address = TextEditingController();
  TextEditingController pincode = TextEditingController();
  TextEditingController proof = TextEditingController();
  TextEditingController job = TextEditingController();
  TextEditingController blood = TextEditingController();
  var url;
  var filename;


  var bloodgroup;
  var assume="Value from DB";//for demo
  late TextEditingController _takedob; //For late initialisation:- late Keyword
  late String _valueChanged3;
  late String _valueToValidate3;
  late String _valueSaved3;
  final ImagePicker _picker = ImagePicker();// For pick Image
  XFile ? _image; // For accept Null:-?

  void initState() {
    // TODO: implement initState
    _takedob=new TextEditingController(text:DateTime.now().toString());
    name.text=widget.name;
    emailInputcontroller.text=widget.email;
    phone.text=widget.phone;
    filename = DateTime.now().toString();

    super.initState(

    );
  }
  @override
  Widget build(BuildContext context) {
    var visible=true;

    return Scaffold(
      body: SafeArea(
        child:SingleChildScrollView(
          child: Form(
            key: _profilekey,
            child: Column(
              children: [
                ClipPath(
                  clipper: WaveClipperOne(),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height/6,
                    color: Color(0xffef5350),
                  ),
                ),
                SizedBox(height: 20,),
                const Text("Setup Your Profile",
                  style: TextStyle(color: Colors.black,fontSize: 20,
                    fontWeight: FontWeight.bold,
                    fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                  ),),
                SizedBox(height: 10,),
                SizedBox(height: 20,),
                GestureDetector(
                  onTap: (){
                    _getpics();
                  },
                  child:   _image != null
                      ? ClipRect(
                    child: Container(
                      width: 80, //MediaQuery.of(context).size.width,
                      height: 80,
                      child: Image.file(
                        File(_image!.path), //null check operator added:-!
                      ),
                    ),
                  )
                      : widget.imgurl!=null?Container(
                    width: 80, //MediaQuery.of(context).size.width,
                    height: 80, //MediaQuery.of(context).size.height,
                    child: Image.network(widget.imgurl),
                  ):Container(
                    child:Icon(Icons.account_circle),
                  ),

                ),

                ElevatedButton(
                  style:ElevatedButton.styleFrom(primary: Colors.white),
                  onPressed: () {
                    var ref = FirebaseStorage.instance
                        .ref()
                        .child('user/$filename');
                    UploadTask uploadTask = ref.putFile(File(_image!.path));

                    uploadTask.then((res) async {
                      url = (await ref.getDownloadURL()).toString();
                    }).then((value) => FirebaseFirestore.instance
                        .collection('user')
                        .doc(widget.uid)
                        .update({
                      'imgurl': url,
                    }).then((value) {
                      showsnackbar(context, "Image uploaded");
                    }));

                  },
                  child: Text("Uplaod Image",style:TextStyle(color:Colors.red),),
                ),
                const Padding(
                  padding: EdgeInsets.only(left:20),
                  child: Center(
                    child: Text("Upload profile picture",style: TextStyle(color: Colors.black,fontSize: 10,
                      fontWeight: FontWeight.bold,
                      fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                    ),),
                  ),
                ),
                SizedBox(height: 20,),
                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Container(width: 400,height: 700,
                    child: Card(shadowColor: Colors.grey,elevation: 2.0,
                      shape:RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      ),color: Colors.white,
                      child: Container(
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Column(
                            children: [

                              SizedBox(height:10),
                              TextFormField(
                                controller: name,
                                readOnly: true,
                                decoration: InputDecoration(
                                  hintText: assume,
                                  hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                                ),
                              ),//Name
                              TextFormField(
                                controller: emailInputcontroller,
                                readOnly: true,
                                decoration: InputDecoration(
                                  hintText: assume,
                                  hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                                ),
                              ),//Email
                              TextFormField(
                                controller: phone,
                                readOnly: true,
                                decoration: InputDecoration(
                                  hintText: assume,
                                  hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                                ),
                              ),//mobile
                              DateTimePicker(
                                type: DateTimePickerType.date,
                                //dateMask: 'yyyy/MM/dd',
                                fieldLabelText: 'Date of birth',
                                controller: _takedob,
                                firstDate: DateTime(1990),
                                lastDate: DateTime(2050),
                                icon: Icon(Icons.event),
                                dateLabelText: 'Date of birth',
                                //locale: Locale('pt', 'BR'),
                                onChanged: (val) => setState(() => _valueChanged3 = val),
                                validator: (val) {
                                  setState(() => _valueToValidate3 = val ?? '');
                                  return null;
                                },
                                onSaved: (val) => setState(() => _valueSaved3 = val ?? ''),
                              ),//dob
                              TextFormField(
                                keyboardType: TextInputType.multiline,
                                maxLines: null,
                                controller: address,
                                decoration: const InputDecoration(
                                  hintText: 'Address',
                                  hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                                ),
                                validator: (value) {
                                return Validate.addressvalidator(value!.trim());
                              },

                              ),//address_Multilined
                              TextFormField(
                                controller: pincode,
                                cursorColor: Colors.black,
                                keyboardType: TextInputType.number,
                                decoration: const InputDecoration(
                                  hintText: 'Pincode',
                                  hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                                ),
                                validator: (value) {
                                  return Validate.pincodevalidator(value!.trim());
                                },
                              ), //pincode
                              TextFormField(
                                controller: proof,
                                cursorColor: Colors.black,
                                keyboardType: TextInputType.text,
                                decoration: const InputDecoration(
                                  hintText: 'ID(Adhar/passport/Liscemse)',
                                  hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                                ),
                                validator: (value) {
                                  return Validate.proofvalidator(value!.trim());
                                },
                              ), //proof
                              DropdownButtonFormField<String>(
                                value: bloodgroup,
                                decoration: const InputDecoration(
                                  hintText: "Blood Group",
                                  //prefixIcon: Icon(Icons.email),
                                  //suffixIcon: Icon(Icons.panorama_fish_eye),
                                  //border: OutlineInputBorder(borderRadius: BorderRadius.circular(30),
                                ),
                                onChanged: (ctgry) =>
                                    setState(() => bloodgroup = ctgry),
                                validator: (value) => value == null ? 'field required' : null,
                                items: ['A+','A-','B+','B-','AB+','AB-','O+','O-']
                                    .map<DropdownMenuItem<String>>(
                                        (String value) {
                                      return DropdownMenuItem<String>(
                                        value: value,
                                        child: Text(value),
                                      );
                                    }).toList(),
                              ),//blood
                              TextFormField(
                                controller: job,
                                cursorColor: Colors.black,
                                keyboardType: TextInputType.text,
                                decoration: const InputDecoration(
                                  hintText: 'Job',
                                  hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                                ),
                                validator: (value) {
                                  return Validate.jobvalidator(value!.trim());
                                },
                              ),//job
                              SizedBox(height:20,),
                              ElevatedButton(
                                  onPressed: () {
                                    if (_profilekey.currentState!.validate()){
                                      FirebaseFirestore.instance
                                          .collection('user')
                                          .doc(widget.uid)
                                          .update({
                                        'address': address.text,
                                        'dob': _takedob.text,
                                        'pincode': pincode.text,
                                        'proof': proof.text,
                                        'job': job.text,
                                        'bloodgroup': bloodgroup,
                                      }).then((value) {
                                        showsnackbar(
                                            context, "Profile Updated..");

                                        Navigator.pop(context);
                                      });


                                    }

                                    },
                                  style: ElevatedButton.styleFrom(
                                    primary: Color(0xffef5350),
                                    shape:  RoundedRectangleBorder(
                                      borderRadius: new BorderRadius.circular(15),
                                    ),
                                  ),
                                  child: const Text(
                                    "Submit",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                                    ),
                                  )),
                              //SizedBox(height:15),


                            ],
                          ),
                        ),


                      ),
                    ),
                  ),
                ),

              ],
            ),
          ),
        ),

      ),
    );
  }

  _imgbrowse()async{
    final XFile? photo = await _picker.pickImage(source: ImageSource.camera);
    //final XFile? image = await _picker.pickImage(source: ImageSource.gallery);
    setState(() {
      _image=photo;
    });
  }
  _imggallery()async{
    //final XFile? photo = await _picker.pickImage(source: ImageSource.camera);
    final XFile? image = await _picker.pickImage(source: ImageSource.gallery);
    setState(() {
      _image=image;
    });}
  _getpics(){
    showModalBottomSheet(
        context: context,
        builder: (context){
          return Container(
            child: Wrap(
              children: [
                Expanded(
                  child: ListTile(
                      title: Text("Photo Gallery"),
                      leading: Icon(Icons.image),
                      onTap: (){
                        _imggallery();
                      }

                  ),
                ),
                Expanded(
                  child: ListTile(
                    title: Text("Camera"),
                    leading: Icon(Icons.camera),
                    onTap: (){
                      _imgbrowse();
                    },
                  ),
                ),

              ],

            ),

          );
        }
    );
  }
  showsnackbar(BuildContext context, String value) {
    ScaffoldMessenger.of(context).showSnackBar(
         SnackBar(behavior: SnackBarBehavior.floating,
            backgroundColor: Colors.red,
            margin:EdgeInsetsDirectional.all(10),
            content: Text(
              value,
              style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
            )));
  }

}

