import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:santhwanam_plus/users/userhome.dart';
import 'package:santhwanam_plus/users/userdesign.dart';
import 'package:santhwanam_plus/validation.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';

class Register extends StatefulWidget {
  const Register({Key? key}) : super(key: key);

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  var _registerkey = GlobalKey<FormState>();
  TextEditingController emailInputcontroller = TextEditingController();
  TextEditingController passwordInputcontroller = TextEditingController();
  TextEditingController Confirmpasswordcontroller = TextEditingController();
  TextEditingController phone = TextEditingController();
  TextEditingController name = TextEditingController();

  bool visible = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Form(
            key: _registerkey,
            child: Column(
              children: [
                ClipPath(
                 clipper: WaveClipperOne(),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height/6,
                    color: Color(0xffef5350),
                  ),

                ),
                Text('Create Account',style: TextStyle(fontSize: 20,fontFamily: 'RobotoSlab-VariableFont_wght.ttf',fontWeight: FontWeight.bold)),
                const SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: const EdgeInsets.all(15.0),
                    child: SingleChildScrollView(
                      child: Container(
                        width: 340,height: 560,
                        child: Card(
                          shadowColor: Colors.grey,
                          elevation: 2.0,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20.0),
                          ),
                          color: Colors.white,
                          child: Container(
                            child: Padding(
                              padding: const EdgeInsets.all(20.0),
                              child: Column(
                                children: [
                                  SizedBox(height: 20),

                                  TextFormField(
                                    decoration: const InputDecoration(
                                      hintText: "Name",
                                      hintStyle: TextStyle(
                                          fontFamily:
                                              "RobotoSlab-VariableFont_wght.ttf"),
                                    ),
                                    controller: name,
                                    cursorColor: Colors.black,
                                    keyboardType: TextInputType.text,
                                    validator: (value) {
                                      return Validate.namevalidator(value!.trim());
                                    },
                                  ),

                                  TextFormField(
                                    controller: emailInputcontroller,
                                    cursorColor: Colors.black,
                                    keyboardType: TextInputType.text,
                                    decoration: const InputDecoration(
                                      hintText: 'Email',
                                      hintStyle: TextStyle(
                                          fontFamily:
                                              "RobotoSlab-VariableFont_wght.ttf"),
                                    ),
                                    validator: (value) {
                                      return Validate.emailValidator(value!.trim());
                                    },
                                  ),

                                  TextFormField(
                                    controller: passwordInputcontroller,
                                    cursorColor: Colors.black,
                                    keyboardType: TextInputType.text,
                                    obscureText: visible,
                                    obscuringCharacter: ".",
                                    decoration: const InputDecoration(
                                      hintText: 'Password',
                                      hintStyle: TextStyle(
                                          fontFamily:
                                              "RobotoSlab-VariableFont_wght.ttf"),

                                    ),
                                    validator: (value) {
                                      return Validate.passwordvalidator(
                                          value!.trim());
                                    },
                                  ),

                                  TextFormField(
                                    controller: Confirmpasswordcontroller,
                                    cursorColor: Colors.black,
                                    keyboardType: TextInputType.text,
                                    decoration: InputDecoration(
                                      hintText: 'Confirm password',
                                      hintStyle: const TextStyle(
                                          fontFamily:
                                          "RobotoSlab-VariableFont_wght.ttf"),
                                      suffixIcon: GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              visible = !visible;
                                            });
                                          },
                                          child: Icon(
                                            visible
                                                ? Icons.visibility
                                                : Icons.visibility_off,color: Colors.grey,

                                          )),
                                    ),
                                    validator: (value) {
                                      return Validate.confirmpwdvalidator(value!, passwordInputcontroller.text);
                                    },



                                  ),

                                  TextFormField(
                                    controller: phone,
                                    cursorColor: Colors.black,
                                    keyboardType: TextInputType.text,
                                    decoration: const InputDecoration(
                                      hintText: 'Mobile Number',
                                      hintStyle: TextStyle(
                                          fontFamily:
                                              "RobotoSlab-VariableFont_wght.ttf"),
                                    ),
                                    validator: (value) {
                                      return Validate.phvalidator(value!.trim());
                                    },
                                  ),
                                  SizedBox(height: 50),
                                  ElevatedButton(
                                      onPressed: () {
                                        if(_registerkey .currentState!.validate() )
                                        {
                                          FirebaseAuth.instance.createUserWithEmailAndPassword
                                            (email:emailInputcontroller.text.trim(),
                                              password: passwordInputcontroller.text.trim()).
                                          then((user) => FirebaseFirestore.instance.collection('login').doc(user.user!.uid).set({
                                            'uid':user.user!.uid,
                                            'email':emailInputcontroller.text.trim(),
                                            'password':passwordInputcontroller.text.trim(),
                                            'status':1,
                                            'userstatus':1,
                                            'date':DateTime.now()
                                          }).then((value) => FirebaseFirestore.instance.collection('user').doc(user.user!.uid).
                                          set(
                                              {'uid':user.user!.uid,
                                          'email':emailInputcontroller.text.trim(),'password':passwordInputcontroller.text.trim(),
                                          'name':name.text,
                                                'address':null,
                                                'pincode':null,
                                                'proof':null,
                                                'bloodgroup':null,
                                                'job':null,
                                                'dob':null,
                                                'phone':phone.text,
                                                'status':1,
                                                'date':DateTime.now()

                                              }
                                          ).then((value) {
                                            showsnackbar(context, "Registered successfully");
                                            Navigator.pop(context);
                                          }))).catchError((e)=> print(e));
                                        }
                                        else
                                          showsnackbar(context,"Please check the Fields");


                                        //Navigator.push(context, MaterialPageRoute(builder: (context) => UserDesign()));
                                       // showalert(context);






                                      },
                                      style: ElevatedButton.styleFrom(
                                        primary: Color(0xffef5350),
                                        shape:  RoundedRectangleBorder(
                                          borderRadius:
                                              new BorderRadius.circular(15),
                                        ),
                                      ),
                                      child: const Text(
                                        "  Register  ",
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          fontFamily:
                                              "RobotoSlab-VariableFont_wght.ttf",
                                        ),
                                      )),
                                  SizedBox(height: 15),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),

                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
  showsnackbar(BuildContext context, String value) {
    ScaffoldMessenger.of(context).showSnackBar(
         SnackBar(behavior: SnackBarBehavior.floating,
            backgroundColor: Colors.red,
            margin: EdgeInsetsDirectional.all(20),
            content: Text(
              value,
              style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
            )));
  }
  }
