import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:santhwanam_plus/users/history.dart';
import 'package:santhwanam_plus/users/Ratefeedback.dart';
import 'package:santhwanam_plus/users/ambulance.dart';
import 'package:santhwanam_plus/users/bloodbank.dart';
import 'package:santhwanam_plus/users/loginpage.dart';
import 'package:santhwanam_plus/users/profilesetup.dart';
import 'package:santhwanam_plus/users/request_sevice.dart';
import 'package:santhwanam_plus/users/service_notification.dart';
import 'package:santhwanam_plus/users/serviceprovider.dart';
import 'package:santhwanam_plus/users/user_notification.dart';
import 'package:santhwanam_plus/users/userhome.dart';
import 'package:santhwanam_plus/users/profilecard.dart';
import 'package:santhwanam_plus/users/store.dart';


class UserDesign extends StatefulWidget {
  var uid;
  var uemail;
  var uphone;
  var uname;
  var imgurl;
  var dob;
  var address;
  var proof;
  var job;
  var bloodgroup;
  var pincode;

   UserDesign({Key? key,this.uid,this.uname,this.uphone,this.uemail,this.imgurl,this.dob,this.address,this.proof,this.job,this.bloodgroup,this.pincode,}) : super(key: key);

  @override
  _UserDesignState createState() => _UserDesignState();
}

class _UserDesignState extends State<UserDesign> {
   var _widget;


  int _selectedIndex = 0;
  void setdata(){
    _widget = [
      UserHome(uid: widget.uid,uemail: widget.uemail,uphone: widget.uphone,uname: widget.uname,imgurl: widget.imgurl,),
      UserProfile(uid: widget.uid,uemail: widget.uemail,uphone: widget.uphone,uname: widget.uname,dob:widget.dob,address:widget.address,
        proof: widget.proof,job: widget.job,bloodgroup: widget.bloodgroup,imgurl: widget.imgurl,pincode: widget.pincode,),
      Store(uid: widget.uid,uemail: widget.uemail,uphone: widget.uphone,uname: widget.uname,),
      ServiceProvider(uid: widget.uid,uemail: widget.uemail,uphone: widget.uphone,uname: widget.uname,),
    ];
  }

  List _appbarText = ['Santhwanam +', 'Profile', "Store", 'Services'];


  void onTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
@override
  void initState() {

    setdata();
    // TODO: implement initState
    super.initState();
  }
  @override

  Widget build(BuildContext context) {
    return Scaffold(
        drawer: _sideBar(),
        appBar: AppBar(
          title: Text(
            _appbarText[_selectedIndex],
          ),
          actions: [
            IconButton(onPressed: (){
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => service_notification(uid: widget.uid)));

            }, icon: Icon(Icons.comment)),

            IconButton(onPressed: (){
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => User_Notification(uid: widget.uid)));

            }, icon: Icon(Icons.notifications)),
            IconButton(
              icon: Icon(Icons.logout),
              color: Colors.white,
              tooltip: 'menu icon ',
              onPressed: () {
                FirebaseAuth.instance.signOut().then((value) =>
                    Navigator.pushReplacement(context,
                        MaterialPageRoute(builder: (context) =>
                            LoginPage(
                            ))));
              },
            ),
          ],
          backgroundColor: Color(0xffef5350),
          //leading: Icon(Icons.menu),
        ),
        bottomNavigationBar: BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Home',
              backgroundColor: Colors.white
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.perm_contact_cal_outlined),
              label: 'Profile',
              backgroundColor: Colors.white
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.accessible),
              label: 'Store',
              backgroundColor: Colors.white
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.volunteer_activism),
              label: 'Services',
              backgroundColor: Colors.white
            ),
          ],
          onTap: onTapped,
          currentIndex: _selectedIndex,
          //currentIndex: _selectedIndex,
          selectedItemColor:Color(0xffef5350),
          unselectedItemColor: Colors.blueGrey,
        ),
        body: Center(
            child: _widget.elementAt(
          _selectedIndex,
        )));
  }

  Drawer _sideBar() {
    return Drawer(
      child: ListView(
        children: [
          const SizedBox(
            height: 10,
          ),
          DrawerHeader(
            child: Container(
              width: double.infinity,
              padding: EdgeInsets.only(top: 10.0),
              decoration: BoxDecoration(borderRadius: BorderRadius.circular(20),color: Colors.white,border: Border.all(
                color: Color(0xff89d3fb))),
              child: Column(
                children: [
                  Row(
                    children: [
                      widget.imgurl==null?   Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          margin: EdgeInsets.only(bottom: 20),
                          height: 70,width: 70,
                          decoration: const BoxDecoration(color: Colors.red,
                              shape: BoxShape.circle,
                            image:DecorationImage(
                              image: AssetImage('images/jobin.png'),


                            )

                          ),

                        ),
                      ):Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                            margin: EdgeInsets.only(bottom: 20),
                            height: 70,width: 70,
                            child: Image.network(widget.imgurl)),
                      ),
                      SizedBox(width: 20,),
                      Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(bottom: 20),
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    Text(widget.uname,style: TextStyle(fontFamily: "RobotoSlab-VariableFont_wght.ttf",fontWeight: FontWeight.bold),)
                                  ],
                                ),
                                SizedBox(height: 10,),
                                // Row(
                                //   mainAxisAlignment: MainAxisAlignment.start,
                                //   children: [
                                //     Icon(Icons.email,size: 20,),
                                //
                                //     Text(widget.uemail,)
                                //   ],
                                // ),
                                Row(
                                  children: [
                                    Icon(Icons.phone_android_outlined),

                                    SizedBox(width: 4,),
                                    Text(widget.uphone),
                                  ],
                                )

                              ],

                            ),
                          )

                        ],
                      ),

                    ],
                  ),


                ],
              ),
            ),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
              ),
          ),
          const SizedBox(
            height: 10,
          ),
          ListTile(
            title: Row(
              children: const [
               Icon(Icons.person_rounded),
            SizedBox(width:10),
            Text(
                  "profile setup",
                  style: TextStyle(fontFamily: 'RobotoSlab-VariableFont_wght.ttf'),
                ),
              ],
            ),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => ProfileSetup(
                    uid: widget.uid,
                    name: widget.uname,
                    email: widget.uemail,
                    phone: widget.uphone,
                    imgurl:widget.imgurl ,
                  )));
            },
          ),
          ListTile(
            title: Row(
              children: const [
                Icon(Icons.bubble_chart),
                SizedBox(width:10),
                Text(
                  "Blood Bank",
                  style: TextStyle(fontFamily: 'RobotoSlab-VariableFont_wght.ttf'),
                ),
              ],
            ),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => BloodBank(
                    uid: widget.uid,
                    name: widget.uname,
                    phone:widget.uphone,

                  )));
            },
          ),
          ListTile(
            title: Row(
              children: const [
                Icon(Icons.miscellaneous_services),
                SizedBox(width:10),
                Text("Request&service",
                    style:
                        TextStyle(fontFamily: 'RobotoSlab-VariableFont_wght.ttf')),
              ],
            ),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => ServiceRequest(uid: widget.uid,uname:widget.uname)));
            },
          ),
          ListTile(
            title: Row(
              children: const [
                Icon(Icons.local_hospital_outlined),
                SizedBox(width:10),
                Text("Ambulance booking ",
                    style:
                        TextStyle(fontFamily: 'RobotoSlab-VariableFont_wght.ttf')),
              ],
            ),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => Ambulance()));
            },
          ),
          ListTile(
            title: Row(
              children: const [
                Icon(Icons.rate_review_rounded),
                SizedBox(width:10),
                Text("Rate&Feedback ",
                    style:
                        TextStyle(fontFamily: 'RobotoSlab-VariableFont_wght.ttf')),
              ],
            ),
            onTap: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Rating()));
            },
          ),
          /*ListTile(
            title: Row(
              children: const [
                Icon(Icons.history),
                SizedBox(width: 10,),
                Text("History",
                    style:
                        TextStyle(fontFamily: 'RobotoSlab-VariableFont_wght.ttf')),
              ],
            ),
            onTap: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => History()));
            },
          ),*/
        ],
      ),
    );
  }
}
