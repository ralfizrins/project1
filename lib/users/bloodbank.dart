import 'package:flutter/material.dart';

import 'package:santhwanam_plus/users/donate.dart';

import 'request.dart';

class BloodBank extends StatefulWidget {
  var uid;
  var name;
  var phone;
  //var email;
  BloodBank({Key? key,this.uid,this.name,this.phone}) : super(key: key);
  @override
  State<BloodBank> createState() => _BloodBankState();
}

class _BloodBankState extends State<BloodBank> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Color(0xffef5350),
            title: const Text(
              'Bood Bank',
              style: TextStyle(fontFamily: 'RobotoSlab-VariableFont_wght.ttf'),
            ),
            bottom: const TabBar(
              indicatorColor: Colors.white,
              tabs: [
                Tab(icon: Icon(Icons.add_box_sharp), text: "Donate"),
                Tab(icon: Icon(Icons.request_quote_rounded), text: "Request")
              ],
            ),
          ),
          body: TabBarView(
            children: [
              Donation(uid: widget.uid,name: widget.name,phone: widget.phone),
              Request(uid: widget.uid, name: widget.name, phone:widget.phone,),
            ],
          ),
        ),
      ),
    );
  }
}
