import 'package:flutter/material.dart';
import 'contributionscreen1.dart';
import 'contributionscreen2.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';

void main() => runApp(Contributions());

class Contributions extends StatefulWidget {
  var uid;
  var uname;
  var uphone;
  Contributions({Key? key,this.uid,this.uname,this.uphone}) : super(key: key);

  @override
  State<Contributions> createState() => _ContributionsState();
}

class _ContributionsState extends State<Contributions> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Color(0xffef5350),
            title: Text('Contributions',style: TextStyle(fontFamily: 'RobotoSlab-VariableFont_wght.ttf'),),
            bottom: const TabBar(
              indicatorColor: Colors.white,
              tabs: [

                Tab(icon: Icon(Icons.attach_money_sharp), text: "Money "),
                Tab(icon: Icon(Icons.add_box), text: "Equipments")
              ],
            ),
          ),
          body: TabBarView(
            children: [
              GiveMoney(),
              GiveThings(uid: widget.uid,uname: widget.uname,uphone: widget.uphone,),
            ],
          ),
        ),
      ),
    );
  }
}