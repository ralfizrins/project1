import 'package:flutter/material.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';

class UserProfile extends StatefulWidget {
  var uid;
  var uemail;
  var uphone;
  var uname;
  var imgurl;
  var dob;
  var address;
  var proof;
  var job;
  var bloodgroup;
  var pincode;
   UserProfile({Key? key,this.uid,this.uname,this.uphone,this.uemail,this.imgurl,this.dob,this.address,this.proof,this.job,this.bloodgroup,this.pincode}) : super(key: key);

  @override
  _UserProfileState createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  var _profilekey = GlobalKey<FormState>();
  TextEditingController emailInputcontroller = TextEditingController();
  TextEditingController phone = TextEditingController();
  TextEditingController name = TextEditingController();
  TextEditingController address = TextEditingController();
  TextEditingController proof = TextEditingController();
  TextEditingController job = TextEditingController();
  TextEditingController blood = TextEditingController();

  var bloodgroup;
  var assume="Value not found";//for demo
  late TextEditingController _takedob; //For late initialisation:- late Keyword
  late String _valueChanged3;
  late String _valueToValidate3;
  late String _valueSaved3;
  final ImagePicker _picker = ImagePicker();// For pick Image
  XFile ? _image; // For accept Null:-?

  void initState() {
    // TODO: implement initState
    _takedob=new TextEditingController(text:DateTime.now().toString());


    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    var visible=true;

    return Scaffold(
      body: SafeArea(
        child:SingleChildScrollView(
          child: Form(
            key: _profilekey,
           child: Column(
             children: [
               SizedBox(height: 30,),
               Center(
                 child: widget.imgurl!=null?
                 Container(
                         margin: EdgeInsets.only(bottom: 20),
                         height: 180,width: 180,
                         decoration: BoxDecoration(
                             shape: BoxShape.circle,
                             image:DecorationImage(
                               image: NetworkImage(widget.imgurl),
                             )),
                 ):Icon(Icons.account_circle),
               ),
               Text(widget.uname,style:TextStyle(fontWeight: FontWeight.bold,fontFamily: 'RobotoSlab-VariableFont_wght.ttf')),
               SizedBox(height: 10,),
               Container(
                 width: 380,height: 400,
                 child: Padding(
                   padding: const EdgeInsets.all(8.0),
                   child: Card(
                     shadowColor: Colors.grey,elevation: 2.0,
                     shape:RoundedRectangleBorder(
                       borderRadius: BorderRadius.circular(20.0),
                     ),color: Colors.white,
                     child: Container(
                      child: Column(
                        children: [
                          SizedBox(height: 20,),
                          Row(
                            children: [
                              const Padding(
                                padding: EdgeInsets.all(8.0),
                                child: Text("Email :",style: TextStyle(fontSize: 20),),
                              ),
                              Text(widget.uemail)
                            ],
                          ),

                          Row(
                            children: [
                              const Padding(
                                padding: EdgeInsets.all(8.0),
                                child: Text("Phone :",style: TextStyle(fontSize: 20),),
                              ),
                              Text(widget.uphone)
                            ],
                          ),
                          Row(
                            children: [
                              const Padding(
                                padding: EdgeInsets.all(8.0),
                                child: Text("DOB:",style: TextStyle(fontSize: 20),),
                              ),
                              widget.dob!=null?
                              Text(widget.dob):Text(assume),
                            ],
                          ),
                          Row(
                            children: [
                              const Padding(
                                padding: EdgeInsets.all(8.0),
                                child: Text("Address:",style: TextStyle(fontSize: 20),),
                              ),
                              widget.address!=null?
                              Text(widget.address):Text(assume),
                            ],
                          ),
                          Row(
                            children: [
                              const Padding(
                                padding: EdgeInsets.all(8.0),
                                child: Text("Pincode:",style: TextStyle(fontSize: 20),),
                              ),
                              widget.pincode!=null?
                              Text(widget.pincode):Text(assume),

                            ],
                          ),
                          Row(
                            children: [
                              const Padding(
                                padding: EdgeInsets.all(8.0),
                                child: Text("Proof:",style: TextStyle(fontSize: 20),),
                              ),
                              widget.proof!=null?
                              Text(widget.proof):Text(assume),

                            ],
                          ),
                          Row(
                            children: [
                              const Padding(
                                padding: EdgeInsets.all(8.0),
                                child: Text("Bloodgroup:",style: TextStyle(fontSize: 20),),
                              ),
                              widget.bloodgroup!=null?
                              Text(widget.bloodgroup):Text(assume),

                            ],
                          ),
                          Row(
                            children: [
                              const Padding(
                                padding: EdgeInsets.all(8.0),
                                child: Text("job:",style: TextStyle(fontSize: 20),),
                              ),
                              widget.job!=null?
                              Text(widget.job):Text(assume),

                            ],
                          ),

                        ],
                      ),
                     ),
                   ),
                 ),
               ),



             ],

           ),
          ),
        ),

      ),
    );
  }

  _imgbrowse()async{
    final XFile? photo = await _picker.pickImage(source: ImageSource.camera);
    //final XFile? image = await _picker.pickImage(source: ImageSource.gallery);
    setState(() {
      _image=photo;
    });
  }
  _imggallery()async{
    //final XFile? photo = await _picker.pickImage(source: ImageSource.camera);
    final XFile? image = await _picker.pickImage(source: ImageSource.gallery);
    setState(() {
      _image=image;
    });}
  _getpics(){
    showModalBottomSheet(
        context: context,
        builder: (context){
          return Container(
            child: Wrap(
              children: [
                Expanded(
                  child: ListTile(
                      title: Text("Photo Gallery"),
                      leading: Icon(Icons.image),
                      onTap: (){
                        _imggallery();
                      }

                  ),
                ),
                Expanded(
                  child: ListTile(
                    title: Text("Camera"),
                    leading: Icon(Icons.camera),
                    onTap: (){
                      _imgbrowse();
                    },
                  ),
                ),

              ],

            ),

          );
        }
    );
  }


}

