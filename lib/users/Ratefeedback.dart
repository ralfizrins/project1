import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:santhwanam_plus/users/userdesign.dart';
import 'package:santhwanam_plus/users/userhome.dart';
import 'package:uuid/uuid.dart';

import '../validation.dart';

// void main() => runApp(Rating());

class Rating extends StatefulWidget {
  @override

  _RatingState createState() => _RatingState();
}

class _RatingState extends State<Rating> {
  var uuid = Uuid();
  var feedbackid;
  late final _ratingController;
  late double _rating;

  double _userRating = 3.0;
  int _ratingBarMode = 1;
  double _initialRating = 2.0;
  bool _isRTLMode = false;
  bool _isVertical = false;

  IconData? _selectedIcon;
  TextEditingController feedback = TextEditingController();
  TextEditingController textinputcontroller = TextEditingController();
  bool visible = true;

  var _ratingkey = GlobalKey<FormState>();
  @override
  void initState() {
    feedbackid=uuid.v1();
    super.initState();
    _ratingController = TextEditingController(text: '3.0');
    _rating = _initialRating;
  }

  @override
  Widget build(BuildContext context) {
    return  Scaffold(

          appBar: AppBar(
            backgroundColor: Color(0xffef5350),
            title: Text(' Rating&Feedback',style: TextStyle(fontFamily: 'RobotoSlab-VariableFont_wght.ttf'),),

          ),
          body: Directionality(
            textDirection: _isRTLMode ? TextDirection.rtl : TextDirection.ltr,
            child: SingleChildScrollView(
              child: Form(
                key: _ratingkey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    SizedBox(
                      height: 40.0,
                    ),
                    Center(child: Text('What is you rate?',style: TextStyle(fontFamily: 'RobotoSlab-VariableFont_wght.ttf',fontSize: 20),)),
                    SizedBox(height: 30,),
                    _ratingBar(_ratingBarMode),
                    Text(
                      'Rating: $_rating',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 30,),
                    Text("Please share your opinion ",style: TextStyle(fontFamily: 'RobotoSlab-VariableFont_wght.ttf',fontSize: 20),),
                    Text("About our services ",style: TextStyle(fontFamily: 'RobotoSlab-VariableFont_wght.ttf',fontSize: 20),),
                    SizedBox(height: 20,),

                    Container(
                      height: 180,width: 280,
                      child: TextFormField(
                        controller: textinputcontroller,
                        cursorColor: Colors.grey,
                        keyboardType: TextInputType.multiline,
                        maxLines: 10,
                        decoration: InputDecoration(
                          hintStyle: TextStyle(fontSize: 13,fontFamily: 'RobotoSlab-VariableFont_wght.ttf',color: Color(0xffc5c5c5)),
                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(color: Colors.redAccent),
                          )
                        ),
                        validator: (value) {
                          return Validate.namevalidator(value!.trim());
                        },
                      ),
                    ),
                    SizedBox(height:40),
                    Padding(
                      padding: const EdgeInsets.only(left: 50),
                      child: Row(
                        children: [
                          ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              fixedSize: Size(100,30),
                              primary: Color(0xffef5350),
                                shape: new RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10),
                                )
                            ),
                              onPressed: (){
                                Navigator.pop(context);

                              },
                              child: Text("Back")),
                          SizedBox(width: 30,),
                          ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  fixedSize: Size(100,30),
                                  primary: Color(0xffef5350),
                                  shape: new RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  )
                              ),
                              onPressed: ()
                              {
                                if (_ratingkey.currentState!.validate())
                                  FirebaseFirestore.instance.collection('userfeedback').doc(feedbackid).
                                  set(
                                      {'feedback_id':feedbackid,
                                        'need':textinputcontroller.text,
                                        'status':1,
                                        'rating':_rating,
                                        'date':DateTime.now()
                                      }
                                  ).then((value) {
                                    showsnackbar(context,"Thank you for your feedback");
                                    // Navigator.push(
                                    //     context,
                                    //     MaterialPageRoute(
                                    //         builder: (context) => UserHome()));
                                    //Navigator.pop(context);


                                  });



                              },
                              child: Text("Submit")),
                        ],
                      ),
                    )

                  ],
                ),
              ),
            ),
          ),
        );

  }

  Widget _ratingBar(int mode) {
    switch (mode) {
      case 1:
        return RatingBar.builder(
          initialRating: _initialRating,
          minRating: 1,
          direction: _isVertical ? Axis.vertical : Axis.horizontal,
          allowHalfRating: true,
          unratedColor: Color(0xffef5350).withAlpha(50),
          itemCount: 5,
          itemSize: 50.0,
          itemPadding: EdgeInsets.symmetric(horizontal: 4),
          itemBuilder: (context, _) => Icon(
            _selectedIcon ?? Icons.favorite,size: 10,
            color: Color(0xffef5350),
          ),
          onRatingUpdate: (rating) {
            setState(() {
              _rating = rating;
            });
          },
          updateOnDrag: true,
        );
      case 2:
        return RatingBar(
          initialRating: _initialRating,
          direction: _isVertical ? Axis.vertical : Axis.horizontal,
          allowHalfRating: true,
          itemCount: 5,
          ratingWidget: RatingWidget(
            full: _image('assets/heart.png'),
            half: _image('assets/heart_half.png'),
            empty: _image('assets/heart_border.png'),
          ),
          itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
          onRatingUpdate: (rating) {
            setState(() {
              _rating = rating;
            });
          },
          updateOnDrag: true,
        );
      case 3:
        return RatingBar.builder(
          initialRating: _initialRating,
          direction: _isVertical ? Axis.vertical : Axis.horizontal,
          itemCount: 5,
          itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
          itemBuilder: (context, index) {
            switch (index) {
              case 0:
                return Icon(
                  Icons.sentiment_very_dissatisfied,
                  color: Colors.red,
                );
              case 1:
                return Icon(
                  Icons.sentiment_dissatisfied,
                  color: Colors.redAccent,
                );
              case 2:
                return Icon(
                  Icons.sentiment_neutral,
                  color: Colors.amber,
                );
              case 3:
                return Icon(
                  Icons.sentiment_satisfied,
                  color: Colors.lightGreen,
                );
              case 4:
                return Icon(
                  Icons.sentiment_very_satisfied,
                  color: Colors.green,
                );
              default:
                return Container();
            }
          },
          onRatingUpdate: (rating) {
            setState(() {
              _rating = rating;
            });
          },
          updateOnDrag: true,
        );
      default:
        return Container();
    }
  }

  Widget _image(String asset) {
    return Image.asset(
      asset,
      height: 30.0,
      width: 30.0,
      color: Colors.amber,
    );
  }

  showsnackbar(BuildContext context, String value) {
    ScaffoldMessenger.of(context).showSnackBar( SnackBar(
        behavior: SnackBarBehavior.floating,
        backgroundColor: Colors.red,
        margin: EdgeInsetsDirectional.all(10),
        content: Text(
          value,
          style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
        )));
  }
}

