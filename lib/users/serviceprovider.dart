import 'package:flutter/material.dart';
import 'package:santhwanam_plus/users/akshaya.dart';
import 'package:santhwanam_plus/users/doctors.dart';
import 'package:santhwanam_plus/users/hospital_details.dart';
import 'package:santhwanam_plus/users/institutions.dart';
import 'package:santhwanam_plus/users/kseb.dart';
import 'package:santhwanam_plus/users/sysvolunteersdetails.dart';

class ServiceProvider extends StatefulWidget {
  var uid;
  var uemail;
  var uphone;
  var uname;
   ServiceProvider({Key? key,this.uid,this.uname,this.uphone,this.uemail}) : super(key: key);

  @override
  _ServiceProviderState createState() => _ServiceProviderState();
}

class _ServiceProviderState extends State<ServiceProvider> {
  var _serviceskey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xfff8f8f8),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Form(
              key: _serviceskey,
              child: Column(
                children: [
                  const Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Padding(
                      padding: EdgeInsets.only(right: 210),
                      child: Text("Services",
                          style: TextStyle(
                              fontFamily: 'RobotoSlab-VariableFont_wght.ttf',
                              fontSize: 20,
                              fontWeight: FontWeight.bold)),
                    ),
                  ),
                  SizedBox(height: 20),
                  Row(
                    children: [
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => HospitalDetails()));
                        },
                        child: Container(
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 20),
                                child: Image.asset(
                                  'images/hospitalimage-removebg-preview.png',
                                  width: 120,
                                  height: 120,
                                ),
                              ),
                              const Text("Hospitals",
                                  style: TextStyle(
                                      fontFamily:
                                          'RobotoSlab-VariableFont_wght.ttf',
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold)),
                            ],
                          ),
                          width: 160,
                          height: 200,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12),

                              color: Colors.white),
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => Doctors_Details()));
                        },
                        child: Container(
                            child: Column(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(top: 20),
                                  child: Image.asset(
                                    'images/doctors-removebg-preview.png',
                                    width: 120,
                                    height: 120,
                                  ),
                                ),
                                const Text("Doctors",
                                    style: TextStyle(
                                        fontFamily:
                                            'RobotoSlab-VariableFont_wght.ttf',
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold)),
                              ],
                            ),
                            width: 160,
                            height: 200,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(12),
                                color: Colors.white)),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: [
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => Institutions_Details()));
                        },

                        child: Container(
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 20),
                                child: Image.asset(
                                  'images/institutions-removebg-preview.png',
                                  width: 120,
                                  height: 120,
                                ),
                              ),
                              const Text("Institutions",
                                  style: TextStyle(
                                      fontFamily:
                                          'RobotoSlab-VariableFont_wght.ttf',
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold)),
                            ],
                          ),
                          width: 160,
                          height: 200,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12),
                              color: Colors.white),
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => Akshaya_Details ()));
                        },


                        child: Container(
                            child: Column(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(top: 20),
                                  child: Image.asset(
                                    'images/akshaya-removebg-preview.png',
                                    width: 120,
                                    height: 120,
                                  ),
                                ),
                                const Text("Akshaya",
                                    style: TextStyle(
                                        fontFamily:
                                            'RobotoSlab-VariableFont_wght.ttf',
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold)),
                              ],
                            ),
                            width: 160,
                            height: 200,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(12),
                                color: Colors.white)),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: [
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>  Kseb_Details()));
                        },

                        child: Container(
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 20),
                                child: Image.asset(
                                  'images/KSEB-logo-removebg-preview.png',
                                  width: 120,
                                  height: 120,
                                ),
                              ),
                              const Text("kSEB",
                                  style: TextStyle(
                                      fontFamily:
                                          'RobotoSlab-VariableFont_wght.ttf',
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold)),
                            ],
                          ),
                          width: 160,
                          height: 200,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12),
                              color: Colors.white),
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>  Sys_Volunteers_Details()));
                        },


                        child: Container(
                            child: Column(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(top: 20),
                                  child: Image.asset(
                                    'images/sysvolunteers-removebg-preview.png',
                                    width: 120,
                                    height: 120,
                                  ),
                                ),
                                const Text("Volunteers",
                                    style: TextStyle(
                                        fontFamily:
                                            'RobotoSlab-VariableFont_wght.ttf',
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold)),
                              ],
                            ),
                            width: 160,
                            height: 200,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(12),
                                color: Colors.white)),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
