import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:santhwanam_plus/users/userhome.dart';
import 'package:santhwanam_plus/users/userdesign.dart';
import 'package:uuid/uuid.dart';

import '../validation.dart';

class ServiceRequest extends StatefulWidget {
  var uid,uname;
  ServiceRequest({Key? key,this.uid,this.uname}) : super(key: key);

  @override
  _ServiceRequestState createState() => _ServiceRequestState();

}

class _ServiceRequestState extends State<ServiceRequest> {
  var uuid = Uuid();
  var serviceid;
  var _requestservicekey = GlobalKey<FormState>();
  //TextEditingController  = TextEditingController();
  TextEditingController pincode = TextEditingController();
  TextEditingController ref_number= TextEditingController();
  var equipment;

  @override
  void initState() {
    serviceid=uuid.v1();
    //name.text=widget.name;
    //phone.text=widget.phone;

    // TODO: implement initState
    super.initState();
  }
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xffef5350),
      ),
      body: SingleChildScrollView(
        child: SafeArea(
          child:Form(
            key: _requestservicekey,
            child: Column(
              children: [
                SizedBox(height: 30,),
                const Text("Request to a service",
                  style: TextStyle(color: Colors.black,fontSize: 20,
                    fontWeight: FontWeight.bold,
                    fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                  ),),
                SizedBox(height: 20,),
                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Container(width: 400,height: 400,
                    child: Card(shadowColor: Colors.grey,elevation: 2.0,
                      shape:RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      ),color: Colors.white,
                      child: Container(
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Column(
                            children: [

                              SizedBox(height:20),
                              DropdownButtonFormField<String>(

                                value: equipment,
                                decoration: const InputDecoration(
                                  hintText: "Equipment",

                                ),
                                onChanged: (ctgry) =>
                                    setState(() => equipment = ctgry),
                                validator: (value) => value == null ? 'field required' : null,
                                items: ['WHEELCHAIR','WATERBED','HOSPITAL BED','STICK','OXYGEN',]
                                    .map<DropdownMenuItem<String>>(
                                        (String value) {
                                      return DropdownMenuItem<String>(
                                        value: value,
                                        child: Text(value),
                                      );
                                    }).toList(),
                              ),//blood

                             /* TextFormField(
                                keyboardType: TextInputType.multiline,
                                maxLines: null,
                                controller: description,
                                decoration: InputDecoration(
                                  hintText: 'Put your need',
                                  hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                                ),
                                validator: (value) {
                                  return Validate.addressvalidator(value!.trim());
                                },),//address_Multilined*/

                              TextFormField(
                                controller: pincode,
                                cursorColor: Colors.black,
                                keyboardType: TextInputType.number,
                                decoration: const InputDecoration(
                                  hintText: 'Pincode',
                                  hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                                ),
                                validator: (value) {
                                  return Validate.pincodevalidator(value!.trim());
                                },
                              ), //pincode
                              TextFormField(
                                controller: ref_number,
                                cursorColor: Colors.black,
                                keyboardType: TextInputType.number,
                                decoration: const InputDecoration(
                                  hintText: 'Reference Contact',
                                  hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                                ),
                                validator: (value) {
                                  return Validate.phvalidator(value!.trim());
                                },
                                 ), //proof
                              SizedBox(height: 60,),
                              ElevatedButton(
                                  onPressed: () {
                                    if (_requestservicekey.currentState!.validate()) {
                                      FirebaseFirestore.instance.collection('servicerequest').doc(serviceid).
                                      set(
                                          {'sevice_id':serviceid,
                                            'uid':widget.uid,
                                            'usere_name':widget.uname,
                                            'need':equipment,
                                            'pincode':pincode.text,
                                            'phone':ref_number.text,
                                            'status':1,
                                            'service status':0,
                                            'date':DateTime.now()
                                          }
                                      ).then((value) {
                                        showsnackbar(context,"Requested successfully");

                                        Navigator.pop(context);
                                      });
                                    }
                                    },
                                  style: ElevatedButton.styleFrom(
                                    primary: Color(0xffef5350),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: new BorderRadius.circular(15),
                                    ),
                                  ),
                                  child: const Text(
                                    "Submit",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                                    ),
                                  )),
                              //SizedBox(height:15),


                            ],
                          ),
                        ),


                      ),
                    ),
                  ),
                ),

              ],
            ),
          ),

        ),
      ),
    );
  }

  showsnackbar(BuildContext context, String value) {
    ScaffoldMessenger.of(context).showSnackBar( SnackBar(
        behavior: SnackBarBehavior.floating,
        backgroundColor: Colors.red,
        margin:EdgeInsetsDirectional.all(10),
        content: Text(
          value,
          style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
        )));
  }
}

