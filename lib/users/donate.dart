import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:santhwanam_plus/users/userdesign.dart';
import 'package:santhwanam_plus/users/userhome.dart';
import 'package:santhwanam_plus/validation.dart';
import 'package:uuid/uuid.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';

import 'loginpage.dart';

class Donation extends StatefulWidget {
  var uid;
  var name;
  var phone;
  Donation({Key? key, this.uid, this.name, this.phone}) : super(key: key);

  @override
  _DonationState createState() => _DonationState();
}

class _DonationState extends State<Donation> {
  var uuid = Uuid();
  var did;
  var _donationkey = GlobalKey<FormState>();
  var bloodgroup;
  TextEditingController place = TextEditingController();
  TextEditingController phone = TextEditingController();
  TextEditingController name = TextEditingController();
  @override
  void initState() {
    did = uuid.v1();
    name.text = widget.name;
    phone.text = widget.phone;

    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var visible = true;
    var category;
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Form(
            key: _donationkey,
            child: Column(
              children: [
                const SizedBox(
                  height: 20,
                ),
                const Text('Donate Blood',
                    style: TextStyle(
                        fontSize: 24,
                        fontFamily: 'RobotoSlab-VariableFont_wght.ttf',
                        fontWeight: FontWeight.bold)),
                const SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: SingleChildScrollView(
                    child: Container(
                      width: 340,
                      height: 450,
                      child: Card(
                        shadowColor: Colors.grey,
                        elevation: 2.0,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                        color: Colors.white,
                        child: Container(
                          child: Padding(
                            padding: const EdgeInsets.all(20.0),
                            child: Column(
                              children: [
                                SizedBox(height: 20),

                                TextFormField(
                                  decoration: const InputDecoration(
                                    hintText: "Name",
                                    hintStyle: TextStyle(
                                        fontFamily:
                                            "RobotoSlab-VariableFont_wght.ttf"),
                                  ),
                                  controller: name,
                                  readOnly: true,
                                  cursorColor: Colors.black,
                                  //keyboardType: TextInputType.text,
                                  validator: (value) {
                                    return Validate.namevalidator(
                                        value!.trim());
                                  },
                                ),
                                SizedBox(height: 10),
                                TextFormField(
                                  controller: place,
                                  cursorColor: Colors.black,
                                  keyboardType: TextInputType.text,
                                  decoration: const InputDecoration(
                                    hintText: 'place',
                                    hintStyle: TextStyle(
                                        fontFamily:
                                            "RobotoSlab-VariableFont_wght.ttf"),
                                  ),
                                  validator: (value) {
                                    return Validate.placevalidator(
                                        value!.trim());
                                  },
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                DropdownButtonFormField<String>(
                                  value: bloodgroup,
                                  decoration: const InputDecoration(
                                    hintText: "Blood Group",


                                  ),
                                  onChanged: (ctgry) =>
                                      setState(() => bloodgroup = ctgry),
                                  validator: (value) =>
                                      value == null ? 'field required' : null,
                                  items: [
                                    'A+',
                                    'A-',
                                    'B+',
                                    'B-',
                                    'AB+',
                                    'AB-',
                                    'O+',
                                    'O-'
                                  ].map<DropdownMenuItem<String>>(
                                      (String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(value),
                                    );
                                  }).toList(),
                                ), //blood
                                const SizedBox(
                                  height: 10,
                                ),
                                TextFormField(
                                  readOnly: true,
                                  controller: phone,
                                  cursorColor: Colors.black,
                                  keyboardType: TextInputType.number,
                                  decoration: const InputDecoration(
                                    hintText: 'Mobile Number',
                                    hintStyle: TextStyle(
                                        fontFamily:
                                            "RobotoSlab-VariableFont_wght.ttf"),
                                  ),
                                  validator: (value) {
                                    return Validate.phonevalidator(
                                        value!.trim());
                                  },
                                ),
                                const SizedBox(
                                  height: 30,
                                ),
                                ElevatedButton(
                                    onPressed: () {
                                      if (_donationkey.currentState!.validate()) {
                                        FirebaseFirestore.instance
                                            .collection('donation')
                                            .doc(did)
                                            .set({
                                          'don_id': did,
                                          //'userid':widget.name,
                                          'name': widget.name,
                                          'place': place.text,
                                          'phone': widget.phone,
                                          'blood': bloodgroup,
                                          'status': 1,
                                          'date': DateTime.now()
                                        }).then((value) {

                                          showsnackbar(
                                              context, "Donated successfully");
                                        });
                                      }


                                    },
                                    style: ElevatedButton.styleFrom(
                                      primary: Color(0xffef5350),
                                      shape:  RoundedRectangleBorder(
                                        borderRadius:
                                             BorderRadius.circular(15),
                                      ),
                                    ),
                                    child: const Text(
                                      "  Submit  ",
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontFamily:
                                            "RobotoSlab-VariableFont_wght.ttf",
                                      ),
                                    )),
                                SizedBox(height: 15),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  showsnackbar(BuildContext context, String value) {
    ScaffoldMessenger.of(context).showSnackBar( SnackBar(
        behavior: SnackBarBehavior.floating,
        backgroundColor: Colors.red,
        margin: EdgeInsetsDirectional.all(10),
        content: Text(
          value,
          style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
        )));
  }
}
