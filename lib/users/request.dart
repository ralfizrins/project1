import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'package:santhwanam_plus/users/userdesign.dart';
import 'package:santhwanam_plus/users/userhome.dart';
import 'package:santhwanam_plus/validation.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:uuid/uuid.dart';

import 'loginpage.dart';

class Request extends StatefulWidget {
  var uid;
  var name;
  var phone;

   Request({Key? key,this.uid,this.name,this.phone}) : super(key: key);

  @override
  _RequestState createState() => _RequestState();
}

class _RequestState extends State<Request> {
  var uuid = Uuid();
  var reqid;
  var _requestkey = GlobalKey<FormState>();
  var bloodgroup;
  TextEditingController place = TextEditingController();
  TextEditingController phone = TextEditingController();
  TextEditingController name = TextEditingController();
  TextEditingController need = TextEditingController();

  @override
  void initState() {
    reqid=uuid.v1();
    name.text=widget.name;
    phone.text=widget.phone;

    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    var visible = true;
    var category;
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Form(
            key: _requestkey,
            child: Column(
              children: [
                const SizedBox(
                  height: 20,
                ),
                Text('Blood Request',style: TextStyle(fontSize: 24,fontFamily: 'RobotoSlab-VariableFont_wght.ttf',fontWeight: FontWeight.bold)),
                const SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: SingleChildScrollView(
                    child: Container(
                      width: 340,height: 500,
                      child: Card(
                        shadowColor: Colors.grey,
                        elevation: 2.0,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                        color: Colors.white,
                        child: Container(
                          child: Padding(
                            padding: const EdgeInsets.all(20.0),
                            child: Column(
                              children: [
                                SizedBox(height: 20),

                                TextFormField(
                                  controller: need,
                                  decoration: const InputDecoration(
                                    hintText: "Need",
                                    hintStyle: TextStyle(
                                        fontFamily:
                                        "RobotoSlab-VariableFont_wght.ttf"),
                                  ),

                                  cursorColor: Colors.black,
                                  keyboardType: TextInputType.text,
                                  validator: (value) {
                                    return Validate.namevalidator(value!.trim());
                                  },
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                TextFormField(
                                  controller:name ,
                                  readOnly: true,
                                  cursorColor: Colors.black,
                                  keyboardType: TextInputType.text,
                                  decoration: const InputDecoration(
                                    hintText: 'Name',
                                    hintStyle: TextStyle(
                                        fontFamily:
                                        "RobotoSlab-VariableFont_wght.ttf"),
                                  ),
                                  validator: (value) {
                                    return Validate.namevalidator(value!.trim());
                                  },
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                DropdownButtonFormField<String>(
                                  value: bloodgroup,
                                  decoration: const InputDecoration(
                                    hintText: "Blood Group",
                                    //prefixIcon: Icon(Icons.email),
                                    //suffixIcon: Icon(Icons.panorama_fish_eye),
                                    //border: OutlineInputBorder(borderRadius: BorderRadius.circular(30),
                                  ),
                                  onChanged: (ctgry) =>
                                      setState(() => bloodgroup = ctgry),
                                  validator: (value) => value == null ? 'field required' : null,
                                  items: ['A+','A-','B+','B-','AB+','AB-','O+','O-']
                                      .map<DropdownMenuItem<String>>(
                                          (String value) {
                                        return DropdownMenuItem<String>(
                                          value: value,
                                          child: Text(value),
                                        );
                                      }).toList(),
                                ),//blood
                                const SizedBox(
                                  height: 10,
                                ),
                                TextFormField(
                                  controller: place,
                                  cursorColor: Colors.black,
                                  keyboardType: TextInputType.text,
                                  decoration: const InputDecoration(
                                    hintText: 'Place',
                                    hintStyle: TextStyle(
                                        fontFamily:
                                        "RobotoSlab-VariableFont_wght.ttf"),
                                  ),
                                  validator: (value) {
                                    return Validate.namevalidator(value!.trim());
                                  },
                                ),
                                SizedBox(height:10),
                                TextFormField(
                                  controller: phone,
                                  readOnly: true,
                                  cursorColor: Colors.black,
                                  keyboardType: TextInputType.number,
                                  decoration: const InputDecoration(
                                    hintText: 'Mobile Number',
                                    hintStyle: TextStyle(
                                        fontFamily:
                                        "RobotoSlab-VariableFont_wght.ttf"),
                                  ),
                                  validator: (value) {
                                    return Validate.phonevalidator(value!.trim());
                                  },
                                ),

                                const SizedBox(
                                  height: 30,
                                ),
                                ElevatedButton(
                                    onPressed: ()
                                      {
                                        if (_requestkey.currentState!.validate()) {
                                          FirebaseFirestore.instance.collection('bloodrequest').doc(reqid).
                                          set(
                                              {'req_id':reqid,
                                                'userid':widget.uid,
                                                'name':widget.name,
                                                'need':need.text,
                                                'place':place.text,
                                                'phone':widget.phone,
                                                'blood':bloodgroup,
                                                'status':1,
                                                'bstatus':0,
                                                'date':DateTime.now()
                                              }
                                          ).then((value) {
                                            showsnackbar(context,"Requested successfully");




                                          });
                                        }




                                        /*Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => UserDesign()));*/
                                       //showsnackbar(context,"Thank you for your Request");



                                    },
                                    style: ElevatedButton.styleFrom(
                                      primary: Color(0xffef5350),
                                      shape:  RoundedRectangleBorder(
                                        borderRadius:
                                         BorderRadius.circular(15),
                                      ),
                                    ),
                                    child: const Text(
                                      "  submit  ",
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontFamily:
                                        "RobotoSlab-VariableFont_wght.ttf",
                                      ),
                                    )),
                                SizedBox(height: 15),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),

                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
  showsnackbar(BuildContext context, String value) {
    ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(behavior: SnackBarBehavior.floating,
            backgroundColor: Colors.red,
            margin:EdgeInsetsDirectional.all(10),
            content: Text(
              value,
              style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
            )));
  }
}
