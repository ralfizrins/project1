import 'package:flutter/material.dart';

import 'akshaya.dart';

import 'doctors.dart';

import 'hospital_details.dart';

import 'institutions.dart';

import 'kseb.dart';

import 'sysvolunteersdetails.dart';

class Services extends StatefulWidget {
  const Services({Key? key}) : super(key: key);

  @override
  _ServicesState createState() => _ServicesState();
}

class _ServicesState extends State<Services> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xffef5350),
      ),

      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Column(
              children: [
                const Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Padding(
                    padding: EdgeInsets.only(right: 210),
                    child: Text("Services",
                        style: TextStyle(
                            fontFamily: 'RobotoSlab-VariableFont_wght.ttf',
                            fontSize: 20,
                            fontWeight: FontWeight.bold)),
                  ),
                ),
                const SizedBox(height: 20),
                Row(
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => HospitalDetails()));
                      },
                      child: Container(
                        child: Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(top: 20),
                              child: Image.asset(
                                'images/hospitalimage-removebg-preview.png',
                                width: 120,
                                height: 120,
                              ),
                            ),
                            const Text("Hospitals",
                                style: TextStyle(
                                    fontFamily:
                                    'RobotoSlab-VariableFont_wght.ttf',
                                    fontSize: 15,
                                    fontWeight: FontWeight.bold)),
                          ],
                        ),
                        width: 160,
                        height: 200,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(12),

                            color: Colors.white),
                      ),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => Doctors_Details()));
                      },
                      child: Container(
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 20),
                                child: Image.asset(
                                  'images/doctors-removebg-preview.png',
                                  width: 120,
                                  height: 120,
                                ),
                              ),
                              const Text("Doctors",
                                  style: TextStyle(
                                      fontFamily:
                                      'RobotoSlab-VariableFont_wght.ttf',
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold)),
                            ],
                          ),
                          width: 160,
                          height: 200,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12),
                              color: Colors.white)),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => Institutions_Details()));
                      },

                      child: Container(
                        child: Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(top: 20),
                              child: Image.asset(
                                'images/institutions-removebg-preview.png',
                                width: 120,
                                height: 120,
                              ),
                            ),
                            const Text("Institutions",
                                style: TextStyle(
                                    fontFamily:
                                    'RobotoSlab-VariableFont_wght.ttf',
                                    fontSize: 15,
                                    fontWeight: FontWeight.bold)),
                          ],
                        ),
                        width: 160,
                        height: 200,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(12),
                            color: Colors.white),
                      ),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => Akshaya_Details ()));
                      },


                      child: Container(
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 20),
                                child: Image.asset(
                                  'images/akshaya-removebg-preview.png',
                                  width: 120,
                                  height: 120,
                                ),
                              ),
                              const Text("Akshaya",
                                  style: TextStyle(
                                      fontFamily:
                                      'RobotoSlab-VariableFont_wght.ttf',
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold)),
                            ],
                          ),
                          width: 160,
                          height: 200,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12),
                              color: Colors.white)),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>  Kseb_Details()));
                      },

                      child: Container(
                        child: Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(top: 20),
                              child: Image.asset(
                                'images/KSEB-logo-removebg-preview.png',
                                width: 120,
                                height: 120,
                              ),
                            ),
                            const Text("kSEB",
                                style: TextStyle(
                                    fontFamily:
                                    'RobotoSlab-VariableFont_wght.ttf',
                                    fontSize: 15,
                                    fontWeight: FontWeight.bold)),
                          ],
                        ),
                        width: 160,
                        height: 200,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(12),
                            color: Colors.white),
                      ),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>  Sys_Volunteers_Details()));
                      },


                      child: Container(
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top: 20),
                                child: Image.asset(
                                  'images/sysvolunteers-removebg-preview.png',
                                  width: 120,
                                  height: 120,
                                ),
                              ),
                              const Text("Volunteers",
                                  style: TextStyle(
                                      fontFamily:
                                      'RobotoSlab-VariableFont_wght.ttf',
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold)),
                            ],
                          ),
                          width: 160,
                          height: 200,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12),
                              color: Colors.white)),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
