import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';

class Akshaya_Details extends StatefulWidget {

  

  @override
  _Akshaya_DetailsState createState() => _Akshaya_DetailsState();
}

class _Akshaya_DetailsState extends State<Akshaya_Details> {
  TextEditingController akshayaname = TextEditingController();
  TextEditingController akshayaphone = TextEditingController();
  TextEditingController akshayaplace= TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xffef5350),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [

              const SizedBox(
                height: 10,
              ),
              const Text(
                "Akshaya Details",
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  width: 370,height: 300,
                  decoration: BoxDecoration(border:Border.all(color:Colors.grey),borderRadius: BorderRadius.circular(12)),
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(height: 10,),
                        const Text("  Latest News",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20,color: Colors.orangeAccent,fontFamily: 'RobotoSlab-VariableFont_wght.ttf'),),
                        const Padding(
                          padding: EdgeInsets.only(left: 10,top: 10),
                          child: Text(" Press Release- Admission for vaccancy seat of MCA School of Studies.\n ",style:TextStyle(color: Colors.blueAccent,fontFamily: 'RobotoSlab-VariableFont_wght.ttf'),),
                        ),
                        const Padding(
                          padding: EdgeInsets.only(left: 10),
                          child: Text("Upoading your Sports Achievement cirtificate.\n",style:TextStyle(color: Colors.blueAccent,fontFamily: 'RobotoSlab-VariableFont_wght.ttf'),),
                        ),
                        const Padding(
                          padding: EdgeInsets.only(left: 10),
                          child: Text("Third Sem Result of the year 2020-2021. \n",style:TextStyle(color: Colors.blueAccent,fontFamily: 'RobotoSlab-VariableFont_wght.ttf'),),
                        ),
                        const Padding(
                          padding: EdgeInsets.only(left: 10),
                          child: Text("fifth Sem Result of the year 2020-2021. \n",style:TextStyle(color: Colors.blueAccent,fontFamily: 'RobotoSlab-VariableFont_wght.ttf'),),
                        ),
                        const Padding(
                          padding: EdgeInsets.only(left: 10),
                          child: Text("First Sem Result of the year 2020-2021. \n",style:TextStyle(color: Colors.blueAccent,fontFamily: 'RobotoSlab-VariableFont_wght.ttf'),),
                        ),
                        const Padding(
                          padding: EdgeInsets.only(left: 10),
                          child: Text("Registration of UG admission 2021. \n",style:TextStyle(color: Colors.blueAccent,fontFamily: 'RobotoSlab-VariableFont_wght.ttf'),),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 10),
                          child: Row(
                            children: const [
                              Text("Read more "),
                              Icon(Icons.arrow_drop_down)
                            ],
                          ),
                        ),

                      ],

                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Container(
                width: 365,
                height: 600,
                color: Colors.white,
                child: StreamBuilder<QuerySnapshot>(
                  stream: FirebaseFirestore.instance.collection('akshayas').where('status',isEqualTo: 1).snapshots(),
                  builder: (context, snapshot) {
              if(!snapshot.hasData)
              return Center(child: CircularProgressIndicator());
              else  if(snapshot.hasData && snapshot.data!.docs.length==0)
              return Center(child: Text("no data found"));
                    else {
                return ListView.builder(
                        itemCount: snapshot.data!.docs.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Card(
                            elevation: 10.0,
                            margin: const EdgeInsets.only(
                                left: 10, top: 0, right: 10, bottom: 10),
                            shadowColor: Colors.white,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15),
                            ),
                            child: ListTile(
                              onTap: () {},
                              title: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Row(
                                      children: [
                                        Text(
                                          snapshot.data!.docs[index]['name'],
                                          style: const TextStyle(
                                              fontFamily:
                                              'RobotoSlab-VariableFont_wght.ttf',
                                              fontWeight: FontWeight.bold,
                                              fontSize: 18),
                                        ),
                                        SizedBox(width: 10,),
                                        Text(
                                          snapshot.data!.docs[index]['phone'],
                                          style: const TextStyle(
                                              fontFamily:
                                              'RobotoSlab-VariableFont_wght.ttf',
                                              color: Colors.blue,

                                              fontSize: 18),
                                        ),

                                      ],
                                    ),
                                  ),

                                  Padding(
                                    padding: const EdgeInsets.only(left: 10),
                                    child: Text(
                                      snapshot.data!.docs[index]['place'],
                                    ),
                                  ),

                                ],
                              ),
                            ),
                          );
                        });
              }
                  }
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }
}
