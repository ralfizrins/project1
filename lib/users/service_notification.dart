import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class service_notification extends StatefulWidget {
  var uid;
  service_notification({Key? key,this.uid}) : super(key: key);

  @override
  _service_notificationState createState() => _service_notificationState();
}

class _service_notificationState extends State<service_notification> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Service Messages",
        ),
        backgroundColor: const Color(0xffef5350),
      ),
      body: SafeArea(
        child: StreamBuilder<QuerySnapshot>(
          stream: FirebaseFirestore.instance
              .collection('notification').where('user_id',isEqualTo: widget.uid)
              .snapshots(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return const Center(child: CircularProgressIndicator());
            } else if (snapshot.hasData && snapshot.data!.docs.isEmpty) {
              return const Center(
                  child: Text(
                    "No allocation found",
                    style: TextStyle(fontSize: 25),
                  ));
            } else {
              return ListView.builder(
                  itemCount: snapshot.data!.docs.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Column(
                      children:  [
                        //SizedBox(height: 20,),
                        Card(
                          elevation: 10.0,
                          margin: const EdgeInsets.only(
                              left: 10, top: 0, right: 10, bottom: 10),
                          shadowColor: Colors.white,
                          shape: const RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(30),
                                topRight: Radius.circular(30)),
                          ),
                          child: SingleChildScrollView(
                            child: ListTile(
                              onTap: () {},
                              title: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children:  [
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  Row(
                                    children: const [
                                      Text(
                                        "Service Allocated Successfully",
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontFamily:
                                            'RobotoSlab-VariableFont_wght.ttf'),
                                      ),
                                      Icon(
                                        Icons.check,
                                        color: Colors.green,
                                        size: 30,
                                      )
                                    ],
                                  ),
                                  SizedBox(height: 10,),
                                  Row(
                                    children: [
                                      Text(
                                        snapshot.data?.docs[index]['msg'],
                                      )
                                    ],
                                  )


                                ],
                              ),
                            ),
                          ),
                        ),
                      ],

                    );

                  });
            }
          },
        ),
      ),
    );
  }
}
