import 'package:flutter/material.dart';
import 'package:marquee/marquee.dart';
import 'package:santhwanam_plus/users/bloodbank.dart';

import 'package:url_launcher/url_launcher.dart';

import 'package:santhwanam_plus/users/contribute1.dart';


class UserHome extends StatefulWidget {
  var uid;
  var uemail;
  var uphone;
  var uname;
  var imgurl;
  UserHome({Key? key,this.uid,this.uname,this.uphone,this.uemail,this.imgurl}) : super(key: key);

  @override
  _UserHomeState createState() => _UserHomeState();
}

class _UserHomeState extends State<UserHome> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: (){

        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>Contributions(uid:widget.uid,uname:widget.uname,uphone: widget.uphone,)));

      },
          backgroundColor: Colors.white ,
          child: Icon(Icons.add, color: Colors.redAccent)

      ),
      body: SafeArea(
          child:SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 10,),
                Padding(
                  padding: const EdgeInsets.only(left: 15,right: 15),
                  child: ElevatedButton(

                      style: ElevatedButton.styleFrom(primary: Colors.white60,shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),),
                      onPressed: (){},
                      child: Row(
                        children: const [
                          Icon(Icons.search,color: Colors.grey,size: 25,),
                          Text(' Search',style: TextStyle(fontFamily: 'RobotoSlab-VariableFont_wght.ttf',color: Colors.grey,fontSize: 15),)
                        ],
                      )),
                ),
                SizedBox(height: 10,),
                Image.asset('images/banner.jpg',),
                SizedBox(height: 10,),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(width: MediaQuery.of(context).size.width,height: 30.0,
                      child:Marquee(
                        text: 'അശരണരുടെ കൂടെ നിൽക്കുന്ന, അവർക്ക് ആശ്വാസവും സ്നേഹവും പകരുന്ന, ആവശ്യങ്ങൾ നിവർത്തിക്കുന്ന സന്നദ്ധസേവകരുടെ സംഘം.',
                        //style: TextStyle(fontWeight: FontWeight.bold),
                        scrollAxis: Axis.horizontal,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        blankSpace: 20.0,
                        velocity: 100.0,
                        pauseAfterRound: Duration(seconds: 1),
                        startPadding: 10.0,
                        accelerationDuration: Duration(seconds: 1),
                        accelerationCurve: Curves.linear,
                        decelerationDuration: Duration(milliseconds: 500),
                        decelerationCurve: Curves.easeOut,
                      )
                  ),
                ),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ElevatedButton(
                        onPressed: (){
                          Navigator.push(context,MaterialPageRoute(builder: (context)=>BloodBank(uid: widget.uid,
                            name: widget.uname,
                            phone:widget.uphone,)));
                          }, child: Text("Donate Blood",style: TextStyle(fontFamily: 'RobotoSlab-VariableFont_wght.ttf',fontSize: 15),),
                        style: ElevatedButton.styleFrom(
                          primary: Color(0xffef5350),
                          shape:  RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(15),
                          ),
                        ),
                      ),
                      SizedBox(width: 20,),
                      ElevatedButton(onPressed: (){
                        Navigator.push(context,MaterialPageRoute(builder: (context)=>BloodBank(uid: widget.uid,
                          name: widget.uname,
                          phone:widget.uphone,)));
                      }, child: Text("Request Blood",style: TextStyle(fontFamily: 'RobotoSlab-VariableFont_wght.ttf',fontSize: 15),),
                          style: ElevatedButton.styleFrom(
                          primary: Color(0xffef5350),
                shape:  RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(15),
                ),
                )

                      ),

                    ],
                  ),
                  width: MediaQuery.of(context).size.width,
                  //decoration: BoxDecoration(border: Border.all(color: Colors.grey),borderRadius: BorderRadius.circular(15)),
                  height: 60,
                ),

                Container(
                  child: const Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: Text('WHO WE ARE ?',style: TextStyle(
                      color: Colors.grey, fontWeight: FontWeight.bold,fontSize: 18,
                      fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                    ),),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Container(child: const Text('With so many different ways today to find info online, it can sometimes be hard to know where to go to first. I want to look at the major & most effective solution.',
                    style: TextStyle(
                      color: Colors.black, fontSize: 15,
                      fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                    ),
                  )
                  ),
                ),
                SingleChildScrollView(
                  child: Container(
                    height: 200,width: 400,
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: <Widget>[
                        Container(
                          height: 320,
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Column(
                                    children: [
                                      SizedBox(height: 10,),
                                      Text('Santhwanam',style: TextStyle(fontWeight: FontWeight.bold,fontFamily: 'RobotoSlab-VariableFont_wght.ttf'),),
                                      SizedBox(width:20),
                                      Padding(
                                        padding: const EdgeInsets.only(left: 10),
                                        child: Container(width: 250,height: 150,
                                          child: Image.asset('images/syslogo.png',),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Column(
                                    children: [
                                      SizedBox(height: 10,),
                                      Text('Ambulance',style: TextStyle(fontWeight: FontWeight.bold,fontFamily: 'RobotoSlab-VariableFont_wght.ttf'),),
                                      SizedBox(width:20,height: 10,),
                                      Container(width: 250,height: 150,
                                        child: Image.asset('images/ambulance.jpg',),
                                      )

                                    ],
                                  ),
                                  Column(
                                    children: [
                                      SizedBox(height: 10,),
                                      Text('Blood Donation',style: TextStyle(fontWeight: FontWeight.bold,fontFamily: 'RobotoSlab-VariableFont_wght.ttf'),),
                                      SizedBox(width:20),
                                      Container(width: 250,height: 150,
                                        child: Image.asset('images/sys2.jpg',),
                                      )
                                    ],
                                  ),
                                  Column(
                                    children: [
                                      SizedBox(height: 10,),
                                      Text('Blood Bank',style: TextStyle(fontWeight: FontWeight.bold,fontFamily: 'RobotoSlab-VariableFont_wght.ttf'),),
                                      SizedBox(width:20),
                                      Padding(
                                        padding: const EdgeInsets.only(left: 15),
                                        child: Container(width: 250,height: 150,
                                          child: Image.asset('images/GIVE_BLOOD-removebg-preview.png',),
                                        ),
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            ],
                          ),

                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Stack(
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: 350,
                        decoration: BoxDecoration(border:Border.all(),borderRadius: BorderRadius.circular(12),),
                        child: Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(12),
                              ),
                                  child: Image.asset('images/vaccination.png',)),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(
                                height: 130,
                                child: Column(
                                  children: [
                                    const Text(
                                      "Did You Get Vaccinated ?",
                                      style: TextStyle(
                                          fontFamily: 'RobotoSlab-VariableFont_wght.ttf',
                                          fontSize: 20),
                                    ),
                                    Column(
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.only(left: 20,),
                                          child: Row(
                                            children: [
                                              Image.asset(
                                                'images/flyimage4.png',
                                                width: 30,
                                                height: 30,
                                              ),
                                              TextButton(
                                                  onPressed: (){
                                                    _LaunchUrl('https://selfregistration.cowin.gov.in/');
                                                  },
                                                  child: const Text(
                                                    "Book Your Vaccination Slot",
                                                    style: TextStyle(
                                                        fontFamily: 'RobotoSlab-VariableFont_wght.ttf',fontSize: 10,fontWeight: FontWeight.bold,
                                                    ),
                                                  ))
                                            ],
                                          ),
                                        ),
                                        SizedBox(width: 10,),
                                        Padding(
                                          padding: const EdgeInsets.only(left: 20),
                                          child: Row(
                                            children: [
                                              Image.asset('images/flyimage5.png',width: 30,height: 30,),
                                              TextButton(onPressed: (){
                                                _LaunchUrl('https://selfregistration.cowin.gov.in/');
                                              },
                                                  child:   const Text(
                                                    "Download Your vaccination Cirtificate ",
                                                    style: TextStyle(
                                                        fontFamily: 'RobotoSlab-VariableFont_wght.ttf',fontSize: 10,fontWeight: FontWeight.bold
                                                    ),
                                                  ))
                                            ],
                                          ),
                                        )


                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),

                          ],
                        ),
                      ),
                    ],
                  ),
                )

              ],
            ),
          )
      ),
    );
  }
  Future<void> _LaunchUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}