import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';

class Kseb_Details extends StatefulWidget {


  var tollfreenumber=['1912'];
  var allindianum=['0471-2555544'];


  @override
  _Kseb_DetailsState createState() => _Kseb_DetailsState();
}

class _Kseb_DetailsState extends State<Kseb_Details> {
  var _ksebkey = GlobalKey<FormState>();
  TextEditingController description = TextEditingController();
  TextEditingController pincode = TextEditingController();
  TextEditingController ref_number = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: SafeArea(
          child: Form(
            key: _ksebkey,
            child: Column(
              children: [
                ClipPath(
                  clipper: WaveClipperOne(),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height / 6,
                    color: Color(0xffef5350),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                const Text(
                  "Kseb Details",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                  ),
                ),
              SizedBox(height: 10,),
              Container(


                child: Padding(
                  padding: const EdgeInsets.only(left: 20),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Text("Toll Free number",style: TextStyle(fontWeight: FontWeight.bold),),
                          TextButton(
                              onPressed: (){
                                _makePhoneCall(widget.tollfreenumber[0]);

                              }, child: Text(widget.tollfreenumber[0]))
                        ],
                      ),
                    Row(
                      children: [
                        Text("All india Numbers",style: TextStyle(fontWeight: FontWeight.bold),),
                        TextButton(
                            onPressed: (){
                              _makePhoneCall(widget.allindianum[0]);

                            }, child: Text(widget.allindianum[0]))


                      ],
                    ),



                    ],


                  ),
                ),
              ),


                const SizedBox(
                  height: 30,
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height:400,
                 // width: MediaQuery.of(context).size.width,
                  //height: MediaQuery.of(context).size.height,
                  color: Colors.white,
                  child: StreamBuilder<QuerySnapshot>(
                    stream: FirebaseFirestore.instance.collection('kseb').where('status',isEqualTo: 1).snapshots(),
                    builder: (context, snapshot) {
                      if(!snapshot.hasData)
                        return Center(child: CircularProgressIndicator());
                      else  if(snapshot.hasData && snapshot.data!.docs.length==0)
                        return Center(child: Text("no data found"));
                      else {
                        return ListView.builder(
                          itemCount: snapshot.data!.docs.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Card(
                              elevation: 10.0,
                              margin: const EdgeInsets.only(
                                  left: 10, top: 0, right: 10, bottom: 10),
                              shadowColor: Colors.white,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15),
                              ),
                              child: SingleChildScrollView(
                                child: ListTile(
                                  onTap: () {},
                                  title: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Row(
                                          children: [
                                            Text(
                                        snapshot.data!.docs[index]['name'],
                                              style: const TextStyle(
                                                  fontFamily:
                                                  'RobotoSlab-VariableFont_wght.ttf',
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 15),
                                            ),
                                            SizedBox(width: 20,),
                                            Text(
                                              snapshot.data!.docs[index]['phone'],
                                              style: const TextStyle(
                                                  fontFamily:
                                                  'RobotoSlab-VariableFont_wght.ttf',
                                                  color: Colors.blue,

                                                  fontSize: 18),
                                            ),

                                          ],
                                        ),
                                      ),

                                      Padding(
                                        padding: const EdgeInsets.only(left: 10),
                                        child: Text(
                                          snapshot.data!.docs[index]['place'],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            );
                          });
                      }
                    }
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
  Future<void> _makePhoneCall(String number) async {
    var url = 'tel:${number}';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
