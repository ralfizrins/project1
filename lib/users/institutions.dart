import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';

class Institutions_Details extends StatefulWidget {




  @override
  _Institutions_DetailsState createState() => _Institutions_DetailsState();
}

class _Institutions_DetailsState extends State<Institutions_Details> {
  var _institutionkey = GlobalKey<FormState>();
  TextEditingController institutionname = TextEditingController();
  TextEditingController phone = TextEditingController();
  TextEditingController place = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: SafeArea(
          child: Form(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              key: _institutionkey,
              children: [
                ClipPath(
                  clipper: WaveClipperOne(),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height / 6,
                    color: Color(0xffef5350),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                const Text(
                  "Institution Details",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  color: Colors.white,
                  child: StreamBuilder<QuerySnapshot>(
                    stream: FirebaseFirestore.instance.collection('institute').where('status',isEqualTo: 1).snapshots(),
                    builder: (context, snapshot) {
                      if(!snapshot.hasData)
                        return Center(child: CircularProgressIndicator());
                      else  if(snapshot.hasData && snapshot.data!.docs.length==0)
                        return Center(child: Text("no data found"));

                      else {
                        return ListView.builder(
                          itemCount: snapshot.data!.docs.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Card(
                              elevation: 10.0,
                              margin: const EdgeInsets.only(
                                  left: 10, top: 0, right: 10, bottom: 10),
                              shadowColor: Colors.white,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15),
                              ),
                              child: SingleChildScrollView(
                                child: ListTile(
                                  onTap: () {},
                                  title: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(5.0),
                                        child: Row(
                                          children: [
                                            Text(
                                              snapshot.data!.docs[index]['name'],
                                              style: const TextStyle(
                                                  fontFamily:
                                                  'RobotoSlab-VariableFont_wght.ttf',
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 15),
                                            ),
                                            SizedBox(width: 8,),
                                            Text(
                                              snapshot.data!.docs[index]['phone'],
                                              style: const TextStyle(
                                                  fontFamily:
                                                  'RobotoSlab-VariableFont_wght.ttf',
                                                  color: Colors.blue,

                                                  fontSize: 18),
                                            ),

                                          ],
                                        ),
                                      ),

                                      Padding(
                                        padding: const EdgeInsets.only(left: 10),
                                        child: Text(
                                          snapshot.data!.docs[index]['place'],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            );
                          });
                      }
                    }
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
