import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';

class Sys_Volunteers_Details extends StatefulWidget {
  @override
  _Sys_Volunteers_DetailsState createState() => _Sys_Volunteers_DetailsState();
}

class _Sys_Volunteers_DetailsState extends State<Sys_Volunteers_Details> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xffef5350),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(height: 10),
              const Text(
                "Volunteer Details",
                style: TextStyle(
                    fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                    fontSize: 20),
              ),
              SizedBox(height: 20),
              Container(
                height: 600,
                width: 500,
                color: Colors.white,
                child: StreamBuilder<QuerySnapshot>(
                    stream: FirebaseFirestore.instance
                        .collection('volunteer')
                        .where('status', isEqualTo: 1)
                        .snapshots(),
                    builder: (context, snapshot) {
                      if (!snapshot.hasData)
                        return Center(child: CircularProgressIndicator());
                      else if (snapshot.hasData &&
                          snapshot.data!.docs.length == 0)
                        return Center(child: Text("no data found"));
                      else {
                        return ListView.builder(
                            itemCount: snapshot.data!.docs.length,
                            itemBuilder: (BuildContext context, int index) {
                              return Card(
                                elevation: 10.0,
                                margin: const EdgeInsets.only(
                                    left: 10, top: 0, right: 10, bottom: 10),
                                shadowColor: Colors.white,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                child: SingleChildScrollView(
                                  child: ListTile(
                                    onTap: () {
                                      Navigator.pushNamed(context, '/9');
                                    },
                                    title: Row(
                                      children: [
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(left: 23),
                                          child: Text(
                                            snapshot.data!.docs[index]['name'],
                                            style: const TextStyle(
                                                fontFamily:
                                                    'RobotoSlab-VariableFont_wght.ttf',
                                                fontWeight: FontWeight.bold,
                                                fontSize: 18),
                                          ),
                                        ),
                                        const SizedBox(
                                          width: 5,
                                        ),
                                        TextButton(
                                          onPressed: () {
                                            _makePhoneCall(snapshot
                                                .data!.docs[index]['phone']);
                                          },
                                          child: Text(
                                            snapshot.data!.docs[index]['phone'],
                                            style:
                                                TextStyle(color: Colors.blue),
                                          ),
                                        ),
                                        const Icon(
                                          Icons.smartphone,
                                          size: 20,
                                          color: Colors.blue,
                                        ),
                                      ],
                                    ),
                                    hoverColor: Colors.blue,
                                    subtitle: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        SizedBox(height: 10),
                                        Row(
                                          children: [
                                            const Padding(
                                              padding: EdgeInsets.only(
                                                  left: 20),
                                              child: Text(
                                                'pincode :',
                                                style: TextStyle(
                                                  fontFamily:
                                                      'RobotoSlab-VariableFont_wght.ttf',
                                                  fontSize: 16,
                                                ),
                                              ),
                                            ),
                                            Text(snapshot.data!.docs[index]
                                                ['pincode']),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            });
                      }
                    }),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _makePhoneCall(String number) async {
    var url = 'tel:${number}';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
