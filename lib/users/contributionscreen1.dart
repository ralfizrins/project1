import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


import '../validation.dart';

class GiveMoney extends StatefulWidget {
  const GiveMoney({Key? key}) : super(key: key);

  @override
  _GiveMoneyState createState() => _GiveMoneyState();
}

class _GiveMoneyState extends State<GiveMoney> {
  var _contributemoney = GlobalKey<FormState>();

  TextEditingController amount = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: SingleChildScrollView(
        child: SafeArea(
          child:Form(
            key: _contributemoney,
            child: Column(
              children: [
                SizedBox(height: 10,),
                const Text("Contribute Money",
                  style: TextStyle(color: Colors.black,fontSize: 20,
                    fontWeight: FontWeight.bold,
                    fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                  ),),

                SizedBox(height: 10,),
                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Container(width: 400,height: 610,
                    child: Card(shadowColor: Colors.grey,elevation: 2.0,
                      shape:RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      ),color: Colors.white,
                      child: Container(
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Column(
                            children: [

                              SizedBox(height:20),

                              TextFormField(
                                controller: amount,
                                cursorColor: Colors.black,
                                keyboardType: TextInputType.number,
                                decoration: const InputDecoration(
                                  hintText: 'Enter Amount',
                                  hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                                ),
                                validator: (value) {
                                  return Validate.careagevalidator(value!.trim());
                                },
                              ),
                              SizedBox(height: 30,),
                              Text("Select your payment method",style: TextStyle(fontFamily: "RobotoSlab-VariableFont_wght.ttf",fontWeight: FontWeight.bold),),
                              Row(
                                children: [
                                  Container(
                                    width: 130,height: 130,
                                    child: Image.asset('images/gpay.png'),
                                  ),
                                  Container(
                                    width: 130,height: 130,
                                    child: Image.asset('images/amazonpay.png'),
                                  ),
                                ],
                              ),
                              Row(
                                children: [Container(
                                  width: 130,height: 130,
                                  child: Image.asset('images/phonepay.png'),
                                ),
                                  Container(
                                    width: 130,height: 130,
                                    child: Image.asset('images/paytm.png'),
                                  ),

                                ],
                              ),
                              Container(
                                width: 80,height: 80,
                                child: Image.asset('images/WhatsApp.png'),

                              ),
                              SizedBox(height: 30,),
                              //count
                              ElevatedButton(
                                  onPressed: () {
                                    if (_contributemoney.currentState!.validate());
                                    //Go to user home

                                  },
                                  style: ElevatedButton.styleFrom(
                                    primary: Color(0xffef5350),
                                    shape:  RoundedRectangleBorder(
                                      borderRadius:  BorderRadius.circular(15),
                                    ),
                                  ),
                                  child: const Text(
                                    "  Submit ",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                                    ),
                                  )),
                              //SizedBox(height:15),


                            ],
                          ),
                        ),


                      ),
                    ),
                  ),
                ),

              ],
            ),
          ),

        ),
      ),
    );
  }


}
