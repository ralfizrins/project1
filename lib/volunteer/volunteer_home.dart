import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:santhwanam_plus/admin/add_careperson.dart';
import 'package:santhwanam_plus/admin/blooddonate_tbl.dart';
import 'package:santhwanam_plus/admin/bloodrequest_tbl.dart';
import 'package:santhwanam_plus/volunteer/add_stock.dart';
import 'package:santhwanam_plus/admin/admin_careview.dart';
import 'package:santhwanam_plus/admin/admin_userview.dart';
import 'package:santhwanam_plus/users/services.dart';
import 'package:santhwanam_plus/volunteer/conttributionsview.dart';
import 'package:santhwanam_plus/volunteer/service_allocate.dart';
//import 'package:santhwanam_plus/store_view.dart';
import 'package:santhwanam_plus/volunteer/update_stock.dart';
import 'package:santhwanam_plus/volunteer/volunteer_add_service.dart';

import '../users/loginpage.dart';
import 'msg_fromadmin.dart';
import 'storview.dart';

class Volunteer_home extends StatefulWidget {
  var uid;
  var vemail;
  var vphone;
  var vname;
  var vaddress;
  var vjob;
  var vbloodgroup;
  var vproof;
  var vpassword;
  var vblood;
  var vpincode;
  Volunteer_home(
      {Key? key,
      this.uid,
      this.vemail,
      this.vphone,
      this.vname,
      this.vaddress,
      this.vjob,
      this.vbloodgroup,
      this.vproof,
      this.vpassword,
      this.vblood,
      this.vpincode})
      : super(
          key: key,
        );

  @override
  _Volunteer_homeState createState() => _Volunteer_homeState();
}

class _Volunteer_homeState extends State<Volunteer_home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Volunteer_Home"),
        backgroundColor: const Color(0xffef5350),
        actions: [
          IconButton(
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Msg_fromadmin()));
              },
              icon: const Icon(Icons.notifications)),
          IconButton(
            icon: const Icon(Icons.logout),
            color: Colors.white,
            tooltip: 'menu icon ',
            onPressed: () {
              FirebaseAuth.instance.signOut().then((value) =>
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) =>
                          LoginPage(
                          ))));
            },
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            children: [
              const SizedBox(
                height: 30,
              ),
              const Text(
                "Welcome Volunteer ",
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 25,
                  fontWeight: FontWeight.bold,
                  fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                ),
              ),
              const Padding(
                padding: EdgeInsets.all(10.0),
                child: Text(
                  "Volunteerism is the voice of the people put into action.  These actions shape and mold the present into a future of which we can all be proud. ",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                    fontWeight: FontWeight.w100,
                    fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: Container(
                  height: 210,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    gradient: const LinearGradient(
                        colors: [Colors.blue, Colors.deepOrange]),
                    borderRadius: BorderRadius.circular(30),

                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Column(
                      children: [
                        const Text(
                          "Users Dashboard",
                          style: TextStyle(
                            fontSize: 20,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontFamily: "RobotoSlab-Bold.ttf",
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        const Text(
                          "Users are the most valuable assets they need our services to brighten their life with us. Whoever is happy will make others happy too.",
                          style: TextStyle(
                            fontSize: 12,
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 5.0),
                          child: Row(
                            children: [
                              ElevatedButton(
                                  onPressed: () {
                                    //if (loginkey.currentState!.validate());
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                Admin_userview()));
                                  },
                                  style: ElevatedButton.styleFrom(
                                    primary: Colors.white,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(15),
                                    ),
                                  ),
                                  child: const Text(
                                    "  View  ",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontFamily:
                                          "RobotoSlab-VariableFont_wght.ttf",
                                    ),
                                  )),
                              const SizedBox(width: 100.0),
                              ElevatedButton(
                                  onPressed: () {
                                    //if (loginkey.currentState!.validate());
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                service_allocate(
                                                  uid: widget.uid,
                                                )));
                                  },
                                  style: ElevatedButton.styleFrom(
                                    primary: Colors.white,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(15),
                                    ),
                                  ),
                                  child: const Text(
                                    "  Allocate  ",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontFamily:
                                          "RobotoSlab-VariableFont_wght.ttf",
                                    ),
                                  )),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: Container(
                  height: 200,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    gradient: const LinearGradient(
                        colors: [Colors.blue, Colors.deepOrange]),
                    borderRadius: BorderRadius.circular(30),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Column(
                      children: [
                        const Text(
                          "Care Home",
                          style: TextStyle(
                            fontSize: 20,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontFamily: "RobotoSlab-Bold.ttf",
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        const Text(
                          "The persons those need special attension. Whoever is happy will make others happy too.",
                          style: TextStyle(
                            fontSize: 12,
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 10.0),
                          child: Row(
                            children: [
                              ElevatedButton(
                                  onPressed: () {
                                    //if (loginkey.currentState!.validate());
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                Admin_careview()));
                                  },
                                  style: ElevatedButton.styleFrom(
                                    primary: Colors.white,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(15),
                                    ),
                                  ),
                                  child: const Text(
                                    "  View List ",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontFamily:
                                          "RobotoSlab-VariableFont_wght.ttf",
                                    ),
                                  )),
                              const SizedBox(width: 10.0),

                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),

              ),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: Container(
                  height: 200,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    gradient: const LinearGradient(
                        colors: [Colors.blue, Colors.deepOrange]),
                    borderRadius: BorderRadius.circular(30),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Column(
                      children: [
                        const Text(
                          "Contributions",
                          style: TextStyle(
                            fontSize: 20,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontFamily: "RobotoSlab-Bold.ttf",
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        const Text(
                          " The users contributed items details that can be also shown here",
                          style: TextStyle(
                            fontSize: 12,
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 10.0),
                          child: Row(
                            children: [
                              ElevatedButton(
                                  onPressed: () {
                                    // if (loginkey.currentState!.validate());
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                Contributionsview()));
                                  },
                                  style: ElevatedButton.styleFrom(
                                    primary: Colors.white,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(15),
                                    ),
                                  ),
                                  child: const Text(
                                    "  View  ",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontFamily:
                                      "RobotoSlab-VariableFont_wght.ttf",
                                    ),
                                  )),
                              const SizedBox(width: 10.0),

                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),

              Padding(
                padding: const EdgeInsets.all(15.0),
                child: Container(
                  height: 200,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    gradient: const LinearGradient(
                        colors: [Colors.blue, Colors.deepOrange]),
                    borderRadius: BorderRadius.circular(30),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Column(
                      children: [
                        const Text(
                          "Store",
                          style: TextStyle(
                            fontSize: 20,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontFamily: "RobotoSlab-Bold.ttf",
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        const Text(
                          "Available information of the equipments that we provided",
                          style: TextStyle(
                            fontSize: 12,
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 20.0),
                          child: Row(
                            children: [
                              ElevatedButton(
                                  onPressed: () {
                                    //if (loginkey.currentState!.validate());
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                Store_view()));
                                  },
                                  style: ElevatedButton.styleFrom(
                                    primary: Colors.white,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(15),
                                    ),
                                  ),
                                  child: const Text(
                                    "  View  ",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontFamily:
                                          "RobotoSlab-VariableFont_wght.ttf",
                                    ),
                                  )),
                              const SizedBox(width: 30.0),
                              ElevatedButton(
                                  onPressed: () {
                                    //if (loginkey.currentState!.validate());
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => Add_stock()));
                                  },
                                  style: ElevatedButton.styleFrom(
                                    primary: Colors.white,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(15),
                                    ),
                                  ),
                                  child: const Text(
                                    " Add",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontFamily:
                                          "RobotoSlab-VariableFont_wght.ttf",
                                    ),
                                  )),
                              const SizedBox(width: 30.0),
                              ElevatedButton(
                                  onPressed: () {
                                    //if (loginkey.currentState!.validate());
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                Update_stock()));
                                  },
                                  style: ElevatedButton.styleFrom(
                                    primary: Colors.white,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(15),
                                    ),
                                  ),
                                  child: const Text(
                                    "Update",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontFamily:
                                          "RobotoSlab-VariableFont_wght.ttf",
                                    ),
                                  )),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: Container(
                  height: 200,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    gradient: const LinearGradient(
                        colors: [Colors.blue, Colors.deepOrange]),
                    borderRadius: BorderRadius.circular(30),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Column(
                      children: [
                        const Text(
                          "Service Providers",
                          style: TextStyle(
                            fontSize: 20,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontFamily: "RobotoSlab-Bold.ttf",
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        const Text(
                          "Different kinds of service providers registered on our wings.",
                          style: TextStyle(
                            fontSize: 12,
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 20.0),
                          child: Row(
                            children: [
                              ElevatedButton(
                                  onPressed: () {
                                    //if (loginkey.currentState!.validate());
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => Services()));
                                  },
                                  style: ElevatedButton.styleFrom(
                                    primary: Colors.white,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(15),
                                    ),
                                  ),
                                  child: const Text(
                                    "View",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontFamily:
                                          "RobotoSlab-VariableFont_wght.ttf",
                                    ),
                                  )),
                              const SizedBox(width: 140.0),
                              ElevatedButton(
                                  onPressed: () {
                                    //if (loginkey.currentState!.validate());
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                Volunteer_add_service()));
                                  },
                                  style: ElevatedButton.styleFrom(
                                    primary: Colors.white,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(15),
                                    ),
                                  ),
                                  child: const Text(
                                    "Add",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontFamily:
                                          "RobotoSlab-VariableFont_wght.ttf",
                                    ),
                                  )),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: GestureDetector(
                  onTap: () {
                    //Navigator.push(context, MaterialPageRoute(builder: (context)=> Admin_userview()));
                  },
                  child: Container(
                    height: 140,
                    width: MediaQuery.of(context).size.width,
                    decoration: const BoxDecoration(
                        gradient: LinearGradient(
                            colors: [Colors.blue, Colors.deepOrange])),
                    child: Column(
                      children: [
                        const Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Text(
                            "Blood Bank",
                            style: TextStyle(
                              fontSize: 16,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                            ),
                          ),
                        ),
                        const Icon(Icons.bloodtype),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            ElevatedButton(
                                onPressed: () {
                                  //if (loginkey.currentState!.validate());
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              Blooddonate_tbl()));
                                },
                                style: ElevatedButton.styleFrom(
                                  primary: Colors.white,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                ),
                                child: const Text(
                                  "View Donors ",
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontFamily:
                                        "RobotoSlab-VariableFont_wght.ttf",
                                  ),
                                )),
                            SizedBox(
                              width: 20.0,
                            ),
                            ElevatedButton(
                                onPressed: () {
                                  //if (loginkey.currentState!.validate());
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              Bloodrequest_tblview()));
                                },
                                style: ElevatedButton.styleFrom(
                                  primary: Colors.white,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                ),
                                child: const Text(
                                  "View Requests",
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontFamily:
                                        "RobotoSlab-VariableFont_wght.ttf",
                                  ),
                                )),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 40),
            ],
          ),
        ),
      ),
    );
  }
}
