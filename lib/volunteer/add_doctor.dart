import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:santhwanam_plus/users/serviceprovider.dart';
import 'package:santhwanam_plus/volunteer/volunteer_add_service.dart';
import 'package:uuid/uuid.dart';
import '../validation.dart';
import 'volunteer_add_service.dart';
import 'volunteer_home.dart';

class Add_doctor extends StatefulWidget {
  var dname;
  var dmobile;
  var ddpt;
  var dhospital;
  var dtime;
  var location;
   Add_doctor({Key? key,
   this.dname,this.ddpt,this.dhospital,this.dtime,this.location,this.dmobile}) : super(key: key);

  @override
  _Add_doctorState createState() => _Add_doctorState();
}

class _Add_doctorState extends State<Add_doctor> {
  var doct_id;
  var uuid=Uuid();
  var _doctorkey = GlobalKey<FormState>();
  TextEditingController drname = TextEditingController();
  TextEditingController drmobile = TextEditingController();
  TextEditingController drdpt = TextEditingController();
  TextEditingController drhospital = TextEditingController();
  TextEditingController drtime = TextEditingController();
  TextEditingController drplace = TextEditingController();
  //TextEditingController stock = TextEditingController();
  //TextEditingController ref_number= TextEditingController();

  @override
  void initState() {
    doct_id=uuid.v1();
    // TODO: implement initState
    super.initState();
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Adding New Doctor"),
        backgroundColor: Color(0xffef5350),
      ),
      body: Form(
        key: _doctorkey,
        child: SingleChildScrollView(
          child: SafeArea(
            child:Column(
              children: [
                SizedBox(height: 30,),
                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Container(width: 400,height: 630,
                    child: Card(shadowColor: Colors.grey,elevation: 2.0,
                      shape:RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      ),color: Colors.white,
                      child: Container(
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Column(
                            children: [

                              SizedBox(height:20),
                              const Text("Fill the form",
                                style: TextStyle(color: Colors.black,fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                                ),),
                              SizedBox(height: 20,),

                              SizedBox(height: 20,),

                              TextFormField(
                                keyboardType: TextInputType.text,
                                maxLines: null,
                                controller: drname,
                                decoration: const InputDecoration(
                                  hintText: 'Name of Dr.',
                                  hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                                ),
                                validator: (value) {
                                  return Validate.namevalidator(value!.trim());
                                },),

                              TextFormField(
                                keyboardType: TextInputType.text,
                                maxLines: null,
                                controller: drdpt,
                                decoration: const InputDecoration(
                                  hintText: 'Department',
                                  hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                                ),
                                validator: (value) {
                                  return Validate.namevalidator(value!.trim());
                                },),

                              TextFormField(
                                keyboardType: TextInputType.text,
                                maxLines: null,
                                controller: drhospital,
                                decoration: const InputDecoration(
                                  hintText: 'Hospital name',
                                  hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                                ),
                                validator: (value) {
                                  return Validate.namevalidator(value!.trim());
                                },),

                              TextFormField(
                                keyboardType: TextInputType.text,
                                maxLines: null,
                                controller: drtime,
                                decoration: const InputDecoration(
                                  hintText: 'Consulting time',
                                  hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                                ),
                                validator: (value) {
                                  return Validate.disease(value!.trim());
                                },),

                              TextFormField(
                                keyboardType: TextInputType.text,
                                maxLines: null,
                                controller: drplace,
                                decoration: const InputDecoration(
                                  hintText: 'Place',
                                  hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                                ),
                                validator: (value) {
                                  return Validate.placevalidator(value!.trim());
                                },),

                              TextFormField(
                                controller: drmobile,
                                cursorColor: Colors.black,
                                keyboardType: TextInputType.number,
                                decoration: const InputDecoration(
                                  hintText: 'Contact',
                                  hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                                ),
                                validator: (value) {
                                  return Validate.phvalidator(value!.trim());
                                },
                              ),



                              SizedBox(height: 40,),
                              ElevatedButton(
                                  onPressed: () {
                                    if (_doctorkey.currentState!.validate())
                                    {
                                      FirebaseFirestore.instance.collection('doctors').doc(doct_id).
                                      set(
                                          {'doctor_id':doct_id,
                                            //'userid':widget.name,
                                            'name':drname.text,
                                            'place':drplace.text,
                                            'phone':drmobile.text,
                                            'department':drdpt.text,
                                            'hospital':drhospital.text,
                                            'time':drtime.text,
                                            'status':1,
                                            'date':DateTime.now()
                                          }
                                      ).then((value) {
                                        showsnackbar(context,"Doctor added successfully");
                                        Navigator.pop(context);
                                      });

                                    }
                                    //_showalert(context);

                                  },
                                  style: ElevatedButton.styleFrom(
                                    primary: Color(0xffef5350),
                                    shape:  RoundedRectangleBorder(
                                      borderRadius: new BorderRadius.circular(15),
                                    ),
                                  ),
                                  child: const Text(
                                    "  Submit",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                                    ),
                                  )),
                              //SizedBox(height:15),


                            ],
                          ),
                        ),


                      ),
                    ),
                  ),
                ),

              ],
            ),

          ),
        ),
      ),
    );
  }
/*
  _showalert(BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Doctor added successfully.."),
            //content: Text("Are you sure"),
            actions: [
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Color(0xffef5350),
                  shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(15),
                  ),
                ),
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context)=> Volunteer_add_service()));
                },
                child: Text("OK",
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                  ),),
              ),

            ],
          );
        });
  }

 */

  showsnackbar(BuildContext context, String value) {
    ScaffoldMessenger.of(context).showSnackBar(
         SnackBar(behavior: SnackBarBehavior.floating,
            backgroundColor: Colors.red,
            margin:EdgeInsetsDirectional.all(10),
            content: Text(
              value,
              style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
            )));
  }
}
