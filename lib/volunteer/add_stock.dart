import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:uuid/uuid.dart';
import '../validation.dart';
import 'volunteer_home.dart';

class Add_stock extends StatefulWidget {
  var uid;

   Add_stock({Key? key,this.uid}) : super(key: key);

  @override
  _Add_stockState createState() => _Add_stockState();
}

class _Add_stockState extends State<Add_stock> {
  var url;
  var filename;
  TextEditingController name = TextEditingController();
  TextEditingController stock = TextEditingController();
  //TextEditingController ref_number= TextEditingController();
  final ImagePicker _picker = ImagePicker();// For pick Image
  XFile ? _image; // For accept Null:-?
  var _stockkey = GlobalKey<FormState>();
  var uuid = Uuid();
  var stockimgid;
  var sid;
  @override
  void initState() {
    stockimgid=DateTime.now().toString();
    sid=DateTime.now().toString();

    // TODO: implement initState
    super.initState();
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(

        backgroundColor: Color(0xffef5350),
      ),
      body: SingleChildScrollView(
        child: SafeArea(
          child:Form(
            key: _stockkey,
            child: Column(
              children: [

                const SizedBox(height: 30,),
                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Container(width: 400,height: 450,
                    child: Card(shadowColor: Colors.grey,elevation: 2.0,
                      shape:RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      ),color: Colors.white,
                      child: Container(
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Column(
                            children: [

                              const SizedBox(height:20),
                              const Text("Add New Item To Store",
                                style: TextStyle(color: Colors.black,fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                                ),),
                              const SizedBox(height: 20,),
                              Column(
                                children: [
                                  GestureDetector(
                                    onTap: (){
                                      _getpics();
                                    },
                                    child: Container(
                                      width:30,//MediaQuery.of(context).size.width,
                                      height:30, //MediaQuery.of(context).size.height,
                                      child: _image!=null? ClipRect(
                                        child: Image.file(
                                          File(_image!.path),//null check operator added:-!
                                        ),
                                      ):Container(child: Icon(Icons.insert_drive_file,size: 50,),),
                                    ),
                                  ),
                                  const SizedBox(height: 20,),

                                  // Text("Upload item image",style: TextStyle(color: Colors.black,fontSize: 10,
                                  //   fontWeight: FontWeight.bold,
                                  //   fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                                  // ),)
                                ],
                              ),
                              const SizedBox(height: 20,),

                              TextFormField(
                                keyboardType: TextInputType.text,
                                maxLines: null,
                                controller: name,
                                decoration: const InputDecoration(
                                  hintText: 'Add name of item to be add',
                                  hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                                ),
                                validator: (value) {
                                return Validate.namevalidator(value!.trim());
                              },
                              ),
                              TextFormField(
                                controller: stock,
                                cursorColor: Colors.black,
                                keyboardType: TextInputType.number,
                                decoration: const InputDecoration(
                                  hintText: 'Count',
                                  hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                                ),
                                validator: (value) {
                                  return Validate.careagevalidator(value!.trim());
                                },
                              ), //pincode


                              const SizedBox(height: 40,),
                              ElevatedButton(
                                  onPressed: () {
                                    if (_stockkey.currentState!.validate()) {
                                      var ref = FirebaseStorage.instance
                                          .ref()
                                          .child('store_img/$sid');
                                      UploadTask uploadTask = ref.putFile(
                                          File(_image!.path));

                                      uploadTask.then((res) async {
                                        url = (await ref.getDownloadURL())
                                            .toString();
                                      }).then((value) =>
                                          FirebaseFirestore.instance
                                              .collection('store')
                                              .doc(sid)
                                              .set({
                                            'sid': sid,
                                            'imgurl': url,
                                            'name': name.text,
                                            'stock': stock.text,

                                          }).then((value) {
                                            showsnackbar(
                                                context, "Stock updated");
                                          }));
                                    } },
                                  style: ElevatedButton.styleFrom(
                                    primary: const Color(0xffef5350),
                                    shape:  RoundedRectangleBorder(
                                      borderRadius:  BorderRadius.circular(15),
                                    ),
                                  ),
                                  child: const Text(
                                    "Submit",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                                    ),
                                  )),
                              //SizedBox(height:15),


                            ],
                          ),
                        ),


                      ),
                    ),
                  ),
                ),

              ],
            ),
          ),

        ),
      ),
    );
  }
  _imgbrowse()async{
    final XFile? photo = await _picker.pickImage(source: ImageSource.camera);
    //final XFile? image = await _picker.pickImage(source: ImageSource.gallery);
    setState(() {
      _image=photo;
    });
  }
  _imggallery()async{
    //final XFile? photo = await _picker.pickImage(source: ImageSource.camera);
    final XFile? image = await _picker.pickImage(source: ImageSource.gallery);
    setState(() {
      _image=image;
    });}
  _getpics(){
    showModalBottomSheet(
        context: context,
        builder: (context){
          return Container(
            child: Wrap(
              children: [
                Expanded(
                  child: ListTile(
                      title: Text("Photo Gallery"),
                      leading: Icon(Icons.image),
                      onTap: (){
                        _imggallery();
                      }

                  ),
                ),
                Expanded(
                  child: ListTile(
                    title: Text("Camera"),
                    leading: Icon(Icons.camera),
                    onTap: (){
                      _imgbrowse();
                    },
                  ),
                ),

              ],

            ),

          );
        }
    );
  }
  _showalert(BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Successfully new item added to our store.."),
            //content: Text("Are you sure"),
            actions: [
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Color(0xffef5350),
                  shape:  RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(15),
                  ),
                ),
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context)=> Volunteer_home()));
                },
                child: const Text("OK",
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                  ),),
              ),

            ],
          );
        });
  }
  showsnackbar(BuildContext context, String value) {
    ScaffoldMessenger.of(context).showSnackBar(
         SnackBar(behavior: SnackBarBehavior.floating,
            backgroundColor: Colors.red,
            margin:EdgeInsetsDirectional.all(10),
            content: Text(
              value,
              style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
            )));
  }

}
