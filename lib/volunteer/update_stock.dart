import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class Update_stock extends StatefulWidget {
  const Update_stock({Key? key}) : super(key: key);

  @override
  _Update_stockState createState() => _Update_stockState();
}

class _Update_stockState extends State<Update_stock> {
  TextEditingController stock=TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Store"),
          backgroundColor: Color(0xffef5350),
        ),

        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.only(top: 15.0),
            child: Column(
              children: [

                Expanded(
                  child: StreamBuilder<QuerySnapshot>(
                    stream:
                    FirebaseFirestore.instance.collection('store').snapshots(),
                    builder: (context, snapshot) {
                      if (!snapshot.hasData) {
                        return const Center(child: CircularProgressIndicator());
                      } else if (snapshot.hasData && snapshot.data!.docs.length == 0) {
                        return const Center(
                            child: Text(
                              "No data found",
                              style: TextStyle(fontSize: 25),
                            ));
                      } else {
                        return ListView.builder(
                            itemCount: snapshot.data!.docs.length,
                            itemBuilder: (BuildContext context, int index) {
                              return Padding(
                                padding: const EdgeInsets.only(bottom: 15.0),
                                child: Center(
                                  child: Container(
                                    width: 260,
                                    height: 300,
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                        color: Color(0xff89d3fb),
                                      ),
                                      borderRadius: BorderRadius.circular(8),
                                    ),
                                    child: Column(
                                      children: [
                                        Column(
                                          children: [
                                            SizedBox(height: 10.0,),
                                            Text(
                                              snapshot.data?.docs[index]['name'],
                                              style: const TextStyle(
                                                  fontFamily:
                                                  'RobotoSlab-VariableFont_wght.ttf',
                                                  fontSize: 20,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            Image.network(
                                              snapshot.data?.docs[index]['imgurl'],
                                              width: 150,
                                              height: 150,
                                            ),
                                            SizedBox(height: 10.0,),
                                            Text(
                                              'Stock: '+ snapshot.data?.docs[index]['stock'],
                                              style: const TextStyle(
                                                  fontFamily:
                                                  'RobotoSlab-VariableFont_wght.ttf',
                                                  fontSize: 15,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            SizedBox(height: 10,),
                                            ElevatedButton(onPressed: (){
                                              _showalert(context,snapshot.data?.docs[index]['sid'],);
                                              //Navigator.push(context, MaterialPageRoute(builder: (context) => ServiceRequest()));
                                            },
                                              child: Text('Update'), style: ElevatedButton.styleFrom(
                                                  fixedSize: Size(100,30),
                                                  primary: Color(0xffef5350),
                                                  shape: RoundedRectangleBorder(
                                                    borderRadius: BorderRadius.circular(10),
                                                  )),

                                            ),
                                            SizedBox(height: 10,),
                                          ],

                                        ),

                                      ],
                                    ),
                                  ),

                                ),
                              );

                            });
                      }
                    },
                  ),

                ),

              ],
            ),
          ),
        ));
  }
  _showalert(BuildContext context,var uid) {

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text(
              "Update",
              style: TextStyle(fontFamily: "RobotoSlab-VariableFont_wght.ttf"),
            ),
            content: TextFormField(
              controller:stock ,
            ),
            actions: [
              TextButton(
                  onPressed: () {
                    FirebaseFirestore.instance
                        .collection('store')
                        .doc(uid)
                        .update({
                      'stock': stock.text,
                    }).then((value) {
                      showsnackbar(context, "Updated Successfully.");
                    });
                   // Navigator.push(context, MaterialPageRoute(builder: (context) =>Admin_userview()));
                    Navigator.of(context).pop();

                  },
                  child: Text("OK")),
            ],
          );
        });
  }
  showsnackbar(BuildContext context, String value) {
    ScaffoldMessenger.of(context).showSnackBar(
         SnackBar(behavior: SnackBarBehavior.floating,
            backgroundColor: Colors.red,
            margin:EdgeInsetsDirectional.all(10),
            content: Text(
              value,
              style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
            )));
  }
}
