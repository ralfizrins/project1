import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:santhwanam_plus/users/serviceprovider.dart';
import 'package:uuid/uuid.dart';
//import 'package:santhwanam_plus/volunteer_add_service.dart';
import '../validation.dart';
import 'volunteer_add_service.dart';
import 'volunteer_home.dart';

class Add_kseb extends StatefulWidget {
  var name;
  var contact;
  var location;
  Add_kseb({Key? key,
  this.name,this.contact,this.location}) : super(key: key);

  @override
  _Add_ksebState createState() => _Add_ksebState();
}

class _Add_ksebState extends State<Add_kseb> {
  var uuid = Uuid();
  var ksebid;
  var _ksebkey = GlobalKey<FormState>();
  TextEditingController section= TextEditingController();
  TextEditingController ksebmobile = TextEditingController();
  TextEditingController ksebplace = TextEditingController();
  //TextEditingController stock = TextEditingController();
  //TextEditingController ref_number= TextEditingController();
  @override
  void initState() {
    ksebid=uuid.v1();
    // TODO: implement initState
    super.initState();
  }




  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Adding New KSEB"),
        backgroundColor: Color(0xffef5350),
      ),
      body: Form(
        key: _ksebkey,
        child: SingleChildScrollView(
          child: SafeArea(
            child:Column(
              children: [
                SizedBox(height: 50,),
                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Container(width: 400,height: 430,
                    child: Card(shadowColor: Colors.grey,elevation: 2.0,
                      shape:RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      ),color: Colors.white,
                      child: Container(
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Column(
                            children: [

                              SizedBox(height:20),
                              const Text("Fill the form",
                                style: TextStyle(color: Colors.black,fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                                ),),
                              SizedBox(height: 30,),

                              TextFormField(
                                keyboardType: TextInputType.text,
                                maxLines: null,
                                controller: section,
                                decoration: const InputDecoration(
                                  hintText: 'Section Number',
                                  hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                                ),
                                validator: (value) {
                                  return Validate.namevalidator(value!.trim());
                                },
                              ),
                              TextFormField(
                                controller: ksebmobile,
                                cursorColor: Colors.black,
                                keyboardType: TextInputType.number,
                                decoration: const InputDecoration(
                                  hintText: 'Contact',
                                  hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                                ),
                                validator: (value) {
                                  return Validate.phvalidator(value!.trim());
                                },
                              ),
                              TextFormField(
                                keyboardType: TextInputType.text,
                                maxLines: null,
                                controller: ksebplace,
                                decoration: const InputDecoration(
                                  hintText: 'Location',
                                  hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                                ),
                                validator: (value) {
                                  return Validate.placevalidator(value!.trim());
                                },),


                              SizedBox(height: 40,),
                              ElevatedButton(
                                  onPressed: () {
                                    if (_ksebkey.currentState!.validate())
                                    {
                                      FirebaseFirestore.instance.collection('kseb').doc(ksebid).
                                      set(
                                          {'kseb_id':ksebid,
                                            //'userid':widget.name,
                                            'name':section.text,
                                            'place':ksebplace.text,
                                            'phone':ksebmobile.text,
                                            'status':1,
                                            'date':DateTime.now()
                                          }
                                      ).then((value) {
                                        showsnackbar(context,"Akshaya added successfully");
                                        Navigator.pop(context);
                                      });

                                    }
                                    //_showalert(context);

                                  },
                                  style: ElevatedButton.styleFrom(
                                    primary: Color(0xffef5350),
                                    shape:  RoundedRectangleBorder(
                                      borderRadius: new BorderRadius.circular(15),
                                    ),
                                  ),
                                  child: const Text(
                                    "  Submit",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                                    ),
                                  )),
                              //SizedBox(height:15),


                            ],
                          ),
                        ),


                      ),
                    ),
                  ),
                ),

              ],
            ),

          ),
        ),
      ),
    );
  }
/*
  _showalert(BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("KSEB added successfully.."),
            //content: Text("Are you sure"),
            actions: [
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Color(0xffef5350),
                  shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(15),
                  ),
                ),
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context)=> Volunteer_add_service()));
                },
                child: Text("OK",
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                  ),),
              ),

            ],
          );
        });
  }

 */

  showsnackbar(BuildContext context, String value) {
    ScaffoldMessenger.of(context).showSnackBar(
         SnackBar(behavior: SnackBarBehavior.floating,
            backgroundColor: Colors.red,
            margin:EdgeInsetsDirectional.all(10),
            content: Text(
              value,
              style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
            )));
  }
}
