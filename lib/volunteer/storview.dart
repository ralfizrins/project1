import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:santhwanam_plus/volunteer/volunteer_home.dart';
import 'package:uuid/uuid.dart';

class Store_view extends StatefulWidget {
  const Store_view({Key? key}) : super(key: key);

  @override
  _Store_viewState createState() => _Store_viewState();
}

class _Store_viewState extends State<Store_view> {
  var _updatestockkey = GlobalKey<FormState>();
  var uuid = Uuid();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Store"),
          backgroundColor: Color(0xffef5350),
        ),
        body: Form(
          key: _updatestockkey,
          child: SafeArea(
            child: Padding(
              padding: const EdgeInsets.only(top: 30.0),
              child: Column(
                children: [
                  Expanded(
                    child: StreamBuilder<QuerySnapshot>(
                      stream: FirebaseFirestore.instance
                          .collection('store')
                          .snapshots(),
                      builder: (context, snapshot) {
                        if (!snapshot.hasData) {
                          return Center(child: CircularProgressIndicator());
                        } else if (snapshot.hasData &&
                            snapshot.data!.docs.length == 0) {
                          return const Center(
                              child: Text(
                            "No data found",
                            style: TextStyle(fontSize: 25),
                          ));
                        } else {
                          return ListView.builder(
                              itemCount: snapshot.data!.docs.length,
                              itemBuilder: (BuildContext context, int index) {
                                return Padding(
                                  padding: const EdgeInsets.only(bottom: 15.0),
                                  child: Center(
                                    child: Container(
                                      width: 250,
                                      height: 250,
                                      decoration: BoxDecoration(
                                        border: Border.all(
                                          color: Color(0xff89d3fb),
                                        ),
                                        borderRadius: BorderRadius.circular(8),
                                      ),
                                      child: Column(
                                        children: [
                                          Column(
                                            children: [
                                              const SizedBox(
                                                height: 10.0,
                                              ),
                                              Text(
                                                snapshot.data?.docs[index]
                                                    ['name'],
                                                style: const TextStyle(
                                                    fontFamily:
                                                        'RobotoSlab-VariableFont_wght.ttf',
                                                    fontSize: 20,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                              Image.network(
                                                snapshot.data?.docs[index]
                                                    ['imgurl'],
                                                width: 150,
                                                height: 150,
                                              ),
                                              const SizedBox(
                                                height: 10.0,
                                              ),
                                              Text(
                                                'Stock: ' +
                                                    snapshot.data?.docs[index]
                                                        ['stock'],
                                                style: const TextStyle(
                                                    fontFamily:
                                                        'RobotoSlab-VariableFont_wght.ttf',
                                                    fontSize: 15,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                );
                              });
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
