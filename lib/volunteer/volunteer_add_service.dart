import 'package:flutter/material.dart';
import 'package:santhwanam_plus/volunteer/add_doctor.dart';
import 'package:santhwanam_plus/volunteer/add_hospital.dart';
import 'package:santhwanam_plus/volunteer/add_institute.dart';
import 'package:santhwanam_plus/volunteer/add_kseb.dart';
import 'package:santhwanam_plus/volunteer/volunteer_home.dart';
import 'add_akshaya.dart';

class Volunteer_add_service extends StatefulWidget {
  const Volunteer_add_service({Key? key}) : super(key: key);

  @override
  _Volunteer_add_serviceState createState() => _Volunteer_add_serviceState();
}

class _Volunteer_add_serviceState extends State<Volunteer_add_service> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Add Services"),
        backgroundColor: Color(0xffef5350),
        actions: [
          IconButton(
            icon: Icon(Icons.home_outlined),
            color: Colors.white,
            tooltip: 'menu icon ',
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>Volunteer_home()));
            },
          ),
        ],
      ),
      body: SafeArea(
        child: Column(

          children: [
            SizedBox(height: 100,),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context)=> Add_hospital()));
                  },
                  child: Container(height: 120,width: 100,
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.black),
                        shape: BoxShape.circle
                    ),
                    child: const Center(
                      child: Text("Hospitals",
                        style: TextStyle(
                          color: Color(0xffef5350),
                          fontWeight: FontWeight.bold,
                          fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                        ),),
                    ),
                  ),
                ),
                SizedBox(width: 50,),
                GestureDetector(
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context)=> Add_akshaya()));
                  },
                  child: Container(height: 120,width: 100,
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.black),
                        shape: BoxShape.circle
                    ),
                    child: const Center(
                      child: Text("Akshaya",
                        style: TextStyle(
                          color: Color(0xffef5350),
                          fontWeight: FontWeight.bold,
                          fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                        ),),
                    ),
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context)=> Add_doctor()));
                  },
                  child: Container(height: 120,width: 100,
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.black),
                        shape: BoxShape.circle
                    ),
                    child: const Center(
                      child: Text("Doctor",
                        style: TextStyle(
                          color: Color(0xffef5350),
                          fontWeight: FontWeight.bold,
                          fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                        ),),
                    ),
                  ),
                ),
                SizedBox(width: 50,),
                GestureDetector(
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context)=> Add_kseb()));
                  },
                  child: Container(height: 120,width: 100,
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.black),
                        shape: BoxShape.circle
                    ),
                    child: const Center(
                      child: Text("KSEB",
                        style: TextStyle(
                          color: Color(0xffef5350),
                          fontWeight: FontWeight.bold,
                          fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                        ),),
                    ),
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context)=> Add_institute()));
                  },
                  child: Container(height: 120,width: 100,
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.black),
                        shape: BoxShape.circle
                    ),
                    child: const Center(
                      child: Text("Institutions",
                        style: TextStyle(
                          color: Color(0xffef5350),
                          fontWeight: FontWeight.bold,
                          fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                        ),),
                    ),
                  ),
                ),
                SizedBox(width: 50,),
                GestureDetector(
                  onTap: (){
                    //Navigator.push(context, MaterialPageRoute(builder: (context)=> Add_service()));
                  },
                  child: Container(height: 120,width: 100,
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.black),
                        shape: BoxShape.circle
                    ),
                    child: const Center(
                      child: Icon(Icons.add_box_rounded,size: 50,color: Color(0xffef5350),),
                    ),
                  ),
                ),

              ],
            ),

          ],
        ),
      ),
    );
  }
}
