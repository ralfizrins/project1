import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

import '../validation.dart';
import 'volunteer_add_service.dart';
import 'volunteer_home.dart';

class Add_akshaya extends StatefulWidget {
  var akname;
  var akmobile;
  var location;
  Add_akshaya({Key? key,
    this.akname,this.akmobile,this.location}) : super(key: key);

  @override
  _Add_akshayaState createState() => _Add_akshayaState();
}

class _Add_akshayaState extends State<Add_akshaya> {
  var uuid = Uuid();
  var akshayaid;
  var _akshayakey = GlobalKey<FormState>();
  TextEditingController akshayaname = TextEditingController();
  TextEditingController akshayamobile = TextEditingController();
  TextEditingController akshayaplace = TextEditingController();
  //TextEditingController stock = TextEditingController();
  //TextEditingController ref_number= TextEditingController();
  @override
  void initState() {
    // TODO: implement initState
    akshayaid=uuid.v1();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Adding New Akshaya"),
        backgroundColor: Color(0xffef5350),
      ),
      body: Form(
        key: _akshayakey,
        child: SingleChildScrollView(
          child: SafeArea(
            child:Column(
              children: [
                SizedBox(height: 50,),
                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Container(width: 400,height: 415,
                    child: Card(shadowColor: Colors.grey,elevation: 2.0,
                      shape:RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      ),color: Colors.white,
                      child: Container(
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Column(
                            children: [

                              SizedBox(height:20),
                              const Text("Fill the form",
                                style: TextStyle(color: Colors.black,fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                                ),),
                              SizedBox(height: 30,),

                              TextFormField(
                                keyboardType: TextInputType.text,
                                maxLines: null,
                                controller: akshayaname,
                                decoration: const InputDecoration(
                                  hintText: 'Name of akshaya',
                                  hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                                ),
                                validator: (value) {
                                  return Validate.namevalidator(value!.trim());
                                },
                                ),
                              TextFormField(
                                controller: akshayamobile,
                                cursorColor: Colors.black,
                                keyboardType: TextInputType.number,
                                decoration: const InputDecoration(
                                  hintText: 'Contact',
                                  hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                                ),
                                validator: (value) {
                                  return Validate.phvalidator(value!.trim());
                                },
                              ),
                              TextFormField(
                                keyboardType: TextInputType.text,
                                maxLines: null,
                                controller: akshayaplace,
                                decoration: const InputDecoration(
                                  hintText: 'Location',
                                  hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                                ),
                                validator: (value) {
                                  return Validate.placevalidator(value!.trim());
                                },),


                              SizedBox(height: 40,),
                              ElevatedButton(
                                  onPressed: () {
                                    if (_akshayakey.currentState!.validate())
                                      {
                                      FirebaseFirestore.instance.collection('akshayas').doc(akshayaid).
                                      set(
                                      {'akshaya_id':akshayaid,
                                      //'userid':widget.name,
                                      'name':akshayaname.text,
                                      'place':akshayaplace.text,
                                      'phone':akshayamobile.text,
                                      'status':1,
                                      'date':DateTime.now()
                                      }
                                      ).then((value) {
                                      showsnackbar(context,"Akshaya added successfully");
                                      Navigator.pop(context);
                                      });

                                      }

                                    //_showalert(context);

                                  },
                                  style: ElevatedButton.styleFrom(
                                    primary: Color(0xffef5350),
                                    shape:  RoundedRectangleBorder(
                                      borderRadius: new BorderRadius.circular(15),
                                    ),
                                  ),
                                  child: const Text(
                                    "  Submit",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                                    ),
                                  )),
                              //SizedBox(height:15),


                            ],
                          ),
                        ),


                      ),
                    ),
                  ),
                ),

              ],
            ),

          ),
        ),
      ),
    );
  }

  _showalert(BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Akshaya added successfully.."),
            //content: Text("Are you sure"),
            actions: [
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Color(0xffef5350),
                  shape:  RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(15),
                  ),
                ),
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context)=> Volunteer_add_service()));
                },
                child: const Text("OK",
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                  ),),
              ),

            ],
          );
        });
  }
  showsnackbar(BuildContext context, String value) {
    ScaffoldMessenger.of(context).showSnackBar(
         SnackBar(behavior: SnackBarBehavior.floating,
            backgroundColor: Colors.red,
            margin:EdgeInsetsDirectional.all(10),
            content: Text(
              value,
              style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
            )));
  }

}
