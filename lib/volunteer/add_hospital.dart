import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:santhwanam_plus/users/serviceprovider.dart';
import 'package:santhwanam_plus/volunteer/volunteer_add_service.dart';
import 'package:uuid/uuid.dart';
import '../validation.dart';
import 'volunteer_add_service.dart';
import 'volunteer_home.dart';

class Add_hospital extends StatefulWidget {
  var hname;
  var hmobile;
  var location;
   Add_hospital({Key? key,this.hname,this.hmobile,this.location}) : super(key: key);

  @override
  _Add_hospitalState createState() => _Add_hospitalState();
}

class _Add_hospitalState extends State<Add_hospital> {
  var uuid = Uuid();
  var hospitalid;
  var _hospitalkey = GlobalKey<FormState>();
  TextEditingController hospitalname = TextEditingController();
  TextEditingController hospitalmobile = TextEditingController();
  TextEditingController hospitalplace = TextEditingController();
  //TextEditingController stock = TextEditingController();
  //TextEditingController ref_number= TextEditingController();
  @override
  void initState() {
    // TODO: implement initState
    hospitalid=uuid.v1();
    //filename = DateTime.now().toString();
    super.initState();
  }




  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Adding New Hosptal"),
        backgroundColor: Color(0xffef5350),
      ),
      body: Form(
        key: _hospitalkey,
        child: SingleChildScrollView(
          child: SafeArea(
            child:Column(
              children: [
                SizedBox(height: 20,),
                SizedBox(height: 30,),
                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Container(width: 400,height: 425,
                    child: Card(shadowColor: Colors.grey,elevation: 2.0,
                      shape:RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      ),color: Colors.white,
                      child: Container(
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Column(
                            children: [

                              SizedBox(height:20),
                              const Text("Fill the form",
                                style: TextStyle(color: Colors.black,fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                                ),),
                              SizedBox(height: 20,),

                              SizedBox(height: 20,),
                              TextFormField(
                                keyboardType: TextInputType.text,
                                maxLines: null,
                                controller: hospitalname,
                                decoration: const InputDecoration(
                                  hintText: 'Name of hospital',
                                  hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                                ),
                                validator: (value) {
                                  return Validate.namevalidator(value!.trim());
                                },
                              ),
                              TextFormField(
                                controller: hospitalmobile,
                                cursorColor: Colors.black,
                                keyboardType: TextInputType.number,
                                decoration: const InputDecoration(
                                  hintText: 'Contact',
                                  hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                                ),
                                validator: (value) {
                                  return Validate.phvalidator(value!.trim());
                                },
                              ),
                              TextFormField(
                                keyboardType: TextInputType.text,
                                maxLines: null,
                                controller: hospitalplace,
                                decoration: const InputDecoration(
                                  hintText: 'Location',
                                  hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                                ),
                                validator: (value) {
                                return Validate.placevalidator(value!.trim());
                              },
                              ),


                              SizedBox(height: 40,),
                              ElevatedButton(
                                  onPressed: () {
                                    if (_hospitalkey.currentState!.validate())
                                    {
                                      FirebaseFirestore.instance.collection('hospitals').doc(hospitalid).
                                      set(
                                          {'hospital_id':hospitalid,
                                            //'userid':widget.name,
                                            'name':hospitalname.text,
                                            'place':hospitalplace.text,
                                            'phone':hospitalmobile.text,
                                            'status':1,
                                            'date':DateTime.now()
                                          }
                                      ).then((value) {
                                        showsnackbar(context,"Hospital added successfully");
                                        Navigator.pop(context);
                                      });

                                    }
                                   // _showalert(context);

                                  },
                                  style: ElevatedButton.styleFrom(
                                    primary: Color(0xffef5350),
                                    shape:  RoundedRectangleBorder(
                                      borderRadius: new BorderRadius.circular(15),
                                    ),
                                  ),
                                  child: const Text(
                                    "  Submit",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                                    ),
                                  )),
                              //SizedBox(height:15),


                            ],
                          ),
                        ),


                      ),
                    ),
                  ),
                ),

              ],
            ),

          ),
        ),
      ),
    );
  }

  showsnackbar(BuildContext context, String value) {
    ScaffoldMessenger.of(context).showSnackBar(
         SnackBar(behavior: SnackBarBehavior.floating,
            backgroundColor: Colors.red,
            margin:EdgeInsetsDirectional.all(10),
            content: Text(
              value,
              style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
            )));
  }

}
