import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class Msg_fromadmin extends StatefulWidget {
  Msg_fromadmin({Key? key}) : super(key: key);

  @override
  _Msg_fromadminState createState() => _Msg_fromadminState();
}

class _Msg_fromadminState extends State<Msg_fromadmin> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Admin Messages",
        ),
        backgroundColor: const Color(0xffef5350),
      ),
      body: SafeArea(
        child: StreamBuilder<QuerySnapshot>(
          stream: FirebaseFirestore.instance
              .collection('volunteer_msg').orderBy('date')
              .snapshots(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return const Center(child: CircularProgressIndicator());
            } else if (snapshot.hasData && snapshot.data!.docs.isEmpty) {
              return const Center(
                  child: Text(
                    "No messages found",
                    style: TextStyle(fontSize: 25),
                  ));
            } else {
              return ListView.builder(
                  itemCount: snapshot.data!.docs.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Column(
                      children: [
                        const SizedBox(
                          height: 5,
                        ),
                        Card(
                          elevation: 10.0,
                          margin: const EdgeInsets.only(
                              left: 10, top: 0, right: 10, bottom: 10),
                          shadowColor: Colors.white,
                          shape: const RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(30),
                                topRight: Radius.circular(30)),
                          ),
                          child: SingleChildScrollView(
                            child: ListTile(
                              onTap: () {},
                              title: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const SizedBox(
                                    height: 5,
                                  ),
                                 Text(snapshot.data?.docs[index]['msg'],),
                                  Text(
                                    snapshot.data?.docs[index]['date'],
                                  ),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    );
                  });
            }
          },
        ),
      ),
    );
  }
}
