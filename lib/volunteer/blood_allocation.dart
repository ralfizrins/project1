import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
class Blood_allocation extends StatefulWidget {
  var name;
  var phone;
  var blood;
  var req_id;
  var place;
  var user_id;
   Blood_allocation({Key? key,this.name,this.blood,this.phone,this.req_id,this.place,this.user_id}) : super(key: key);

  @override
  _Blood_allocationState createState() => _Blood_allocationState();
}

class _Blood_allocationState extends State<Blood_allocation> {
  var donor;
  var donor_id;
  var donor_phone;
  var dname;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Allocation"),
        backgroundColor: Color(0xffef5350),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(

            children: [
              const SizedBox(height: 30,),
              const Text("Select donor :-",
                style: TextStyle(color: Colors.black,fontSize: 25,
                  fontWeight: FontWeight.bold,
                  fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                ),),
              // Text(snapshot.data!.docs['name'],
              //   style: TextStyle(
              //     color: Colors.black,
              //     fontSize: 18,
              //     fontWeight: FontWeight.bold,
              //     fontFamily:
              //     "RobotoSlab-VariableFont_wght.ttf",
              //   ),
              // ),
              StreamBuilder<QuerySnapshot>(
                  stream: FirebaseFirestore.instance
                      .collection('donation').where('blood',isEqualTo: widget.blood)
                      .snapshots(),
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) {
                      return const CupertinoActivityIndicator();
                    }

                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: DropdownButtonFormField(
                        isExpanded: true,
                        value: donor,
                        isDense: true,

                        validator: (value) => value == null
                            ? 'field required'
                            : null,
                        onChanged: (rname) =>
                            setState(() => donor = rname),
                        hint: const Text('DONORS '),
                        items: snapshot.data!.docs
                            .map((DocumentSnapshot document) {
                          return DropdownMenuItem<String>(
                            onTap: (){
                              setState(() {
                                donor_id=document['don_id'];
                                donor_phone=document['phone'];
                                dname=document['name'];
                              });
                            },
                            value: document['don_id'],
                            child: Text(document['name']),
                          );
                        }).toList(),
                      ),
                    );
                  }),
              const SizedBox(height: 30,),
              ElevatedButton(onPressed:(){
                FirebaseFirestore.instance.collection('bloodrequest').doc(widget.req_id).update({
                  'status':2}).then((value) =>  FirebaseFirestore.instance.collection('alloc_list').doc(widget.req_id).set({
                  'request_id':widget.req_id,
                  'user_name':widget.name,
                  'blood':widget.blood,
                  'donor_name':dname,
                  'donor_phone':donor_phone,
                  'donor_id':donor_id,
                  'place':widget.place,
                  'phone':widget.phone,
                  'user_id':widget.user_id,
                  'date':DateTime.now(),
                  'status':1
                }).then((value) =>
                  FirebaseFirestore.instance.collection('notification').doc(DateTime.now().toString()).set({
                    'msg':"hi "+dname+'   '+donor_phone,
                    'user_id':widget.user_id,
                    'date':DateTime.now(),
                  }).then((value) => showsnackbar(context,'Allocated done successfully'))
                )
                );
                Navigator.pop(context);
                ElevatedButton.styleFrom(
                  primary: Color(0xffef5350),
                  shape:  RoundedRectangleBorder(
                    borderRadius:  BorderRadius.circular(15),
                  ),
                );

              }, child: const Text("SUBMIT")),

            ],
          ),
        ),
      ),
    );
  }
  showsnackbar(BuildContext context, String value) {
    ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(behavior: SnackBarBehavior.floating,
            backgroundColor: Colors.red,
            margin:const EdgeInsetsDirectional.all(10),
            content: Text(
              value,
              style: const TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
            )));
  }
}
