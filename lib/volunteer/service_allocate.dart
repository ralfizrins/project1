import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

class service_allocate extends StatefulWidget {
  var uid;
  var name, need, phone, pincode;
  service_allocate(
      {Key? key, this.name, this.uid, this.phone, this.need, this.pincode})
      : super(key: key);

  @override
  _service_allocateState createState() => _service_allocateState();
}

class _service_allocateState extends State<service_allocate> {
  var uuid = Uuid();
  var eqallocationid;
  var _eqallocationkey = GlobalKey<FormState>();
  @override
  void initState() {
    eqallocationid=uuid.v1();
    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("servicerequest"),backgroundColor: const Color(0xffef5350),),
      body: Form(
        key: _eqallocationkey,
        child: SafeArea(
          child: StreamBuilder<QuerySnapshot>(
            stream: FirebaseFirestore.instance
                .collection('servicerequest').where('status',isEqualTo: 1)
                .snapshots(),
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                return const Center(child: CircularProgressIndicator());
              } else if (snapshot.hasData && snapshot.data!.docs.isEmpty) {
                return const Center(
                    child: Text(
                  "No Requests found",
                  style: TextStyle(fontSize: 25),
                ));
              } else {
                return ListView.builder(
                    itemCount: snapshot.data!.docs.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Column(
                        children: [
                          const SizedBox(
                            height: 10,
                          ),
                          Card(
                            elevation: 10.0,
                            margin: const EdgeInsets.only(
                                left: 10, top: 0, right: 10, bottom: 10),
                            shadowColor: Colors.white,
                            shape: const RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(30),
                                  topRight: Radius.circular(30)),
                            ),
                            child: SingleChildScrollView(
                              child: ListTile(
                                onTap: () {},
                                title: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children:  [
                                    const SizedBox(
                                      height: 5,
                                    ),
                                    Row(
                                      children: [
                                        const Text("Request for :- ") ,
                                           Text(snapshot.data?.docs[index]['need'],style: const TextStyle(fontWeight: FontWeight.bold),),
                                      ],
                                    ),

                                    Row(
                                      children: [
                                        const Text(
                                          "Contact :- " ),
                                        Text(snapshot.data?.docs[index]['phone'],style: const TextStyle(color: Colors.blueAccent),),
                                      ],
                                    ),
                                    Text(
                                      "Pincode :- " +
                                          snapshot.data?.docs[index]['pincode'],
                                    ),
                                    const SizedBox(width: 20,),
                                    Center(
                                      child: ElevatedButton(
                                          onPressed: () {
                                            FirebaseFirestore.instance
                                                .collection('servicerequest')
                                                .doc( snapshot.data?.docs[index]['sevice_id'])
                                                .update({

                                              'status': 2,

                                            }).then((value) {
                                              FirebaseFirestore.instance.collection('notification').doc(snapshot.data?.docs[index]['sevice_id']).set({
                                                'msg':"hello "+ snapshot.data?.docs[index]['usere_name']+'  ' +snapshot.data?.docs[index]['need']   +' allocated',
                                                'user_id':snapshot.data?.docs[index]['uid'],
                                                'date':DateTime.now(),
                                              }).then((value) => Navigator.pop(context));


                                            });
                                          },
                                          child: const Text("Allocate")),
                                    ),
                                    const SizedBox(
                                      height: 5,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      );
                    });
              }
            },
          ),
        ),
      ),
    );
  }
}
