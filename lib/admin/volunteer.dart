import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:santhwanam_plus/admin/add_volunteer.dart';
import 'package:santhwanam_plus/admin/admin_home.dart';
import 'package:santhwanam_plus/admin/view_volunteer.dart';
import 'package:uuid/uuid.dart';

class Volunteer extends StatefulWidget {
   Volunteer({Key? key}) : super(key: key);

  @override
  _VolunteerState createState() => _VolunteerState();
}

class _VolunteerState extends State<Volunteer> {
  TextEditingController volunteermessage = TextEditingController();
  var uuid = Uuid();
  var msgid;
  var _msgkey = GlobalKey<FormState>();
@override
  void initState() {
  msgid=uuid.v1();
    // TODO: implement initState
    super.initState();
  }
  var options=["ADD","EDIT","VIEW"];
  @override

  Widget build(BuildContext context) {
    return Scaffold(

      body: SingleChildScrollView(
        child: Form(
        key: _msgkey,
          child: SafeArea(
            child: Column(
              children: [
                const SizedBox(height: 30,),
                const Text("Volunteer Control",
                  style: TextStyle(color: Colors.black,fontSize: 20,
                    fontWeight: FontWeight.bold,
                    fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                  ),),
                const SizedBox(height: 20,),

                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Container(width: MediaQuery.of(context).size.width,height: 300,
                    child: Column(
                      children: [
                        Container(height: 60,width:MediaQuery.of(context).size.width,
                          child: ElevatedButton.icon(
                            label: const Text('ADD Volunteer' ,
                              style: TextStyle(fontSize: 16,
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                              ),),
                            icon: const Icon(Icons.add),
                            style: ElevatedButton.styleFrom(
                              primary: const Color(0xff3BB9FF),
                              shape:  RoundedRectangleBorder(
                                borderRadius:  BorderRadius.circular(15),
                              ),
                            ),
                            onPressed: () {
                              Navigator.push(context, MaterialPageRoute(builder: (context)=> Add_volunteer()));
                            },
                          ),
                        ),
                        const SizedBox(height: 10,),
                        Container(height: 60,width:MediaQuery.of(context).size.width,
                          child: ElevatedButton.icon(
                            label: const Text('EDIT Volunteer' ,
                              style: TextStyle(fontSize: 16,
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                              ),),
                            icon: const Icon(Icons.edit),
                            style: ElevatedButton.styleFrom(
                              primary: const Color(0xff3BB9FF),
                              shape:  RoundedRectangleBorder(
                                borderRadius:  BorderRadius.circular(15),
                              ),
                            ),
                            onPressed: () {
                              Navigator.push(context, MaterialPageRoute(builder: (context)=> View_volunteer()));
                            },
                          ),
                        ),
                        const SizedBox(height: 10,),
                        Container(height: 60,width:MediaQuery.of(context).size.width,
                          child: ElevatedButton.icon(
                            label: const Text('VIEW Volunteers' ,
                              style: TextStyle(fontSize: 16,
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                              ),),
                            icon: const Icon(Icons.view_list),
                            style: ElevatedButton.styleFrom(
                              primary: const Color(0xff3BB9FF),
                              shape:  RoundedRectangleBorder(
                                borderRadius:  BorderRadius.circular(15),
                              ),
                            ),
                            onPressed: () {
                              Navigator.push(context, MaterialPageRoute(builder: (context)=> View_volunteer()));
                            },
                          ),
                        ),
                        const SizedBox(height: 10,),
                        Container(height: 60,width:MediaQuery.of(context).size.width,
                          child: ElevatedButton.icon(
                            label: const Text('Message' ,
                              style: TextStyle(fontSize: 16,
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                              ),),
                            icon: const Icon(Icons.view_list),
                            style: ElevatedButton.styleFrom(
                              primary: const Color(0xff3BB9FF),
                              shape:  RoundedRectangleBorder(
                                borderRadius:  BorderRadius.circular(15),
                              ),
                            ),
                            onPressed: () {
                              _showalert(context);

                              //Navigator.push(context, MaterialPageRoute(builder: (context)=> View_volunteer()));
                            },
                          ),
                        ),
                        const SizedBox(height: 10,),


                      ],
                    ),
                  ),
                ),
                ClipPath(
                  clipper: WaveClipperTwo(reverse: true),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height/6,
                    color: const Color(0xffef5350),
                  ),
                ),
              ],

            ),

          ),
        ),
      ),

    );
  }
  _showalert(BuildContext context) {

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text(
              "Message",
              style: TextStyle(fontFamily: "RobotoSlab-VariableFont_wght.ttf"),
            ),
            content: TextFormField(
              controller:volunteermessage ,
            ),
            actions: [
              TextButton(
                  onPressed: () {
                    FirebaseFirestore.instance
                        .collection('volunteer_msg').doc()
                        .set({
                      'msg': volunteermessage.text,
                      'date':DateTime.now().toString(),
                    }).then((value) {
                      showsnackbar(context, "Message Delivered.");
                    });
                    // Navigator.push(context, MaterialPageRoute(builder: (context) =>Admin_userview()));
                    Navigator.of(context).pop();

                  },
                  child: const Text("SEND")),
            ],
          );
        });
  }

showsnackbar(BuildContext context, String value) {
  ScaffoldMessenger.of(context).showSnackBar(
       SnackBar(behavior: SnackBarBehavior.floating,
          backgroundColor: Colors.red,
          margin:const EdgeInsetsDirectional.all(10),
          content: Text(
            value,
            style: const TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
          )));
}


}
