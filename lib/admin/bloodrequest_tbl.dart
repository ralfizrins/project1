import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:santhwanam_plus/volunteer/blood_allocation.dart';

class Bloodrequest_tblview extends StatefulWidget {
  var name;
  var place;
  var need;
  var mobile;
  var blood;
  var req_id;

  Bloodrequest_tblview(
      {Key? key,
      this.name,
      this.blood,
      this.mobile,
      this.need,
      this.place,
      this.req_id})
      : super(key: key);

  @override
  _Bloodrequest_tblviewState createState() => _Bloodrequest_tblviewState();
}

class _Bloodrequest_tblviewState extends State<Bloodrequest_tblview> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Back "),
        backgroundColor: const Color(0xffef5350),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            color: Colors.white,
            child: StreamBuilder<QuerySnapshot>(
                stream: FirebaseFirestore.instance
                    .collection('bloodrequest')
                    .where('status', isEqualTo: 1)
                    .snapshots(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return const Center(child: CircularProgressIndicator());
                  } else if (snapshot.hasData && snapshot.data!.docs.isEmpty) {
                    return const Center(
                        child: Text(
                      "No data found",
                      style: TextStyle(fontSize: 25),
                    ));
                  } else {
                    return ListView.builder(
                        itemCount: snapshot.data!.docs.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Card(
                            elevation: 10.0,
                            margin: const EdgeInsets.only(
                                left: 10, top: 0, right: 10, bottom: 10),
                            shadowColor: Colors.white,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15),
                            ),
                            child: SingleChildScrollView(
                              child: Column(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(17.0),
                                    child: Container(
                                      width: 400,
                                      height: 320,
                                      child: Card(
                                        shadowColor: Colors.grey,
                                        elevation: 2.0,
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(20.0),
                                        ),
                                        color: Colors.white,
                                        child: Column(
                                          children: [
                                            Container(
                                              width: 400,
                                              height: 40,
                                              decoration: BoxDecoration(borderRadius: BorderRadius.circular(5),color: const Color(0xffef5350),),
                                              // color: const Color(0xffef5350),
                                              child: const Padding(
                                                padding: EdgeInsets.only(left: 10),
                                                child: Text(
                                                  "Request",
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 18,
                                                    fontWeight: FontWeight.bold,
                                                    fontFamily:
                                                        "RobotoSlab-VariableFont_wght.ttf",
                                                  ),
                                                ),
                                              ),
                                            ),
                                            const SizedBox(
                                              height: 10,
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(left: 10),
                                              child: Row(
                                                children: [
                                                  const Text(
                                                    "Need :- ",
                                                    style: TextStyle(
                                                      color: Colors.black,
                                                      fontSize: 18,
                                                      fontWeight: FontWeight.bold,
                                                      fontFamily:
                                                          "RobotoSlab-VariableFont_wght.ttf",
                                                    ),
                                                  ),
                                                  Text(
                                                    snapshot.data!.docs[index]
                                                        ['need'],
                                                    style: const TextStyle(
                                                      color: Colors.black,
                                                      fontSize: 18,
                                                      fontWeight: FontWeight.bold,
                                                      fontFamily:
                                                          "RobotoSlab-VariableFont_wght.ttf",
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            const SizedBox(
                                              height: 20,
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(left: 10),
                                              child: Row(
                                                children: [
                                                  const Text(
                                                    "Name:-",
                                                    style: TextStyle(
                                                      color: Colors.black,
                                                      fontSize: 18,
                                                      fontWeight: FontWeight.bold,
                                                      fontFamily:
                                                          "RobotoSlab-VariableFont_wght.ttf",
                                                    ),
                                                  ),
                                                  Text(
                                                    snapshot.data!.docs[index]
                                                        ['name'],
                                                    style: const TextStyle(
                                                      color: Colors.black,
                                                      fontSize: 18,
                                                      fontWeight: FontWeight.bold,
                                                      fontFamily:
                                                          "RobotoSlab-VariableFont_wght.ttf",
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            const SizedBox(
                                              height: 10,
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(left: 10),
                                              child: Row(
                                                children: [
                                                  const Text(
                                                    "Place :- ",
                                                    style: TextStyle(
                                                      color: Colors.black,
                                                      fontSize: 18,
                                                      fontWeight: FontWeight.bold,
                                                      fontFamily:
                                                          "RobotoSlab-VariableFont_wght.ttf",
                                                    ),
                                                  ),
                                                  Text(
                                                    snapshot.data!.docs[index]
                                                        ['place'],
                                                    style: const TextStyle(
                                                      color: Colors.black,
                                                      fontSize: 18,
                                                      fontWeight: FontWeight.bold,
                                                      fontFamily:
                                                          "RobotoSlab-VariableFont_wght.ttf",
                                                    ),
                                                  ),
                                                  const SizedBox(
                                                    width: 20,
                                                  ),
                                                  Container(
                                                    child: CircleAvatar(
                                                      child: Text(
                                                        snapshot.data!.docs[index]
                                                            ['blood'],
                                                        style: const TextStyle(
                                                          color: Colors.white,
                                                          fontSize: 18,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontFamily:
                                                              "RobotoSlab-VariableFont_wght.ttf",
                                                        ),
                                                      ),
                                                      backgroundColor:
                                                          Color(0xffef5350),
                                                      maxRadius: 30.0,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            const SizedBox(
                                              height: 20,
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(left: 10),
                                              child: Row(
                                                children: [
                                                  const Text(
                                                    "Mobile:-",
                                                    style: TextStyle(
                                                      color: Colors.black,
                                                      fontSize: 16,
                                                      fontWeight: FontWeight.bold,
                                                      fontFamily:
                                                          "RobotoSlab-VariableFont_wght.ttf",
                                                    ),
                                                  ),
                                                  Text(
                                                    snapshot.data!.docs[index]
                                                        ['phone'],
                                                    style: const TextStyle(
                                                      color: Colors.blue,
                                                      fontSize: 18,
                                                      fontWeight: FontWeight.bold,
                                                      fontFamily:
                                                          "RobotoSlab-VariableFont_wght.ttf",
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            const SizedBox(
                                              height: 10,
                                            ),
                                            ElevatedButton(
                                                onPressed: () {
                                                  Navigator.push(context, MaterialPageRoute(builder: (context) =>
                                                              Blood_allocation(
                                                                req_id: snapshot.data?.docs[index]['req_id'],
                                                                name: snapshot.data!.docs[index]['name'],
                                                                phone: snapshot.data!.docs[index]['phone'],
                                                                blood: snapshot.data!.docs[index]['blood'],
                                                                place: snapshot.data!.docs[index]['place'],
                                                                user_id: snapshot.data!.docs[index]['userid'],
                                                              ))); //}
                                                },
                                                style: ElevatedButton.styleFrom(
                                                  primary: const Color(0xffef5350),
                                                  shape:
                                                       RoundedRectangleBorder(
                                                    borderRadius:
                                                         BorderRadius
                                                            .circular(10),
                                                  ),
                                                ),
                                                child: const Text(
                                                  "  Allocate  ",
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    fontWeight: FontWeight.bold,
                                                    fontFamily:
                                                        "RobotoSlab-VariableFont_wght.ttf",
                                                  ),
                                                )),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        });
                  }
                }),
          ),
        ),
      ),
    );
  }
}
