import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:santhwanam_plus/admin/admin_home.dart';
import 'package:santhwanam_plus/admin/users.dart';
import 'package:santhwanam_plus/validation.dart';
import 'package:date_time_picker/date_time_picker.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';

import 'admin_careview.dart';

class Care_fullview extends StatefulWidget {
  var uid;
  var email;
  var name;
  var phonenumber;
  var dob;
  var age;
  var address;
  var pincode;
  var proof;
  var bloodgroup;
  var disease;
  var imgurl;

  Care_fullview({Key? key,
    this.uid,this.email,this.name,this.phonenumber,this.dob,this.address,this.bloodgroup,this.age,this.disease,this.pincode,this.proof,this.imgurl}) : super(key: key);

  @override
  _Care_fullviewState createState() => _Care_fullviewState();
}

class _Care_fullviewState extends State<Care_fullview> {
  TextEditingController emailInputcontroller = TextEditingController();
  TextEditingController phone = TextEditingController();
  TextEditingController name = TextEditingController();
  TextEditingController address = TextEditingController();
  TextEditingController pincode = TextEditingController();
  TextEditingController age = TextEditingController();
  TextEditingController proof = TextEditingController();
  TextEditingController disease = TextEditingController();
  TextEditingController blood = TextEditingController();
  var filename;
  var url;
  var bloodgroup;
  var assume="Value from DB";//for demo
 TextEditingController _takedob=new TextEditingController(); //For late initialisation:- late Keyword
  late String _valueChanged3;
  late String _valueToValidate3;
  late String _valueSaved3;
  final ImagePicker _picker = ImagePicker();// For pick Image
  XFile ? _image; // For accept Null:-?

  void initState() {
    // TODO: implement initState
    name.text = widget.name;
    emailInputcontroller.text = widget.email;
    phone.text = widget.phonenumber;
    _takedob.text=widget.dob!=null?widget.dob:"DOB not set";
    address.text=widget.address!=null?widget.address:"address not set";
    pincode.text=widget.pincode!=null?widget.pincode:"pincode not set";
    proof.text=widget.proof!=null?widget.proof:"proof not set";
    blood.text=widget.bloodgroup!=null?widget.bloodgroup:"bloodgroup not set";
    age.text=widget.age!=null?widget.age:"Age not set";
    disease.text=widget.disease!=null?widget.disease:"disease not set";
    filename = DateTime.now().toString();
    //_takedob=new TextEditingController(text:DateTime.now().toString());
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    var visible=true;

    return Scaffold(
      appBar: AppBar(
        title: Text("Back To Home"),
        backgroundColor: Color(0xffef5350),
      ),
      body: SingleChildScrollView(
        child: SafeArea(
          child:Column(
            children: [

              SizedBox(height: 20,),
              const Text("Care Home Profile",
                style: TextStyle(color: Colors.black,fontSize: 20,
                  fontWeight: FontWeight.bold,
                  fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                ),),
              SizedBox(height: 20,),
              Column(
                children: [
                  GestureDetector(
                    onTap: (){
                      _getpics();
                    },
                    child:   _image != null
                        ? ClipRect(
                      child: Image.file(
                        File(_image!.path), //null check operator added:-!
                      ),
                    )
                        : Container(
                      width: 80, //MediaQuery.of(context).size.width,
                      height: 80, //MediaQuery.of(context).size.height,
                      child: Image.network(widget.imgurl),
                    ),

                  ),

                  ElevatedButton(
                    onPressed: () {
                      var ref = FirebaseStorage.instance
                          .ref()
                          .child('careperson/$filename');
                      UploadTask uploadTask = ref.putFile(File(_image!.path));

                      uploadTask.then((res) async {
                        url = (await ref.getDownloadURL()).toString();
                      }).then((value) => FirebaseFirestore.instance
                          .collection('careperson')
                          .doc(widget.uid)
                          .update({
                        'imgurl': url,
                      }).then((value) {
                        showsnackbar(context, "Image uploaded");
                      }));

                    },
                    child: Text("Uplaod Image"),
                  ),
                  SizedBox(height: 20,),
                  //after profile pic
                ],
              ),
              SizedBox(height: 30,),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: Container(width: 400,height: 650,
                  child: Card(shadowColor: Colors.grey,elevation: 2.0,
                    shape:RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0),
                    ),color: Colors.white,
                    child: Container(
                      child: Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Column(
                          children: [

                            SizedBox(height:20),
                            TextFormField(
                              controller: name,
                              readOnly: true,
                              decoration: const InputDecoration(
                                hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                              ),
                              validator: (value) {
                                return Validate.namevalidator(value!.trim());
                              },
                            ),//Name
                            TextFormField(
                              controller: emailInputcontroller,
                              readOnly: true,
                              decoration: const InputDecoration(

                                hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                              ),
                              validator: (value) {
                                return Validate.emailValidator(value!.trim());
                              },
                            ),//Email
                            TextFormField(
                              controller: phone,
                              //readOnly: true,
                              decoration: const InputDecoration(
                                hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                              ),
                              validator: (value) {
                                return Validate.phvalidator(value!.trim());
                              },
                            ),//mobile
                            AbsorbPointer(
                              child: DateTimePicker(
                                type: DateTimePickerType.date,
                                //dateMask: 'yyyy/MM/dd',
                                fieldLabelText: 'Date of birth',
                                controller: _takedob,
                                firstDate: DateTime(1990),
                                lastDate: DateTime(2050),
                                icon: Icon(Icons.event),
                                dateLabelText: 'Date of birth',
                                //locale: Locale('pt', 'BR'),
                                onChanged: (val) => setState(() => _valueChanged3 = val),
                                validator: (val) {
                                  setState(() => _valueToValidate3 = val ?? '');
                                  return null;
                                },
                                onSaved: (val) => setState(() => _valueSaved3 = val ?? ''),
                              ),
                            ),//dob
                            TextFormField(
                              keyboardType: TextInputType.multiline,
                              maxLines: null,
                              //readOnly: true,
                              controller: address,
                              decoration: const InputDecoration(
                                hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                              ),
                              validator: (value) {
                                return Validate.addressvalidator(value!.trim());
                              },),//address_Multilined
                            TextFormField(
                              controller: pincode,
                              //readOnly: true,
                              cursorColor: Colors.black,
                              keyboardType: TextInputType.number,
                              decoration: const InputDecoration(
                                hintText: 'Pincode',
                                hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                              ),
                              validator: (value) {
                                return Validate.pincodevalidator(value!.trim());
                              },
                            ), //pincode
                            TextFormField(
                              controller: proof,
                              readOnly: true,
                              cursorColor: Colors.black,
                              keyboardType: TextInputType.text,
                              decoration: const InputDecoration(
                                hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                              ),
                              validator: (value) {
                                return Validate.proofvalidator(value!.trim());
                              },
                            ), //proof
                            TextFormField(
                              controller: blood,
                              readOnly: true,
                              cursorColor: Colors.black,
                              keyboardType: TextInputType.text,
                              decoration: const InputDecoration(
                                hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                              ),
                              validator: (value) {
                                return Validate.namevalidator(value!.trim());
                              },
                            ),//job

                            TextFormField(
                              controller: disease,
                              readOnly: true,
                              cursorColor: Colors.black,
                              keyboardType: TextInputType.text,
                              decoration: const InputDecoration(
                                hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                              ),
                              validator: (value) {
                                return Validate.namevalidator(value!.trim());
                              },
                            ),//job
                            TextFormField(
                              controller: age,
                              readOnly: true,
                              cursorColor: Colors.black,
                              keyboardType: TextInputType.text,
                              decoration: const InputDecoration(
                                hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                              ),
                              validator: (value) {
                                return Validate.careagevalidator(value!.trim());
                              },
                            ),

                            SizedBox(height: 10,),

                            SizedBox(height: 30,),
                            Row(
                              children: [
                                ElevatedButton(
                                    onPressed: () {
                                      FirebaseFirestore.instance
                                          .collection('careperson')
                                          .doc(widget.uid)
                                          .update({
                                        'phone': phone.text,
                                        'address': address.text,
                                        'pincode': pincode.text,

                                      }).then((value) {
                                        showsnackbar(
                                            context, "updated successfully");

                                        Navigator.pop(context);
                                      });

                                    },
                                    style: ElevatedButton.styleFrom(
                                      primary: Color(0xffef5350),
                                      shape:  RoundedRectangleBorder(
                                        borderRadius:  BorderRadius.circular(15),
                                      ),
                                    ),
                                    child: const Text(
                                      "UPDATE",
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                                      ),
                                    )),
                                SizedBox(width: 10,),
                                ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    primary: Color(0xffef5350),
                                    shape:  RoundedRectangleBorder(
                                      borderRadius:  BorderRadius.circular(15),
                                    ),
                                  ),
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                  child: const Text("BACK",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                                    ),),
                                ),
                                SizedBox(width: 20,),

                                ElevatedButton(
                                    onPressed: () {
                                      _deletalert(context);

                                    },
                                    style: ElevatedButton.styleFrom(
                                      primary: Color(0xffef5350),
                                      shape:  RoundedRectangleBorder(
                                        borderRadius:  BorderRadius.circular(15),
                                      ),
                                    ),
                                    child: const Text(
                                      "  DELETE  ",
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                                      ),
                                    )),
                              ],
                            ),
                            //SizedBox(height:15),


                          ],
                        ),
                      ),


                    ),
                  ),
                ),
              ),

            ],
          ),

        ),
      ),
    );
  }

  _imgbrowse()async{
    final XFile? photo = await _picker.pickImage(source: ImageSource.camera);
    //final XFile? image = await _picker.pickImage(source: ImageSource.gallery);
    setState(() {
      _image=photo;
    });
  }
  _imggallery()async{
    //final XFile? photo = await _picker.pickImage(source: ImageSource.camera);
    final XFile? image = await _picker.pickImage(source: ImageSource.gallery);
    setState(() {
      _image=image;
    });}
  _getpics(){
    showModalBottomSheet(
        context: context,
        builder: (context){
          return Container(
            child: Wrap(
              children: [
                Expanded(
                  child: ListTile(
                      title: Text("Photo Gallery"),
                      leading: Icon(Icons.image),
                      onTap: (){
                        _imggallery();
                      }

                  ),
                ),
                Expanded(
                  child: ListTile(
                    title: Text("Camera"),
                    leading: Icon(Icons.camera),
                    onTap: (){
                      _imgbrowse();
                    },
                  ),
                ),

              ],

            ),

          );
        }
    );
  }
  showsnackbar(BuildContext context, String value) {
    ScaffoldMessenger.of(context).showSnackBar(
         SnackBar(behavior: SnackBarBehavior.floating,
            backgroundColor: Colors.red,
            margin:EdgeInsetsDirectional.all(10),
            content: Text(
              value,
              style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
            )));
  }
  _deletalert(BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text(
              "Are you sure , you want to delete ?",
              style: TextStyle(fontFamily: "RobotoSlab-VariableFont_wght.ttf"),
            ),
            //content: Text("Are you sure"),
            actions: [
              TextButton(
                  onPressed: () {
                    FirebaseFirestore.instance
                        .collection('careperson')
                        .doc(widget.uid)
                        .update({
                      'status': 0,
                    }).then((value) {
                      showsnackbar(context, "Deleted Successfully.");
                    });
                    Navigator.push(context, MaterialPageRoute(builder: (context) => Admin_careview()));
                    //Navigator.of(context).pop();

                  },
                  child: Text("OK")),
              TextButton(
                  onPressed: () {

                    Navigator.of(context).pop();
                  },
                  child: Text("CANCEL")),
            ],
          );
        });
  }

}
