import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:santhwanam_plus/admin/admin_bloodview.dart';
//import 'package:santhwanam_plus/blood.dart';
import 'package:santhwanam_plus/users/bloodbank.dart';
import 'package:santhwanam_plus/admin/carehome.dart';
import 'package:santhwanam_plus/users/loginpage.dart';
import 'package:santhwanam_plus/users/serviceprovider.dart';
import 'package:santhwanam_plus/admin/users.dart';
import 'package:santhwanam_plus/admin/volunteer.dart';
import 'package:santhwanam_plus/users/bloodbank.dart';

void main() => runApp(Adminhome());

class Adminhome extends StatelessWidget {



  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: DefaultTabController(
        length: 5,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: const Color(0xffef5350),
            title: Row(
              children: [
                Text('Dashboard',style: TextStyle(fontFamily: 'RobotoSlab-VariableFont_wght.ttf'),),
                SizedBox(width: 140,),
                IconButton(
                  icon: const Icon(Icons.logout),
                  color: Colors.white,
                  tooltip: 'menu icon ',
                  onPressed: () {
                    FirebaseAuth.instance.signOut().then((value) =>
                        Navigator.pushReplacement(context,
                            MaterialPageRoute(builder: (context) =>
                                LoginPage(
                                ))));
                  },
                ),
              ],
            ),
            //title: const Text('Dashboard',style: TextStyle(fontFamily: 'RobotoSlab-VariableFont_wght.ttf'),),
            bottom: const TabBar(
              indicatorColor: Colors.white,
              tabs: [

                Tab(icon: Icon(Icons.verified_user), text: "Volunteers "),
                Tab(icon: Icon(Icons.supervised_user_circle), text: "Users"),
                Tab(icon: Icon(Icons.bloodtype_sharp), text: "Blood Bank"),
                Tab(icon: Icon(Icons.accessible), text: "CareHome"),
                Tab(icon: Icon(Icons.wifi), text: "Service Provider"),
              ],
            ),
          ),
          body: TabBarView(
            children: [
              Volunteer(),
              Users(),
              Admin_bloodview(),
              Carehome(),
              ServiceProvider(),
            ],
          ),
        ),
      ),
    );
  }
}
