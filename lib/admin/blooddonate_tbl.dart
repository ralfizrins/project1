import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';

class Blooddonate_tbl extends StatefulWidget {
  var place;
  var mobile;
  var blood;
  var name;

  Blooddonate_tbl({Key? key, this.place, this.mobile, this.name, this.blood})
      : super(key: key);

  @override
  _Blooddonate_tblState createState() => _Blooddonate_tblState();
}

class _Blooddonate_tblState extends State<Blooddonate_tbl> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Back "),
        backgroundColor: const Color(0xffef5350),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [

              const Padding(
                padding: EdgeInsets.only(left: 15,top: 10),
                child: Text(
                  "Donation List",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                  ),

                ),
              ),
              const SizedBox(height: 20,),
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                color: Colors.white,
                child: StreamBuilder<QuerySnapshot>(
                    stream: FirebaseFirestore.instance
                        .collection('donation')
                        .where('status', isEqualTo: 1)
                        .snapshots(),
                    builder: (context, snapshot) {
                      if (!snapshot.hasData) {
                        return const Center(child: CircularProgressIndicator());
                      } else if (snapshot.hasData &&
                          snapshot.data!.docs.isEmpty) {
                        return const Center(
                            child: Text(
                          "No data found",
                          style: TextStyle(fontSize: 25),
                        ));
                      } else {
                        return ListView.builder(
                            itemCount: snapshot.data!.docs.length,
                            itemBuilder: (BuildContext context, int index) {
                              return Card(
                                elevation: 10.0,
                                margin: const EdgeInsets.only(
                                    left: 10, top: 0, right: 10, bottom: 10),
                                shadowColor: Colors.white,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                child: SingleChildScrollView(
                                  child: ListTile(
                                    onTap: () {
                                      //Navigator.push(context, MaterialPageRoute(builder: (context)=> User_fullview()));
                                    },
                                    title: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Row(
                                          children: [
                                            Text(
                                              snapshot.data!.docs[index]['name'],
                                              style: const TextStyle(
                                                  fontFamily:
                                                      'RobotoSlab-VariableFont_wght.ttf',
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 18),
                                            ),
                                          ],
                                        ),
                                        const SizedBox(
                                          height: 10,
                                        ),
                                        Row(
                                          children: [
                                            Text(snapshot.data!.docs[index]['place']),
                                          ],
                                        ), //place
                                        Row(
                                          children: [
                                            Text(snapshot.data!.docs[index]['phone'],style: const TextStyle(color: Colors.blue),),
                                          ],
                                        ), //mobile
                                      ],
                                    ),
                                    trailing: CircleAvatar(
                                      child: Text(
                                        snapshot.data!.docs[index]['blood'],
                                        style: const TextStyle(
                                          color: Colors.white70,
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold,
                                          fontFamily:
                                              "RobotoSlab-VariableFont_wght.ttf",
                                        ),
                                      ),
                                      backgroundColor: const Color(0xffef5350),
                                      maxRadius: 30.0,
                                    ),
                                  ),
                                ),
                              );
                            });
                      }
                    }),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
