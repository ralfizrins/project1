import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:santhwanam_plus/admin/admin_home.dart';
import 'package:santhwanam_plus/validation.dart';
import 'package:date_time_picker/date_time_picker.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'package:santhwanam_plus/admin/volunteer.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';

class Add_volunteer extends StatefulWidget {
  var uid;
  var email;
  var phone;
  var name;
  var address;
  var job;
  var bloodgroup;
  var proof;
  var password;
  var blood;
  var vpincode;
  var dob;

   Add_volunteer({Key? key,this.uid,this.email,this.phone
   ,this.name
   ,this.address,this.vpincode,this.job,this.bloodgroup,this.proof,this.dob,this.password,this.blood,}) : super(key: key);

  @override
  _Add_volunteerState createState() => _Add_volunteerState();
}

class _Add_volunteerState extends State<Add_volunteer> {
  TextEditingController emailInputcontroller = TextEditingController();
  TextEditingController passwordInputcontroller = TextEditingController();
  TextEditingController phone = TextEditingController();
  TextEditingController dob= TextEditingController();
  TextEditingController name = TextEditingController();
  TextEditingController address = TextEditingController();
  TextEditingController vpincode = TextEditingController();
  TextEditingController proof = TextEditingController();
  TextEditingController job = TextEditingController();
  TextEditingController blood = TextEditingController();
  var _addvolunteerkey = GlobalKey<FormState>();
  var bloodgroup;
  var url;
  var filename;


  var assume="Value from DB";//for demo
  late TextEditingController _takedob; //For late initialisation:- late Keyword
  late String _valueChanged3;
  late String _valueToValidate3;
  late String _valueSaved3;
  final ImagePicker _picker = ImagePicker();// For pick Image
  XFile ? _image; // For accept Null:-?

  void initState() {
    // TODO: implement initState
    filename=DateTime.now().toString();
    _takedob=new TextEditingController(text:DateTime.now().toString());
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    var visible=true;

    return Scaffold(

      body: SingleChildScrollView(
        child: Form(
          key: _addvolunteerkey,
          child: SafeArea(
            child:Column(
              children: [
                ClipPath(
                  clipper: WaveClipperOne(),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height/6,
                    color: const Color(0xffef5350),
                  ),
                ),

                const SizedBox(height: 20,),
                const Text("Creating Volunteer...",
                  style: TextStyle(color: Colors.black,fontSize: 20,
                    fontWeight: FontWeight.bold,
                    fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                  ),),
                const SizedBox(height: 20,),
                Column(
                  children: [
                    GestureDetector(
                      onTap: (){
                        _getpics();
                      },
                      child: Container(
                        width:30,//MediaQuery.of(context).size.width,
                        height:30, //MediaQuery.of(context).size.height,
                        child: _image!=null? ClipRect(
                          child: Image.file(
                            File(_image!.path),//null check operator added:-!
                          ),
                        ):Container(child: Icon(Icons.insert_drive_file,size: 50,),),
                      ),
                    ),
                    const SizedBox(height: 20,),
                    const Text("Upload profile picture",style: TextStyle(color: Colors.black,fontSize: 10,
                      fontWeight: FontWeight.bold,
                      fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                    ),)
                  ],
                ),
                const SizedBox(height: 30,),
                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Container(width: 400,height:1000,
                    child: Card(shadowColor: Colors.grey,elevation: 2.0,
                      shape:RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      ),color: Colors.white,
                      child: Container(
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Column(
                            children: [

                              const SizedBox(height:20),
                              TextFormField(

                                controller: name,
                                decoration: const InputDecoration(
                                  hintText: 'Name',
                                  hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                                ),
                                validator: (value) {
                                  return Validate.namevalidator(value!.trim());
                                },
                              ),
                              const SizedBox(height:5),//Name
                              TextFormField(
                                //readOnly: true,
                                controller: emailInputcontroller,
                                decoration: const InputDecoration(
                                  hintText: 'Email',
                                  hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                                ),
                                validator: (value) {
                                  return Validate.emailValidator(value!.trim());
                                },
                              ), const SizedBox(height:5),
                              //Email
                              TextFormField(
                                //readOnly: true,
                                controller: passwordInputcontroller,
                                decoration: const InputDecoration(
                                  hintText: 'Password',
                                  hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                                ),
                                validator: (value) {
                                  return Validate.passwordvalidator(
                                      value!.trim());
                                },
                              ), const SizedBox(height:5),
                              TextFormField(
                                //readOnly: true,
                                controller: phone,
                                decoration: const InputDecoration(
                                  hintText: 'phone',
                                  hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                                ),
                                validator: (value) {
                                  return Validate.phvalidator(value!.trim());
                                },
                              ), const SizedBox(height:5),//mobile
                              DateTimePicker(
                                type: DateTimePickerType.date,
                                //dateMask: 'yyyy/MM/dd',
                                fieldLabelText: 'Date of birth',
                                controller: dob,
                                firstDate: DateTime(1990),
                                lastDate: DateTime(2050),
                                icon: Icon(Icons.event),
                                dateLabelText: 'Date of birth',
                                //locale: Locale('pt', 'BR'),
                                onChanged: (val) => setState(() => _valueChanged3 = val),
                                validator: (value) {
                                  return Validate.dobvalidator(value!.trim());
                                },
                                onSaved: (val) => setState(() => _valueSaved3 = val ?? ''),
                              ), const SizedBox(height:5),//dob
                              TextFormField(
                                keyboardType: TextInputType.multiline,
                                validator: (value) {
                                  return Validate.addressvalidator(value!.trim());
                                },
                                controller: address,
                                decoration: const InputDecoration(
                                  hintText: 'Address',
                                  hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                                ),



                              ), const SizedBox(height:5),
                              TextFormField(
                                //readOnly: true,
                                controller: vpincode,
                                decoration: const InputDecoration(
                                  hintText: 'pincode',
                                  hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                                ),
                                validator: (value) {
                                  return Validate.pincodevalidator(value!.trim());
                                },
                              ),

                              const SizedBox(height:5), //pincode//address_Multilined

                              TextFormField(
                                controller: proof,
                                cursorColor: Colors.black,
                                keyboardType: TextInputType.text,
                                decoration: const InputDecoration(
                                  hintText: 'ID(Adhar/passport/Liscemse)',
                                  hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                                ),
                                validator: (value) {
                                return Validate.proofvalidator(value!.trim());
                              },

                              ), const SizedBox(height:5), //proof
                              DropdownButtonFormField<String>(
                                value: bloodgroup,
                                decoration: const InputDecoration(
                                  hintText: "Blood Group",

                                ),
                                onChanged: (ctgry) =>
                                    setState(() => bloodgroup = ctgry),
                                validator: (value) => value == null ? 'field required' : null,
                                items: ['A+','A-','B+','B-','AB+','AB-','O+','O-']
                                    .map<DropdownMenuItem<String>>(
                                        (String value) {
                                      return DropdownMenuItem<String>(
                                        value: value,
                                        child: Text(value),
                                      );
                                    }).toList(),
                              ), const SizedBox(height:5),//blood
                              TextFormField(
                                controller: job,
                                cursorColor: Colors.black,
                                keyboardType: TextInputType.text,
                                decoration: const InputDecoration(
                                  hintText: 'Job',
                                  hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                                ),validator: (value) {
                                return Validate.jobvalidator(value!.trim());
                              },

                              ), //job



                              SizedBox(height: 70,),
                              ElevatedButton(
                                  onPressed: () {
                                    if(_addvolunteerkey .currentState!.validate() )
                                    {
                                      var ref =
                                      FirebaseStorage.instance.ref().child('volunteer/$filename');
                                      UploadTask uploadTask = ref.putFile(File(_image!.path));


                                      uploadTask.then((res) async{
                                        url = (await ref.getDownloadURL()).toString();

                                      }).then((value) {
                                        FirebaseAuth.instance.createUserWithEmailAndPassword
                                          (email:emailInputcontroller.text.trim(),
                                            password: passwordInputcontroller.text.trim()).
                                        then((user) => FirebaseFirestore.instance.collection('login').doc(user.user!.uid).set({
                                          'uid':user.user!.uid,
                                          'email':emailInputcontroller.text.trim(),
                                          'password':passwordInputcontroller.text.trim(),
                                          'status':1,
                                          'userstatus':2,
                                          'date':DateTime.now()
                                        }).then((value) => FirebaseFirestore.instance.collection('volunteer').doc(user.user!.uid).
                                        set(
                                            {'uid':user.user!.uid,
                                              'email':emailInputcontroller.text.trim(),
                                              'password':passwordInputcontroller.text.trim(),
                                              'name':name.text,
                                              'phone':phone.text,
                                              'address':address.text,
                                              'dob':dob.text,
                                              'pincode':vpincode.text,
                                              'proof':proof.text,
                                              'job':job.text,
                                              'imgurl':url,
                                              'blood':bloodgroup,
                                              'status':1,
                                              'date':DateTime.now()
                                            }
                                        ).then((value) {
                                          showsnackbar(context,"Registered successfully");

                                          Navigator.pop(context);
                                        }))).catchError((e)=> print(e));
                                      });


                                    }
                                    else
                                      showsnackbar(context,"please check the fields");


                                    //Go to admin home

                                  },
                                  style: ElevatedButton.styleFrom(
                                    primary: const Color(0xffef5350),
                                    shape:   RoundedRectangleBorder(
                                      borderRadius:  BorderRadius.circular(15),
                                    ),
                                  ),

                                  child: const Text(
                                    "  Make As Volunteer",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                                    ),
                                  )),



                            ],
                          ),
                        ),


                      ),
                    ),
                  ),
                ),

              ],
            ),

          ),
        ),
      ),
    );
  }
  _imgbrowse()async{
    final XFile? photo = await _picker.pickImage(source: ImageSource.camera);
    //final XFile? image = await _picker.pickImage(source: ImageSource.gallery);
    setState(() {
      _image=photo;
    });
  }
  _imggallery()async{
    //final XFile? photo = await _picker.pickImage(source: ImageSource.camera);
    final XFile? image = await _picker.pickImage(source: ImageSource.gallery);
    setState(() {
      _image=image;
    });}
  _getpics(){
    showModalBottomSheet(
        context: context,
        builder: (context){
          return Container(
            child: Wrap(
              children: [
                Expanded(
                  child: ListTile(
                      title: Text("Photo Gallery"),
                      leading: Icon(Icons.image),
                      onTap: (){
                        _imggallery();
                      }

                  ),
                ),
                Expanded(
                  child: ListTile(
                    title: Text("Camera"),
                    leading: Icon(Icons.camera),
                    onTap: (){
                      _imgbrowse();
                    },
                  ),
                ),

              ],

            ),

          );
        }
    );
  }

  showsnackbar(BuildContext context, String value) {
    ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
            behavior: SnackBarBehavior.floating,
            backgroundColor: Colors.red,
            margin: EdgeInsetsDirectional.all(10),
            content: Text(
          value,
          style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
        )));
  }

}
