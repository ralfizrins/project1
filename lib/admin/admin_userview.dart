import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';

import 'user_fullview.dart';


class Admin_userview extends StatefulWidget {





  @override
  _Admin_userviewState createState() => _Admin_userviewState();
}

class _Admin_userviewState extends State<Admin_userview> {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
      backgroundColor: Color(0xffef5350),),

      body: SingleChildScrollView(
        child: SafeArea(
          child:Column(
            children: [

              const SizedBox(height: 10,),
              const Text("User Details",
                style: TextStyle(color: Colors.black,fontSize: 20,
                  fontWeight: FontWeight.bold,
                  fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                ),),
              const SizedBox(height: 30,),
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                color: Colors.white,
                child: StreamBuilder<QuerySnapshot>(
                  stream: FirebaseFirestore.instance.collection('user').where('status',isEqualTo: 1).snapshots(),
                  builder: (context, snapshot) {
                    if(!snapshot.hasData) {
                      return const Center(child: CircularProgressIndicator());
                    } else  if(snapshot.hasData && snapshot.data!.docs.isEmpty) {
                      return const Center(child: Text("No data found",style: TextStyle(fontSize: 25),));
                    } else {
                      return ListView.builder(
                        itemCount: snapshot.data!.docs.length,

                        itemBuilder: (BuildContext context, int index) {
                          return Card(
                            elevation: 10.0,
                            margin: const EdgeInsets.only(
                                left: 10, top: 0, right: 10, bottom: 10),
                            shadowColor: Colors.white,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15),
                            ),
                            child: SingleChildScrollView(
                              child: ListTile(
                                onTap: () {
                                  Navigator.push(context, MaterialPageRoute(builder: (context)=> User_fullview(
                                    uid:snapshot.data?.docs[index]['uid'],
                                    uemail: snapshot.data!.docs[index]['email'],
                                    uname: snapshot.data!.docs[index]['name'],
                                    uphonenumber: snapshot.data!.docs[index]['phone'],
                                    uaddress: snapshot.data!.docs[index]['address'],
                                    dob: snapshot.data!.docs[index]['dob'],
                                    ubloodgroup: snapshot.data!.docs[index]['bloodgroup'],
                                    ujob: snapshot.data!.docs[index]['job'],
                                    upincode: snapshot.data!.docs[index]['pincode'],
                                    uproof: snapshot.data!.docs[index]['proof'],

                                  )));

                                },
                                title: Row(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(left: 23),
                                      child: Text(
                                       snapshot.data?.docs[index]['name'],
                                        style: const TextStyle(
                                            fontFamily:
                                            'RobotoSlab-VariableFont_wght.ttf',
                                            fontWeight: FontWeight.bold,
                                            fontSize: 18),
                                      ),
                                    ),

                                  ],
                                ),

                              ),
                            ),
                          );
                        });
                    }
                  }
                ),


              ),


            ],
          ),

        ),
      ),
    );
  }
}
