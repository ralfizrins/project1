import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:santhwanam_plus/admin/admin_donorview.dart';
import 'package:santhwanam_plus/admin/admin_home.dart';
import 'package:santhwanam_plus/admin/admin_requestview.dart';
import 'package:santhwanam_plus/admin/admin_userview.dart';
import 'package:santhwanam_plus/admin/blooddonate_tbl.dart';
import 'package:santhwanam_plus/admin/bloodrequest_tbl.dart';

class Admin_bloodview extends StatefulWidget {
  const Admin_bloodview({Key? key}) : super(key: key);

  @override
  _Admin_bloodviewState createState() => _Admin_bloodviewState();
}

class _Admin_bloodviewState extends State<Admin_bloodview> {

  var options=["VIEW","DELET","SEARCH"];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            children: [
              const SizedBox(height: 30,),
              const Text("Blood Bank List",
                style: TextStyle(color: Colors.black,fontSize: 20,
                  fontWeight: FontWeight.bold,
                  fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                ),),
              const SizedBox(height: 20,),

              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Container(width: MediaQuery.of(context).size.width,height: 300,
                  child: Column(
                    children: [
                      Container(height: 60,width:MediaQuery.of(context).size.width,
                        child: ElevatedButton.icon(
                          label: const Text('VIEW Donors' ,
                            style: TextStyle(fontSize: 16,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                            ),),
                          icon: Icon(Icons.view_list),
                          style: ElevatedButton.styleFrom(
                            primary: Color(0xff3BB9FF),
                            shape:  RoundedRectangleBorder(
                              borderRadius:  BorderRadius.circular(15),
                            ),
                          ),
                          onPressed: () {
                            Navigator.push(context, MaterialPageRoute(builder: (context)=> Blooddonate_tbl()));
                          },
                        ),
                      ),
                      const SizedBox(height: 10,),
                      Container(height: 60,width:MediaQuery.of(context).size.width,
                        child: ElevatedButton.icon(
                          label: const Text('View Requests' ,
                            style: TextStyle(fontSize: 16,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                            ),),
                          icon: Icon(Icons.search),
                          style: ElevatedButton.styleFrom(
                            primary: Color(0xff3BB9FF),
                            shape:  RoundedRectangleBorder(
                              borderRadius:  BorderRadius.circular(15),
                            ),
                          ),
                          onPressed: () {
                            Navigator.push(context, MaterialPageRoute(builder: (context)=> Bloodrequest_tblview()));
                          },
                        ),
                      ),
                      SizedBox(height: 10,),
                      Container(height: 60,width:MediaQuery.of(context).size.width,
                        child: ElevatedButton.icon(
                          label: const Text('DELETE User' ,
                            style: TextStyle(fontSize: 16,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                            ),),
                          icon: Icon(Icons.delete),
                          style: ElevatedButton.styleFrom(
                            primary: Color(0xff3BB9FF),
                            shape:  RoundedRectangleBorder(
                              borderRadius:  BorderRadius.circular(15),
                            ),
                          ),
                          onPressed: () {
                            Navigator.push(context, MaterialPageRoute(builder: (context)=> Admin_userview()));
                          },
                        ),
                      ),
                      SizedBox(height: 10,),

                    ],
                  ),
                ),
              ),
              ClipPath(
                clipper: WaveClipperTwo(reverse: true),
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height/6,
                  color: Color(0xffef5350),
                ),
              ),
            ],

          ),

        ),
      ),

    );
  }
}
