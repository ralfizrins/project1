import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:santhwanam_plus/admin/admin_home.dart';
import 'package:santhwanam_plus/users/loginpage.dart';
import 'package:santhwanam_plus/validation.dart';
import 'package:date_time_picker/date_time_picker.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:santhwanam_plus/admin/view_volunteer.dart';

class Fullview extends StatefulWidget {
  var uid;
  var email;
  var phone;
  var name;
  var address;
  var job;
  var bloodgroup;
  var dob;
  var proof;
  var password;
  var blood;
  var vpincode;
  var imgurl;
  Fullview(
      {Key? key,
      this.uid,
      this.email,
      this.phone,
      this.imgurl,
      this.name,
      this.address,
      this.job,
      this.proof,
      this.dob,
      this.password,
        this.blood,
      this.bloodgroup,
      this.vpincode})
      : super(key: key);
  @override
  _FullviewState createState() => _FullviewState();
}

class _FullviewState extends State<Fullview> {
  TextEditingController emailInputcontroller = TextEditingController();
  TextEditingController phone = TextEditingController();
  TextEditingController name = TextEditingController();
  TextEditingController address = TextEditingController();
  TextEditingController vpincode = TextEditingController();
  TextEditingController proof = TextEditingController();
  TextEditingController job = TextEditingController();
  TextEditingController blood = TextEditingController();
  TextEditingController dob = TextEditingController();

  var bloodgroup;
  var assume = "Value from DB"; //for demo
  late TextEditingController _takedob; //For late initialisation:- late Keyword
  late String _valueChanged3;
  late String _valueToValidate3;
  late String _valueSaved3;
  final ImagePicker _picker = ImagePicker();
  var filename;
  // For pick Image
  XFile? _image; // For accept Null:-?
  var url;
  void initState() {
    // TODO: implement initState
    _takedob = new TextEditingController(text: DateTime.now().toString());

    name.text = widget.name;

    _takedob.text = widget.dob;
    emailInputcontroller.text = widget.email;
    phone.text = widget.phone;
    address.text = widget.address;
    vpincode.text = widget.vpincode;
    //dob.text=widget.dob;
    proof.text = widget.proof;
    job.text = widget.job;
    bloodgroup = widget.blood;
    filename = DateTime.now().toString();
    var url;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var visible = true;

    return Scaffold(
      appBar: AppBar(
        title: Text("Back To Home"),
        backgroundColor: Color(0xffef5350),
      ),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            children: [
              const SizedBox(
                height: 20,
              ),
              const Text(
                "Volunteer Profile",
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Column(
                children: [
                  GestureDetector(
                    onTap: (){
                      _getpics();
                    },
                    child:   _image != null
                        ? ClipRect(
                      child: Image.file(
                        File(_image!.path), //null check operator added:-!
                      ),
                    )
                        : Container(
                      width: 80, //MediaQuery.of(context).size.width,
                      height: 80, //MediaQuery.of(context).size.height,
                      child: Image.network(widget.imgurl),
                    ),

                  ),

                  ElevatedButton(
                    onPressed: () {
                      var ref = FirebaseStorage.instance
                          .ref()
                          .child('volunteer/$filename');
                      UploadTask uploadTask = ref.putFile(File(_image!.path));

                      uploadTask.then((res) async {
                        url = (await ref.getDownloadURL()).toString();
                      }).then((value) => FirebaseFirestore.instance
                          .collection('volunteer')
                          .doc(widget.uid)
                          .update({
                        'imgurl': url,
                      }).then((value) {
                        showsnackbar(context, "Image uploaded");
                      }));

                    },
                    child: Text("Uplaod Image"),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  //after profile pic
                ],
              ),
              const SizedBox(
                height: 30,
              ),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: Container(
                  width: 400,
                  height: 650,
                  child: Card(
                    shadowColor: Colors.grey,
                    elevation: 2.0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    color: Colors.white,
                    child: Container(
                      child: Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Column(
                          children: [
                            SizedBox(height: 20),
                            TextFormField(
                              controller: name,
                              readOnly: true,
                              decoration: InputDecoration(
                                hintText: assume,
                                hintStyle: const TextStyle(
                                    fontFamily:
                                        "RobotoSlab-VariableFont_wght.ttf"),
                              ),
                            ), //Name
                            TextFormField(
                              controller: emailInputcontroller,
                              decoration: InputDecoration(
                                hintText: assume,
                                hintStyle: const TextStyle(
                                    fontFamily:
                                        "RobotoSlab-VariableFont_wght.ttf"),
                              ),
                            ), //Email
                            TextFormField(
                              controller: phone,
                              decoration: InputDecoration(
                                hintText: assume,
                                hintStyle: const TextStyle(
                                    fontFamily:
                                        "RobotoSlab-VariableFont_wght.ttf"),
                              ),
                            ), //mobile
                            AbsorbPointer(
                              child: DateTimePicker(
                                type: DateTimePickerType.date,
                                //dateMask: 'yyyy/MM/dd',
                                fieldLabelText: 'Date of birth',
                                controller: _takedob,
                                firstDate: DateTime(1990),
                                lastDate: DateTime(2050),
                                icon: Icon(Icons.event),
                                dateLabelText: 'Date of birth',
                                //locale: Locale('pt', 'BR'),
                                onChanged: (val) =>
                                    setState(() => _valueChanged3 = val),
                                validator: (val) {
                                  setState(() => _valueToValidate3 = val ?? '');
                                  return null;
                                },
                                onSaved: (val) =>
                                    setState(() => _valueSaved3 = val ?? ''),
                              ),
                            ), //dob
                            //Text(DateFormat('dd-MM-yyyy').format(snapshot.data!.docs[index]['dob'].toDate())),//
                            TextField(
                              keyboardType: TextInputType.multiline,
                              maxLines: null,
                              readOnly: true,
                              controller: address,
                              decoration: const InputDecoration(
                                hintText: 'address',
                                hintStyle: TextStyle(
                                    fontFamily:
                                        "RobotoSlab-VariableFont_wght.ttf"),
                              ),
                            ), //address_Multilined
                            TextFormField(
                              controller: vpincode,
                              cursorColor: Colors.black,
                              keyboardType: TextInputType.number,
                              decoration: const InputDecoration(
                                hintText: 'Pincode',
                                hintStyle: TextStyle(
                                    fontFamily:
                                        "RobotoSlab-VariableFont_wght.ttf"),
                              ),
                              validator: (value) {
                                return Validate.pincodevalidator(value!.trim());
                              },
                            ), //pincode
                            TextFormField(
                              controller: proof,
                              cursorColor: Colors.black,
                              keyboardType: TextInputType.text,
                              decoration: const InputDecoration(
                                hintText: 'ID(Adhar/passport/Liscemse)',
                                hintStyle: TextStyle(
                                    fontFamily:
                                        "RobotoSlab-VariableFont_wght.ttf"),
                              ),
                              validator: (value) {
                                return Validate.phonevalidator(value!.trim());
                              },
                            ), //proof
                            DropdownButtonFormField<String>(
                              value: bloodgroup,
                              decoration: const InputDecoration(
                                hintText: "Blood Group",
                                //prefixIcon: Icon(Icons.email),
                                //suffixIcon: Icon(Icons.panorama_fish_eye),
                                //border: OutlineInputBorder(borderRadius: BorderRadius.circular(30),
                              ),
                              onChanged: (ctgry) =>
                                  setState(() => bloodgroup = ctgry),
                              validator: (value) =>
                                  value == null ? 'field required' : null,
                              items: [
                                'A+',
                                'A-',
                                'B+',
                                'B-',
                                'AB+',
                                'AB-',
                                'O+',
                                'O-'
                              ].map<DropdownMenuItem<String>>((String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList(),
                            ), //blood

                            TextFormField(
                              controller: job,
                              cursorColor: Colors.black,
                              keyboardType: TextInputType.text,
                              decoration: const InputDecoration(
                                hintText: 'Job',
                                hintStyle: TextStyle(
                                    fontFamily:
                                        "RobotoSlab-VariableFont_wght.ttf"),
                              ),
                              validator: (value) {
                                return Validate.phonevalidator(value!.trim());
                              },
                            ), //job

                            const SizedBox(
                              height: 10,
                            ),

                            const SizedBox(
                              height: 30,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 10),
                              child: Row(
                                children: [
                                  ElevatedButton(
                                      onPressed: () {
                                        _showalert(context);
                                        //Go to user home
                                      },
                                      style: ElevatedButton.styleFrom(
                                        primary: Color(0xffef5350),
                                        shape:  RoundedRectangleBorder(
                                          borderRadius:
                                               BorderRadius.circular(15),
                                        ),
                                      ),
                                      child: const Center(
                                        child: Text(
                                          "  Delete  ",
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                            fontFamily:
                                                "RobotoSlab-VariableFont_wght.ttf",
                                          ),
                                        ),
                                      )),
                                  const SizedBox(
                                    width: 20,
                                  ),
                                  ElevatedButton(
                                      onPressed: () {
                                        //Go to user home
                                        FirebaseFirestore.instance
                                            .collection('volunteer')
                                            .doc(widget.uid)
                                            .update({
                                          'phone': phone.text,
                                          'address': address.text,
                                          'dob': dob.text,
                                          'pincode': vpincode.text,
                                          'proof': proof.text,
                                          'job': job.text,
                                          'blood': bloodgroup,
                                        }).then((value) {
                                          showsnackbar(
                                              context, "updated successfully");

                                          Navigator.pop(context);
                                        });
                                      },
                                      style: ElevatedButton.styleFrom(
                                        primary: Color(0xffef5350),
                                        shape:  RoundedRectangleBorder(
                                          borderRadius:
                                               BorderRadius.circular(15),
                                        ),
                                      ),
                                      child: const Text(
                                        "  Update  ",
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          fontFamily:
                                              "RobotoSlab-VariableFont_wght.ttf",
                                        ),
                                      )),
                                ],
                              ),
                            ),
                            //SizedBox(height:15),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _imgbrowse() async {
    final XFile? photo = await _picker.pickImage(source: ImageSource.camera);
    //final XFile? image = await _picker.pickImage(source: ImageSource.gallery);
    setState(() {
      _image = photo;
    });
  }

  _imggallery() async {
    //final XFile? photo = await _picker.pickImage(source: ImageSource.camera);
    final XFile? image = await _picker.pickImage(source: ImageSource.gallery);
    setState(() {
      _image = image;
    });
  }

  _getpics() {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Container(
            child: Wrap(
              children: [
                Expanded(
                  child: ListTile(
                      title: Text("Photo Gallery"),
                      leading: Icon(Icons.image),
                      onTap: () {
                        _imggallery();
                      }),
                ),
                Expanded(
                  child: ListTile(
                    title: Text("Camera"),
                    leading: Icon(Icons.camera),
                    onTap: () {
                      _imgbrowse();
                    },
                  ),
                ),
              ],
            ),
          );
        });
  }

  _showalert(BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text(
              "Are you sure , you want to delete ?",
              style: TextStyle(fontFamily: "RobotoSlab-VariableFont_wght.ttf"),
            ),
            //content: Text("Are you sure"),
            actions: [
              TextButton(
                  onPressed: () {
                    FirebaseFirestore.instance
                        .collection('volunteer')
                        .doc(widget.uid)
                        .update({
                      'status': 0,
                    }).then((value) {
                      showsnackbar(context, "Deleted Successfully.");
                    });
                    Navigator.push(context, MaterialPageRoute(builder: (context) => View_volunteer()));
                    //Navigator.of(context).pop();

                  },
                  child: Text("OK")),
              TextButton(
                  onPressed: () {

                    Navigator.of(context).pop();
                  },
                  child: Text("CANCEL")),
            ],
          );
        });
  }


}
