import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:santhwanam_plus/admin/admin_home.dart';
import 'package:santhwanam_plus/admin/admin_userview.dart';
import 'package:santhwanam_plus/users/loginpage.dart';
import 'package:santhwanam_plus/admin/users.dart';
import 'package:santhwanam_plus/validation.dart';
import 'package:date_time_picker/date_time_picker.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';

class User_fullview extends StatefulWidget {
  var uid;
  var uemail;
  var uname;
  var uphonenumber;
  var dob;
  var uaddress;
  var upincode;
  var uproof;
  var ubloodgroup;
  var ujob;


  User_fullview({Key? key,this.uid,this.uemail,this.uname,this.uphonenumber,this.dob,this.uaddress,this.ubloodgroup,this.ujob,this.upincode,this.uproof}) : super(key: key);

  @override
  _User_fullviewState createState() => _User_fullviewState();
}

class _User_fullviewState extends State<User_fullview> {
  TextEditingController emailInputcontroller = TextEditingController();
  TextEditingController phone = TextEditingController();
  TextEditingController name = TextEditingController();
  TextEditingController address = TextEditingController();
  TextEditingController pincode = TextEditingController();
  TextEditingController proof = TextEditingController();
  TextEditingController job = TextEditingController();
  TextEditingController blood = TextEditingController();

  var bloodgroup;
  var assume="Value from DB";//for demo
  late TextEditingController _takedob; //For late initialisation:- late Keyword
  late String _valueChanged3;
  late String _valueToValidate3;
  late String _valueSaved3;
  final ImagePicker _picker = ImagePicker();// For pick Image
  XFile ? _image; // For accept Null:-?
  var filename;
  void initState() {
    // TODO: implement initState
    _takedob=new TextEditingController(text:DateTime.now().toString());
    super.initState();

    name.text = widget.uname;
    emailInputcontroller.text = widget.uemail;
    phone.text = widget.uphonenumber;
    _takedob.text=widget.dob!=null?widget.dob:"DOB not set";
    address.text=widget.uaddress!=null?widget.uaddress:"address not set";
    pincode.text=widget.upincode!=null?widget.upincode:"pincode not set";
    proof.text=widget.uproof!=null?widget.uproof:"proof not set";
    blood.text=widget.ubloodgroup!=null?widget.ubloodgroup:"bloodgroup not set";
    job.text=widget.ujob!=null?widget.ujob:"job not set";
    filename = DateTime.now().toString();
    

  }
  @override
  Widget build(BuildContext context) {
    var visible=true;

    return Scaffold(
      appBar: AppBar(
        title: Text("Back To Home"),
        backgroundColor: Color(0xffef5350),
      ),
      body: SingleChildScrollView(
        child: SafeArea(
          child:Column(
            children: [

              SizedBox(height: 20,),
              const Text("User Profile",
                style: TextStyle(color: Colors.black,fontSize: 20,
                  fontWeight: FontWeight.bold,
                  fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                ),),
              SizedBox(height: 20,),
              Column(
                children: [
                  GestureDetector(
                    onTap: (){
                      _getpics();
                    },
                    child: Container(
                      width:30,//MediaQuery.of(context).size.width,
                      height:30, //MediaQuery.of(context).size.height,
                      child: Icon(Icons.insert_drive_file,size: 50,),
                    ),
                  ),
                  SizedBox(height: 20,),
                  //after profile pic
                ],
              ),
              SizedBox(height: 30,),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: Container(width: 400,height: 650,
                  child: Card(shadowColor: Colors.grey,elevation: 2.0,
                    shape:RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0),
                    ),color: Colors.white,
                    child: Container(
                      child: Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Column(
                          children: [

                            SizedBox(height:20),
                            TextFormField(
                              controller: name,

                              decoration: InputDecoration(
                                hintText: assume,
                                hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                              ),
                            ),//Name
                            TextFormField(
                              controller: emailInputcontroller,

                              decoration: InputDecoration(
                                hintText: assume,
                                hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                              ),
                            ),//Email
                            TextFormField(
                              controller: phone,

                              decoration: InputDecoration(
                                hintText: assume,
                                hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                              ),
                            ),//mobile
                            AbsorbPointer(
                              child: DateTimePicker(
                                type: DateTimePickerType.date,
                                //dateMask: 'yyyy/MM/dd',
                                fieldLabelText: 'Date of birth',
                                controller: _takedob,
                                firstDate: DateTime(1990),
                                lastDate: DateTime(2050),
                                icon: Icon(Icons.event),
                                dateLabelText: 'Date of birth',
                                //locale: Locale('pt', 'BR'),
                                onChanged: (val) => setState(() => _valueChanged3 = val),
                                validator: (val) {
                                  setState(() => _valueToValidate3 = val ?? '');
                                  return null;
                                },
                                onSaved: (val) => setState(() => _valueSaved3 = val ?? ''),
                              ),
                            ),//dob
                            TextField(
                              keyboardType: TextInputType.multiline,
                              maxLines: null,
                              readOnly: true,
                              controller: address,
                              decoration: const InputDecoration(
                                hintText: 'address',
                                hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                              ),),//address_Multilined
                            TextFormField(
                              controller: pincode,
                              readOnly: true,
                              cursorColor: Colors.black,
                              keyboardType: TextInputType.number,
                              decoration: const InputDecoration(
                                hintText: 'Pincode',
                                hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                              ),
                              validator: (value) {
                                return Validate.pincodevalidator(value!.trim());
                              },
                            ), //pincode
                            TextFormField(
                              controller: proof,
                              readOnly: true,
                              cursorColor: Colors.black,
                              keyboardType: TextInputType.text,
                              decoration: const InputDecoration(
                                hintText: 'ID(Adhar/passport/Liscemse)',
                                hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                              ),
                              validator: (value) {
                                return Validate.phonevalidator(value!.trim());
                              },
                            ), //proof
                            AbsorbPointer(
                              child: DropdownButtonFormField<String>(
                                value: bloodgroup,
                                decoration: const InputDecoration(
                                  hintText: "Blood Group",
                                  //prefixIcon: Icon(Icons.email),
                                  //suffixIcon: Icon(Icons.panorama_fish_eye),
                                  //border: OutlineInputBorder(borderRadius: BorderRadius.circular(30),
                                ),
                                onChanged: (ctgry) =>
                                    setState(() => bloodgroup = ctgry),
                                validator: (value) => value == null ? 'field required' : null,
                                items: ['A+','A-','B+','B-','AB+','AB-','O+','O-']
                                    .map<DropdownMenuItem<String>>(
                                        (String value) {
                                      return DropdownMenuItem<String>(
                                        value: value,
                                        child: Text(value),
                                      );
                                    }).toList(),
                              ),
                            ),//blood
                            TextFormField(
                              controller: job,
                              readOnly: true,
                              cursorColor: Colors.black,
                              keyboardType: TextInputType.text,
                              decoration: const InputDecoration(
                                hintText: 'Job',
                                hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                              ),
                              validator: (value) {
                                return Validate.phonevalidator(value!.trim());
                              },
                            ),//job

                            SizedBox(height: 10,),

                            SizedBox(height: 30,),
                            Row(
                              children: [

                                SizedBox(width: 10,),
                                ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    primary: Color(0xffef5350),
                                    shape:  RoundedRectangleBorder(
                                      borderRadius:  BorderRadius.circular(15),
                                    ),
                                  ),
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                  child: const Text("BACK",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                                    ),),
                                ),
                                SizedBox(width: 20,),

                                ElevatedButton(
                                    onPressed: () {
                                      _showalert(context);
                                      //Go to user home

                                    },
                                    style: ElevatedButton.styleFrom(
                                      primary: Color(0xffef5350),
                                      shape:  RoundedRectangleBorder(
                                        borderRadius:  BorderRadius.circular(15),
                                      ),
                                    ),
                                    child: const Text(
                                      "  DELETE  ",
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                                      ),
                                    )),
                              ],
                            ),
                            //SizedBox(height:15),


                          ],
                        ),
                      ),


                    ),
                  ),
                ),
              ),

            ],
          ),

        ),
      ),
    );
  }

  _imgbrowse()async{
    final XFile? photo = await _picker.pickImage(source: ImageSource.camera);
    //final XFile? image = await _picker.pickImage(source: ImageSource.gallery);
    setState(() {
      _image=photo;
    });
  }
  _imggallery()async{
    //final XFile? photo = await _picker.pickImage(source: ImageSource.camera);
    final XFile? image = await _picker.pickImage(source: ImageSource.gallery);
    setState(() {
      _image=image;
    });}
  _getpics(){
    showModalBottomSheet(
        context: context,
        builder: (context){
          return Container(
            child: Wrap(
              children: [
                Expanded(
                  child: ListTile(
                      title: Text("Photo Gallery"),
                      leading: Icon(Icons.image),
                      onTap: (){
                        _imggallery();
                      }

                  ),
                ),
                Expanded(
                  child: ListTile(
                    title: Text("Camera"),
                    leading: Icon(Icons.camera),
                    onTap: (){
                      _imgbrowse();
                    },
                  ),
                ),

              ],

            ),

          );
        }
    );
  }
  _showalert(BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text(
              "Are you sure , you want to delete ?",
              style: TextStyle(fontFamily: "RobotoSlab-VariableFont_wght.ttf"),
            ),
            //content: Text("Are you sure"),
            actions: [
              TextButton(
                  onPressed: () {
                    FirebaseFirestore.instance
                        .collection('user')
                        .doc(widget.uid)
                        .update({
                      'status': 0,
                    }).then((value) {
                      showsnackbar(context, "Deleted Successfully.");
                    });
                    Navigator.push(context, MaterialPageRoute(builder: (context) =>Admin_userview()));
                    //Navigator.of(context).pop();

                  },
                  child: Text("OK")),
              TextButton(
                  onPressed: () {

                    Navigator.of(context).pop();
                  },
                  child: Text("CANCEL")),
            ],
          );
        });
  }



}

