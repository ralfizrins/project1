import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:santhwanam_plus/admin/fullview.dart';
import 'package:santhwanam_plus/admin/user_fullview.dart';

import 'carefullview.dart';

class Admin_careview extends StatefulWidget {




  @override
  _Admin_careviewState createState() => _Admin_careviewState();
}

class _Admin_careviewState extends State<Admin_careview> {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Care Home"),
        backgroundColor: const Color(0xffef5350),
      ),

      body: SingleChildScrollView(
        child: SafeArea(
          child:Column(
            children: [

              const SizedBox(height: 10,),
              const Text("Care Persons",
                style: TextStyle(color: Colors.black,fontSize: 20,
                  fontWeight: FontWeight.bold,
                  fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                ),),
              const SizedBox(height: 30,),
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                color: Colors.white,
                child: StreamBuilder<QuerySnapshot>(
                    stream: FirebaseFirestore.instance.collection('careperson').where('status',isEqualTo: 1).snapshots(),
                    builder: (context, snapshot) {
                      if(!snapshot.hasData) {
                        return const Center(child: CircularProgressIndicator());
                      } else  if(snapshot.hasData && snapshot.data!.docs.isEmpty) {
                        return const Center(child: Text("no data found",style: TextStyle(fontSize: 25),));
                      } else {
                        return ListView.builder(
                          itemCount: snapshot.data!.docs.length,

                          itemBuilder: (BuildContext context, int index) {
                            return Card(
                              elevation: 10.0,
                              margin: const EdgeInsets.only(
                                  left: 10, top: 0, right: 10, bottom: 10),
                              shadowColor: Colors.white,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15),
                              ),
                              child: SingleChildScrollView(
                                child: ListTile(
                                  onTap: () {
                                    Navigator.push(context, MaterialPageRoute(builder: (context)=> Care_fullview(
                                      uid:snapshot.data?.docs[index]['uid'],
                                      imgurl:snapshot.data!.docs[index]['imgurl'],
                                      email: snapshot.data!.docs[index]['email'],
                                      name: snapshot.data!.docs[index]['name'],
                                      age: snapshot.data!.docs[index]['age'],
                                      phonenumber: snapshot.data!.docs[index]['phone'],
                                      address: snapshot.data!.docs[index]['address'],
                                      dob: snapshot.data!.docs[index]['dob'],
                                      bloodgroup: snapshot.data!.docs[index]['blood'],
                                      disease: snapshot.data!.docs[index]['disease'],
                                      pincode: snapshot.data!.docs[index]['pincode'],
                                      proof: snapshot.data!.docs[index]['proof'],

                                    )));

                                  },
                                  title: Row(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(left: 23),
                                        child: Text(
                                          snapshot.data?.docs[index]['name'],
                                          style: const TextStyle(
                                              fontFamily:
                                              'RobotoSlab-VariableFont_wght.ttf',
                                              fontWeight: FontWeight.bold,
                                              fontSize: 18),
                                        ),
                                      ),

                                    ],
                                  ),

                                ),
                              ),
                            );
                          });
                      }
                    }
                ),


              ),


            ],
          ),

        ),
      ),
    );
  }
}
