import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:santhwanam_plus/admin/fullview.dart';
import 'package:santhwanam_plus/admin/user_fullview.dart';

import 'carefullview.dart';

class Delete_careperson extends StatefulWidget {




  @override
  _Delete_carepersonState createState() => _Delete_carepersonState();
}

class _Delete_carepersonState extends State<Delete_careperson> {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Care Home"),),

      body: SingleChildScrollView(
        child: SafeArea(
          child:Column(
            children: [
              ClipPath(
                clipper: WaveClipperOne(),
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height/6,
                  color: Color(0xffef5350),
                ),
              ),
              SizedBox(height: 10,),
              const Text("Care Persons",
                style: TextStyle(color: Colors.black,fontSize: 20,
                  fontWeight: FontWeight.bold,
                  fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                ),),
              SizedBox(height: 30,),
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                color: Colors.white,
                child: StreamBuilder<QuerySnapshot>(
                    stream: FirebaseFirestore.instance.collection('careperson').where('status',isEqualTo: 1).snapshots(),
                    builder: (context, snapshot) {
                      if(!snapshot.hasData) {
                        return const Center(child: CircularProgressIndicator());
                      } else  if(snapshot.hasData && snapshot.data!.docs.length==0) {
                        return const Center(child: Text("No data found",style: TextStyle(fontSize: 25),));
                      } else {
                        return ListView.builder(
                          itemCount: snapshot.data!.docs.length,

                          itemBuilder: (BuildContext context, int index) {
                            return Card(
                              elevation: 10.0,
                              margin: const EdgeInsets.only(
                                  left: 10, top: 0, right: 10, bottom: 10),
                              shadowColor: Colors.white,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15),
                              ),
                              child: SingleChildScrollView(
                                child: ListTile(
                                 trailing: GestureDetector(
                                   onTap: (){
                                     FirebaseFirestore.instance.collection('careperson').doc(  snapshot.data?.docs[index]['uid']).delete();
                                   },
                                     child: Icon(Icons.delete)),
                                  title: Row(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(left: 23),
                                        child: Text(
                                          snapshot.data?.docs[index]['name'],
                                          style: const TextStyle(
                                              fontFamily:
                                              'RobotoSlab-VariableFont_wght.ttf',
                                              fontWeight: FontWeight.bold,
                                              fontSize: 18),
                                        ),
                                      ),

                                    ],
                                  ),

                                ),
                              ),
                            );
                          });
                      }
                    }
                ),


              ),


            ],
          ),

        ),
      ),
    );
  }
}
