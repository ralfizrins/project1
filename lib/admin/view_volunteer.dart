import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:santhwanam_plus/admin/fullview.dart';

class View_volunteer extends StatefulWidget {


  //var date=["date1,","date2,","date3,","date4,",];


  @override
  _View_volunteerState createState() => _View_volunteerState();
}

class _View_volunteerState extends State<View_volunteer> {
  var _volunteerkey = GlobalKey<FormState>();


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Back"),
        backgroundColor: Color(0xffef5350),
      ),
      body: SingleChildScrollView(
        child: SafeArea(
          child:Form(
            key: _volunteerkey,
            child: Column(
              children: [
                ClipPath(
                  clipper: WaveClipperOne(),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height/6,
                    color: Color(0xffef5350),
                  ),
                ),
                SizedBox(height: 10,),
                const Text("Volunteer Details",
                  style: TextStyle(color: Colors.black,fontSize: 20,
                    fontWeight: FontWeight.bold,
                    fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                  ),),
                SizedBox(height: 30,),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  color: Colors.white,
                  child: StreamBuilder<QuerySnapshot>(
                    stream: FirebaseFirestore.instance.collection('volunteer').where('status',isEqualTo: 1).snapshots(),
                    builder: (context, snapshot) {
                      if(!snapshot.hasData) {
                        return const Center(child: CircularProgressIndicator());
                      } else  if(snapshot.hasData && snapshot.data!.docs.isEmpty) {
                        return const Center(child: Text("no data found"));
                      } else {
                        return ListView.builder(
                          itemCount: snapshot.data!.docs.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Card(
                              elevation: 10.0,
                              margin: const EdgeInsets.only(
                                  left: 10, top: 0, right: 10, bottom: 10),
                              shadowColor: Colors.white,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15),
                              ),
                              child: SingleChildScrollView(
                                child: ListTile(
                                  onTap: () {
                                    Navigator.push(context, MaterialPageRoute(builder: (context)=> Fullview(
                                      uid: snapshot.data!.docs[index]['uid'],
                                      imgurl:snapshot.data!.docs[index]['imgurl'],
                                      email: snapshot.data!.docs[index]['email'],
                                      phone: snapshot.data!.docs[index]['phone'],
                                      address: snapshot.data!.docs[index]['address'],
                                      name: snapshot.data!.docs[index]['name'],
                                      dob: snapshot.data!.docs[index]['dob'],
                                      job: snapshot.data!.docs[index]['job'],
                                      blood: snapshot.data!.docs[index]['blood'],
                                      proof: snapshot.data!.docs[index]['proof'],
                                      vpincode: snapshot.data!.docs[index]['pincode']
                                    )));

                                  },
                                  title: Row(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(left: 23),
                                        child: Text(
                                         snapshot.data!.docs[index]['name'],
                                          style: const TextStyle(
                                              fontFamily:
                                              'RobotoSlab-VariableFont_wght.ttf',
                                              fontWeight: FontWeight.bold,
                                              fontSize: 18),
                                        ),
                                      ),

                                    ],
                                  ),

                                ),
                              ),
                            );
                          });
                      }
                    }
                  ),

                )


              ],
            ),
          ),

        ),
      ),
    );
  }
}