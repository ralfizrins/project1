import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:santhwanam_plus/validation.dart';
import 'package:date_time_picker/date_time_picker.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'package:santhwanam_plus/volunteer/volunteer_home.dart';

class Add_careperson extends StatefulWidget {
  var uid;
  var email;
  var phone;
  var name;
  var address;
  var bloodgroup;
  var proof;
  var password;
  var pincode;
  var dob;
  var disease;
  var age;
  var imgurl;
  Add_careperson({Key? key,this.uid,this.email,this.phone
    ,this.name
    ,this.address,this.pincode,this.bloodgroup,this.proof,this.dob,this.password,this.disease,this.age,this.imgurl}) : super(key: key);


  @override
  _Add_carepersonState createState() => _Add_carepersonState();
}

class _Add_carepersonState extends State<Add_careperson> {
  TextEditingController emailInputcontroller = TextEditingController();
  TextEditingController passwordInputcontroller = TextEditingController();
  TextEditingController phone = TextEditingController();
  TextEditingController name = TextEditingController();
  TextEditingController address = TextEditingController();
  TextEditingController pincode = TextEditingController();
  TextEditingController proof = TextEditingController();
  TextEditingController age = TextEditingController();
  TextEditingController blood = TextEditingController();
  TextEditingController disease= TextEditingController();

  var bloodgroup;
  var filename;
  var url;
  var assume="Value from DB";//for demo
  late TextEditingController _takedob; //For late initialisation:- late Keyword
  late String _valueChanged3;
  late String _valueToValidate3;
  late String _valueSaved3;
  final ImagePicker _picker = ImagePicker();// For pick Image
  XFile ? _image;
  var _carepersonkey = GlobalKey<FormState>();
  // For accept Null:-?

  void initState() {
    // TODO: implement initState
    filename=DateTime.now().toString();
    _takedob= TextEditingController(text:DateTime.now().toString());
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    var visible=true;

    return Scaffold(
      appBar: AppBar(
        //title: Text("Back To Home"),
        backgroundColor: const Color(0xffef5350),
      ),
      body: SingleChildScrollView(
        child: SafeArea(
          child:Form(
            key: _carepersonkey,
            child: Column(
              children: [

                const SizedBox(height: 20,),
                const Text("Adding new care person..",
                  style: TextStyle(color: Colors.black,fontSize: 20,
                    fontWeight: FontWeight.bold,
                    fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                  ),),
                const SizedBox(height: 20,),
                Column(
                  children: [
                    GestureDetector(
                      onTap: (){
                        _getpics();
                      },
                      child:   _image != null
                          ? ClipRect(
                        child: Container(
                          width: 80, //MediaQuery.of(context).size.width,
                          height: 80,
                          child: Image.file(
                            File(_image!.path), //null check operator added:-!
                          ),
                        ),
                      )
                          : widget.imgurl!=null?Container(
                        width: 80, //MediaQuery.of(context).size.width,
                        height: 80, //MediaQuery.of(context).size.height,
                        child: Image.network(widget.imgurl),
                      ):Container(
                        child:const Icon(Icons.account_circle),
                      ),

                    ),
                    ElevatedButton(
                      style:ElevatedButton.styleFrom(primary: Colors.white),
                      onPressed: () {
                        var ref = FirebaseStorage.instance
                            .ref()
                            .child('careperson/$filename');
                        UploadTask uploadTask = ref.putFile(File(_image!.path));

                        uploadTask.then((res) async {
                          url = (await ref.getDownloadURL()).toString();
                        }).then((value) => FirebaseFirestore.instance
                            .collection('careperson')
                            .doc(widget.uid)
                            .update({
                          'imgurl': url,
                        }).then((value) {
                          showsnackbar(context, "Image uploaded");
                        }));

                      },
                      child: const Text("Uplaod Image",style:TextStyle(color:Colors.red),),
                    ),
                    const SizedBox(height: 20,),
                    const Text("Upload profile picture",style: TextStyle(color: Colors.black,fontSize: 10,
                      fontWeight: FontWeight.bold,
                      fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                    ),),
                    const SizedBox(height: 20,),

                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Container(width: 400,height: 900,
                    child: Card(shadowColor: Colors.grey,elevation: 2.0,
                      shape:RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      ),color: Colors.white,
                      child: Container(
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Column(
                            children: [
                              const SizedBox(height:20),
                              TextFormField(
                                controller: name,
                                decoration: const InputDecoration(
                                  hintText: 'Name',
                                  hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                                ), validator: (value) {
                                return Validate.namevalidator(value!.trim());
                              },

                              ),
                              //Name
                              TextFormField(
                                controller: emailInputcontroller,
                                decoration: const InputDecoration(
                                  hintText: 'Email',
                                  hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                                ),
                                validator: (value) {
                                  return Validate.emailValidator(value!.trim());
                                },
                              ), //Email

                              TextFormField(
                                controller: passwordInputcontroller,
                                decoration: const InputDecoration(
                                  hintText: 'Password',
                                  hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                                ),
                                validator: (value) {
                                  return Validate.passwordvalidator(value!.trim());
                                },
                              ),

                              TextFormField(
                                controller: age,
                                keyboardType: TextInputType.number,
                                decoration: const InputDecoration(
                                  hintText: 'Age',
                                  hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                                ),
                                validator: (value) {
                                  return Validate.careagevalidator(value!.trim());
                                },
                              ),
                              DateTimePicker(
                                type: DateTimePickerType.date,
                                //dateMask: 'yyyy/MM/dd',
                                fieldLabelText: 'Date of birth',
                                controller: _takedob,
                                firstDate: DateTime(1990),
                                lastDate: DateTime(2050),
                                icon: Icon(Icons.event),
                                dateLabelText: 'Date of birth',
                                //locale: Locale('pt', 'BR'),
                                onChanged: (val) => setState(() => _valueChanged3 = val),
                                validator: (val) {
                                  setState(() => _valueToValidate3 = val ?? '');
                                  return null;
                                },
                                onSaved: (val) => setState(() => _valueSaved3 = val ?? ''),
                              ), //dob
                              TextFormField(
                                keyboardType: TextInputType.multiline,
                                maxLines: null,
                                controller: address,
                                decoration: const InputDecoration(
                                  hintText: 'Address',
                                  hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                                ),
                                validator: (value) {
                                  return Validate.addressvalidator(value!.trim());
                                },

                              ), //address_Multilined
                              TextFormField(
                                controller: pincode,
                                cursorColor: Colors.black,
                                keyboardType: TextInputType.number,
                                decoration: const InputDecoration(
                                  hintText: 'Pincode',
                                  hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                                ),
                                validator: (value) {
                                  return Validate.pincodevalidator(value!.trim());
                                },
                              ),  //pincode
                              TextFormField(
                                controller: proof,
                                cursorColor: Colors.black,
                                keyboardType: TextInputType.text,
                                decoration: const InputDecoration(
                                  hintText: 'ID(Adhar/passport/Liscemse)',
                                  hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                                ),
                                validator: (value) {
                                  return Validate.proofvalidator(value!.trim());
                                },
                              ),  //proof
                              DropdownButtonFormField<String>(
                                value: bloodgroup,
                                decoration: const InputDecoration(
                                  hintText: "Blood Group",
                                  //prefixIcon: Icon(Icons.email),
                                  //suffixIcon: Icon(Icons.panorama_fish_eye),
                                  //border: OutlineInputBorder(borderRadius: BorderRadius.circular(30),
                                ),
                                onChanged: (ctgry) =>
                                    setState(() => bloodgroup = ctgry),
                                validator: (value) => value == null ? 'field required' : null,
                                items: ['A+','A-','B+','B-','AB+','AB-','O+','O-']
                                    .map<DropdownMenuItem<String>>(
                                        (String value) {
                                      return DropdownMenuItem<String>(
                                        value: value,
                                        child: Text(value),
                                      );
                                    }).toList(),
                              ), //blood
                              TextFormField(
                                controller: phone,
                                cursorColor: Colors.black,
                                keyboardType: TextInputType.number,
                                decoration: const InputDecoration(
                                  hintText: 'Phone',
                                  hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                                ),
                                validator: (value) {
                                  return Validate.phonevalidator(value!.trim());
                                },
                              ), //job
                              TextFormField(
                                controller: disease,
                                decoration: const InputDecoration(
                                  hintText: 'Disease',
                                  hintStyle: TextStyle(fontFamily:"RobotoSlab-VariableFont_wght.ttf"),
                                ),
                                validator: (value) {
                                  return Validate.disease(value!.trim());
                                },
                              ),



                              const SizedBox(height: 50,),
                              ElevatedButton(
                                  onPressed: () {
                                    if (_carepersonkey.currentState!.validate())
                                    {
                                      var ref =
                                      FirebaseStorage.instance.ref().child('careperson/$filename');
                                      UploadTask uploadTask = ref.putFile(File(_image!.path));


                                      uploadTask.then((res) async{
                                        url = (await ref.getDownloadURL()).toString();

                                      }).then((value) {
                                        FirebaseAuth.instance.createUserWithEmailAndPassword
                                          (email:emailInputcontroller.text.trim(),
                                            password: passwordInputcontroller.text.trim()).
                                        then((user) => FirebaseFirestore.instance.collection('login').doc(user.user!.uid).set({
                                          'uid':user.user!.uid,
                                          'email':emailInputcontroller.text.trim(),
                                          'password':passwordInputcontroller.text.trim(),
                                          'status':1,
                                          'userstatus':4,
                                          'date':DateTime.now()
                                        }).then((value) => FirebaseFirestore.instance.collection('careperson').doc(user.user!.uid).
                                        set(
                                            {'uid':user.user!.uid,
                                              'email':emailInputcontroller.text.trim(),
                                              'password':passwordInputcontroller.text.trim(),
                                              'name':name.text,
                                              'phone':phone.text,
                                              'address':address.text,
                                              'dob':_takedob.text,
                                              'pincode':pincode.text,
                                              'proof':proof.text,
                                              'age':age.text,
                                              'disease':disease.text,
                                              'imgurl':url,
                                              'blood':bloodgroup,
                                              'status':1,
                                              'date':DateTime.now()
                                            }
                                        ).then((value) {
                                          showsnackbar(context,"Registered successfully");

                                          Navigator.pop(context);
                                        }))).catchError((e)=> print(e));
                                      });


                                    }
                                    else {
                                      showsnackbar(context, "please check the fields");
                                    }
                                    },
                                  style: ElevatedButton.styleFrom(
                                    primary: const Color(0xffef5350),
                                    shape:  RoundedRectangleBorder(
                                      borderRadius:  BorderRadius.circular(15),
                                    ),
                                  ),
                                  child: const Text(
                                    "Submit",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                                    ),
                                  )),
                              //SizedBox(height:15),


                            ],
                          ),
                        ),


                      ),
                    ),
                  ),
                ),

              ],
            ),
          ),

        ),
      ),
    );
  }

  _imgbrowse()async{
    final XFile? photo = await _picker.pickImage(source: ImageSource.camera);
    //final XFile? image = await _picker.pickImage(source: ImageSource.gallery);
    setState(() {
      _image=photo;
    });
  }
  _imggallery()async{
    //final XFile? photo = await _picker.pickImage(source: ImageSource.camera);
    final XFile? image = await _picker.pickImage(source: ImageSource.gallery);
    setState(() {
      _image=image;
    });}
  _getpics(){
    showModalBottomSheet(
        context: context,
        builder: (context){
          return Container(
            child: Wrap(
              children: [
                Expanded(
                  child: ListTile(
                      title: Text("Photo Gallery"),
                      leading: Icon(Icons.image),
                      onTap: (){
                        _imggallery();
                      }

                  ),
                ),
                Expanded(
                  child: ListTile(
                    title: Text("Camera"),
                    leading: Icon(Icons.camera),
                    onTap: (){
                      _imgbrowse();
                    },
                  ),
                ),

              ],

            ),

          );
        }
    );
  }

  showsnackbar(BuildContext context, String value) {
    ScaffoldMessenger.of(context).showSnackBar(
         SnackBar(behavior: SnackBarBehavior.floating,
            backgroundColor: Colors.red,
            margin:const EdgeInsetsDirectional.all(10),
            content: Text(
              value,
              style: const TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
            )));
  }
}