import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:santhwanam_plus/admin/admin_careview.dart';
import 'package:santhwanam_plus/admin/add_careperson.dart';

import 'deletecareperson.dart';

class Carehome extends StatefulWidget {
  const Carehome({Key? key}) : super(key: key);

  @override
  _CarehomeState createState() => _CarehomeState();
}

class _CarehomeState extends State<Carehome> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            children: [
              SizedBox(height: 30,),
              const Text("Care Home Control",
                style: TextStyle(color: Colors.black,fontSize: 20,
                  fontWeight: FontWeight.bold,
                  fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                ),),
              SizedBox(height: 20,),

              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Container(width: MediaQuery.of(context).size.width,height: 300,
                  child: Column(
                    children: [
                      Container(height: 60,width:MediaQuery.of(context).size.width,
                        child: ElevatedButton.icon(
                          label: const Text('ADD Care Person' ,
                            style: TextStyle(fontSize: 16,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                            ),),
                          icon: Icon(Icons.add),
                          style: ElevatedButton.styleFrom(
                            primary: Color(0xff3BB9FF),
                            shape:  RoundedRectangleBorder(
                              borderRadius:  BorderRadius.circular(15),
                            ),
                          ),
                          onPressed: () {
                            Navigator.push(context, MaterialPageRoute(builder: (context)=> Add_careperson()));
                          },
                        ),
                      ),
                      SizedBox(height: 10,),
                      Container(height: 60,width:MediaQuery.of(context).size.width,
                        child: ElevatedButton.icon(
                          label: const Text('VIEW Persons' ,
                            style: TextStyle(fontSize: 16,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                            ),),
                          icon: Icon(Icons.view_list),
                          style: ElevatedButton.styleFrom(
                            primary: Color(0xff3BB9FF),
                            shape:  RoundedRectangleBorder(
                              borderRadius:  BorderRadius.circular(15),
                            ),
                          ),
                          onPressed: () {
                            Navigator.push(context, MaterialPageRoute(builder: (context)=> Admin_careview()));

                          },
                        ),
                      ),
                      SizedBox(height: 10,),
                      Container(height: 60,width:MediaQuery.of(context).size.width,
                        child: ElevatedButton.icon(
                          label: const Text('SEARCH Person' ,
                            style: TextStyle(fontSize: 16,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                            ),),
                          icon: Icon(Icons.search),
                          style: ElevatedButton.styleFrom(
                            primary: Color(0xff3BB9FF),
                            shape:  RoundedRectangleBorder(
                              borderRadius:  BorderRadius.circular(15),
                            ),
                          ),
                          onPressed: () {
                            print('Pressed');
                          },
                        ),
                      ),
                      SizedBox(height: 10,),
                      Container(height: 60,width:MediaQuery.of(context).size.width,
                        child: ElevatedButton.icon(
                          label: const Text('DELETE Person' ,
                            style: TextStyle(fontSize: 16,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontFamily: "RobotoSlab-VariableFont_wght.ttf",
                            ),),
                          icon: Icon(Icons.delete),
                          style: ElevatedButton.styleFrom(
                            primary: Color(0xff3BB9FF),
                            shape:  RoundedRectangleBorder(
                              borderRadius:  BorderRadius.circular(15),
                            ),
                          ),
                          onPressed: () {
                            Navigator.push(context, MaterialPageRoute(builder: (context)=> Delete_careperson(

                            )));

                          },
                        ),
                      ),
                      SizedBox(height: 10,),

                    ],
                  ),
                ),
              ),
              ClipPath(
                clipper: WaveClipperTwo(reverse: true),
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height/6,
                  color: Color(0xffef5350),
                ),
              ),
            ],

          ),

        ),
      ),

    );
  }
}
